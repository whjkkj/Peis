package com.whjk;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 应用启动入口
 *
 * @author hl
 * @date 2021-07-14 00:19:58
 */
@EnableConfigurationProperties
@SpringBootApplication
@MapperScan(basePackages = {"com.whjk.system.*.mapper", "com.whjk.system.*.timer.mapper"})
public class SystemApplication {

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext context = SpringApplication.run(SystemApplication.class, args);
        String hostAddress = InetAddress.getLocalHost().getHostAddress();
        Environment environment = context.getBean(Environment.class);
        System.out.println("swagger地址:\nhttp://" + hostAddress + ":" + environment.getProperty("server.port") + environment.getProperty("server.servlet.context-path") + "/swagger-ui/");
        System.out.println("swagger-bootstrap-ui-地址:\nhttp://" + hostAddress + ":" + environment.getProperty("server.port") + environment.getProperty("server.servlet.context-path") + "/doc.html");
    }

}
