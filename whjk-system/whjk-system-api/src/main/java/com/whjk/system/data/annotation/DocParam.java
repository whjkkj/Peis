package com.whjk.system.data.annotation;


import com.deepoove.poi.data.PictureType;
import com.whjk.system.data.uitls.DocTestType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hl
 * @date 20230608
 * @description ExcelParam
 */

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DocParam {
    /**
     * 值
     */
    DocTestType type() default DocTestType.TEXT;

    /**
     * 字体颜色 FF0000
     * @return
     */
    String textColor() default "";

    /**
     * 当type=BASE64 以下必填
     * 图片类型
     *
     * @return
     */
    PictureType imgType() default PictureType.PNG;

    /**
     * 图片大小宽高
     *
     * @return
     */
    int[] imgSize() default {20, 20};


}
