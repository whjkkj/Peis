package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.whjk.core.common.dto.PageDto;
import com.whjk.system.data.po.TcZhxmPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 体检套餐主表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_tc")
@ApiModel(value="WhjkTc对象", description="体检套餐主表")
public class WhjkTc extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "编号")
    @TableField("bh")
    private String bh;

    @ApiModelProperty(value = "套餐名称")
    @TableField("tcmc")
    private String tcmc;

    @ApiModelProperty(value = "套餐拼音")
    @TableField("pinyin_code")
    private String pinyinCode;

    @ApiModelProperty(value = "简称")
    @TableField("jc")
    private String jc;

    @ApiModelProperty(value = "显示顺序")
    @TableField("xssx")
    private BigDecimal xssx;

    @ApiModelProperty(value = "是否启用（0-启用，1-未启用）")
    @TableField("sfqy")
    private String sfqy;

    @ApiModelProperty(value = "标准价格")
    @TableField("bzjg")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "打折")
    @TableField("dz")
    private BigDecimal dz;

    @ApiModelProperty(value = "实际价格")
    @TableField("sjjg")
    private BigDecimal sjjg;

    @ApiModelProperty(value = "业务类型ID")
    @TableField("ywlx_id")
    private String ywlxId;

    @ApiModelProperty(value = "适用范围（0-通用，1-集体，2-个人）")
    @TableField("syfw")
    private String syfw;

    @ApiModelProperty(value = "性别(2-女 1-男 %-不限)")
    @TableField("xb")
    private String xb;

    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;

    @ApiModelProperty(value = "简介")
    @TableField("jj")
    private String jj;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @TableField(exist = false)
    private List<TcZhxmPo> zhxmList;
}
