package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkTc;
import com.whjk.system.data.entity.WhjkZhxm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "WhjkTcZhxmDto", description = "WhjkTcZhxmDto")
public class WhjkTcZhxmDto extends WhjkTc{

    @ApiModelProperty(value = "组合id")
    private List<WhjkZhxm> whjkZhxmList;
}
