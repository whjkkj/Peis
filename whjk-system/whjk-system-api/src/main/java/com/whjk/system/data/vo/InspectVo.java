package com.whjk.system.data.vo;

import lombok.Data;

@Data
public class InspectVo {
    /** 序号 */
    private String num;
    /** 检验项目名称 */
    private String monitorName;
    /** 检验结果值 */
    private String value;
    /** 结果说明 */
    private String resultTips;
    /** 单位 */
    private String unit;
    /** 参考范围 */
    private String referenceRange;
    /** 指向箭头 */
    private String arrow;
}
