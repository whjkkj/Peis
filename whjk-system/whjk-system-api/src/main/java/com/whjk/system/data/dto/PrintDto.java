package com.whjk.system.data.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PrintDto {

    @ApiModelProperty("登记Id")
    private String personId;
    @ApiModelProperty("登记Id集合")
    private List<String> personIds;
    @ApiModelProperty("1、old,2、new")
    private String printType;

}
