package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 体检类型设置
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_lx")
@ApiModel(value="WhjkLx对象", description="体检类型设置")
public class WhjkLx extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "编码")
    @TableField("bm")
    private String bm;

    @ApiModelProperty(value = "名称")
    @TableField("mc")
    private String mc;

    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;

    @ApiModelProperty(value = "是否启用（0-启用，1-未启用）")
    @TableField("sfqy")
    private String sfqy;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "体检类别（0-健康体检，1-职业体检，2-从业体检，3-招工体检，4-学生体检，5-征兵体检）")
    @TableField("tjlx")
    private String tjlx;

    @ApiModelProperty(value = "业务类型id")
    @TableField("ywlx_id")
    private String ywlxId;

    @ApiModelProperty(value = "职业病类型")
    @TableField("zyblx")
    private String zyblx;


}
