package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 常见结果维护
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_cjjgwh")
@ApiModel(value="WhjkCjjgwh对象", description="常见结果维护")
public class WhjkCjjgwh extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "体检项目id(基础项目)")
    @TableField("tjxm_id")
    private String tjxmId;

    @ApiModelProperty(value = "描述")
    @TableField("mx")
    private String mx;

    @ApiModelProperty(value = "诊断id")
    @TableField("zd_id")
    private String zdId;

    @ApiModelProperty(value = "是否阳性（0-否，1-是）")
    @TableField("sfyx")
    private String sfyx;

    @ApiModelProperty(value = "性别(0-女 1-男 %-不限)")
    @TableField("xb")
    private String xb;

    @ApiModelProperty(value = "拼音简码")
    @TableField("pyjm")
    private String pyjm;

    @ApiModelProperty(value = "五笔简码")
    @TableField("wbjm")
    private String wbjm;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "临床类型Id  检验科室必填")
    @TableField("lclx_id")
    private String lclxId;

    @ApiModelProperty(value = "下限年龄")
    @TableField("xxnl")
    private BigDecimal xxnl;

    @ApiModelProperty(value = "下限月份")
    @TableField("xxyf")
    private BigDecimal xxyf;

    @ApiModelProperty(value = "上限年龄")
    @TableField("sxnl")
    private BigDecimal sxnl;

    @ApiModelProperty(value = "上限月份")
    @TableField("sxyf")
    private BigDecimal sxyf;

    @ApiModelProperty(value = "上限")
    @TableField("sx")
    private BigDecimal sx;

    @ApiModelProperty(value = "下限")
    @TableField("xx")
    private BigDecimal xx;

    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String xmmc;
}
