package com.whjk.system.data.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.whjk.system.data.entity.WhjkCjjgwh;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/12/012 14:31
 */
@ApiModel(value = "WhjkPackageRelInfoDto", description = "WhjkPackageRelInfoDto")
@Data
public class WhjkPackageRelInfoDto extends WhjkPackageRelInfo {

    @ApiModelProperty(value = "基础项目编号")
    private String jcxmBh;

    @ApiModelProperty(value = "常见结果维护")
    private List<WhjkCjjgwh> jgList;

    @ApiModelProperty(value = "参考值")
    private String reference;

    @ApiModelProperty(value = "基础项目名称")
    private String jcxmName;

    @ApiModelProperty(value = "单位")
    private String dw;

    @ApiModelProperty(value = "数值上限")
    private String szsx;

    @ApiModelProperty(value = "数值下限")
    private String szxx;

    @ApiModelProperty(value = "显示顺序")
    private String xssx;

    @ApiModelProperty(value = "常见结果id")
    private String jgId;

    @ApiModelProperty(value = "检查状态")
    private String checkStatus;
}
