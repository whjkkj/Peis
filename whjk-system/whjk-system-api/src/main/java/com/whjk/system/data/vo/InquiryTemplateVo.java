package com.whjk.system.data.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.whjk.system.data.annotation.ExcelDropdown;
import lombok.Data;

@Data
public class InquiryTemplateVo {
    @ColumnWidth(100)
    @ExcelProperty(value = "姓名", index = 0)
    private String name;

    @ExcelProperty(value = "身份证号", index = 1)
    private String idCard;

    @ExcelProperty(value = "手机号码", index = 2)
    private String phone;

    @ExcelProperty(value = "婚姻状况", index = 3)
    @ExcelDropdown(value = {"未婚","已婚","离异","丧偶","其他"})
    private String marriage;

    @ExcelProperty(value = "联系地址", index = 4)
    private String address;

}