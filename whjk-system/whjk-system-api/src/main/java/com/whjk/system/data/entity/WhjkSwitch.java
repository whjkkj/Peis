package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 开关
 * </p>
 *
 * @author cs
 * @since 2023-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_switch")
@ApiModel(value="WhjkSwitch对象", description="开关")
public class WhjkSwitch implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "标识")
    @TableField("Identification")
    private String Identification;

    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "类型")
    @TableField("type")
    private String type;

    @ApiModelProperty(value = "是否开启0否1是")
    @TableField("open")
    private String open;
}
