package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 体检回访
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_arrange")
@ApiModel(value="WhjkArrange对象", description="体检回访")
public class WhjkArrange implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "体检登记id")
    @TableField("pe_id")
    private String peId;

    @ApiModelProperty(value = "回访方式1:电话2:线下")
    @TableField("arr_type")
    private String arrType;

    @ApiModelProperty(value = "回访记录内容")
    @TableField("arr_detail")
    private String arrDetail;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "随访通知者 1-本人  2-亲属")
    @TableField("arr_notify")
    private String arrNotify;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "回访时间")
    @TableField("arr_time")
    private LocalDateTime arrTime;

    @ApiModelProperty(value = "回访人")
    @TableField("arr_user")
    private String arrUser;
}
