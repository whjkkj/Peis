package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 检查项目申请单
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_jcxmsqd")
@ApiModel(value="WhjkJcxmsqd对象", description="检查项目申请单")
public class WhjkJcxmsqd implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "编号")
    @TableField("bh")
    private String bh;

    @ApiModelProperty(value = "分类名称")
    @TableField("flmc")
    private String flmc;

    @ApiModelProperty(value = "显示顺序")
    @TableField("xssx")
    private BigDecimal xssx;

    @ApiModelProperty(value = "检查类型（1-医生检查，2-检验项目，3-功能检查）")
    @TableField("jclx")
    private String jclx;

    @ApiModelProperty(value = "标本类型id")
    @TableField("bblx")
    private String bblx;

    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;

    @ApiModelProperty(value = "序号")
    @TableField("xh")
    private String xh;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;


}
