package com.whjk.system.data.po;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "TcZhxmPo", description = "套餐项目信息")
public class TcZhxmPo extends PageDto {

    @ApiModelProperty(value = "套餐ID")
    private String tcId;

    @ApiModelProperty(value = "组合id")
    private String id;

    @ApiModelProperty(value = "组合编号")
    private String zhbh;

    @ApiModelProperty(value = "组合名称")
    private String zhmc;

    @ApiModelProperty(value = "五笔简码")
    private String wbjm;

    @ApiModelProperty(value = "标准价格")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "打折")
    private BigDecimal dz;

    @ApiModelProperty(value = "实际价格")
    private BigDecimal sjjg;
}
