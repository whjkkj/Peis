package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkZdjywhInfo;
import com.whjk.system.data.entity.WhjkZdmx;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "WhjkZdmxDto", description = "WhjkZdmxDto")
public class WhjkZdmxDto extends WhjkZdmx {

    @ApiModelProperty(value = "子项名称")
    private String xmmc;

    @ApiModelProperty(value = "组合名称")
    private String zhmc;

    @ApiModelProperty(value = "科室")
    private String ksmc;

    @ApiModelProperty
    private String checkDoctor;

    @ApiModelProperty(value = "诊断建议从表")
    private List<WhjkZdjywhInfo> infoList;
}
