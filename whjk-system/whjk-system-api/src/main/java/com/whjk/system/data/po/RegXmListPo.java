package com.whjk.system.data.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 登记关联项目列表
 * </p>
 *
 * @author ayuanz
 * @since 2021-07-08
 */
@Data
@ApiModel(value = "RegXmList对象", description = "登记关联项目列表")
public class RegXmListPo {

    @ApiModelProperty(value = "项目id")
    private String xmId;
    @ApiModelProperty(value = "组合编号")
    private String zhbh;
    @ApiModelProperty(value = "组合名称")
    private String zhmc;

    @ApiModelProperty(value = "检查类型（1-医生检查，2-检验项目，3-功能检查）")
    private String jclx;

    @ApiModelProperty(value = "检查状态（0-未检查，1-已检查,2-弃检,3-未保存)")
    private String checkStatus;

    @ApiModelProperty(value = "登记人组合项目关联id")
    private String regItemId;

    @ApiModelProperty(value = "科室id")
    private String ksId;
    @ApiModelProperty(value = "科室名称")
    private String ksmc;
    @ApiModelProperty(value = "小结")
    private String summarize;
    @ApiModelProperty(value = "检查日期")
    private Date checkTime;
    @ApiModelProperty(value = "检查医生")
    private String checkDoctor;
    @ApiModelProperty(value = "检查所见")
    private String auxiliaraResult;
    @ApiModelProperty(value = "检查部位")
    private String checkPart;
    @ApiModelProperty(value = "是否复查0否1是")
    private String reviewType;
    @ApiModelProperty(value = "复查信息")
    private String reviewMsg;
    @ApiModelProperty(value = "总检复查审核状态0确认1未确认")
    private String insReview;
    @ApiModelProperty(value = "建议复查时间")
    private Date reviewTimes;
    @ApiModelProperty(value = "登记流水号")
    private String regSerialNo;
    @ApiModelProperty(value = "Lis对应项目编码")
    private String lisCode;
    @ApiModelProperty(value = "pacs图片标识")
    private String studyNo;
    private String regId;
    @ApiModelProperty(value = "(职业病专用)检查结果类别:1定性，2定量，空或者3描述，4半定量")
    private String type;
    @ApiModelProperty(value = "合格标记0异常1未见异常（通用），胸片检查项目2尘肺样改变，3其他异常9未检查")
    private String mark;

    @ApiModelProperty(value = "职业病对照编码")
    private String zybCode;
    @ApiModelProperty(value = "(职业病专用)检查结果类别:1定性，2定量，空或者3描述，4半定量")
    private String zybType;
    @ApiModelProperty(value = "是否是职业病检查项目0否1是(组合表只针对检查项目)")
    private String zybStatus;
    @ApiModelProperty(value = "审查者医生")
    private String reviewDoctor;
    @ApiModelProperty(value = "是否外送0否1是")
    private String delivery;

    @ApiModelProperty(value = "pacs类别")
    private String pacsDeviceName;

}

