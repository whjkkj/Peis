package com.whjk.system.data.uitls.pdfUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;

import java.util.HashMap;
import java.util.Map;

/**
 * PdfFont
 *
 * @author ayuanz
 * @date 2021/7/28
 */
public class PdfFont {

    public static Map<String,BaseFont> baseFontMap = new HashMap<>(30);

    static {
        try {
            baseFontMap.put("bfChinese",BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED));
            String path = System.getProperty("os.name").toLowerCase().startsWith("win")? "C:\\Windows\\Fonts\\":"/opt/jar/Fonts/";
            baseFontMap.put("LiSu",BaseFont.createFont(path+ "SIMLI.TTF", BaseFont.IDENTITY_H,BaseFont.EMBEDDED)); //隶书
            baseFontMap.put("simsun",BaseFont.createFont(path+ "simsun.ttc,0", BaseFont.IDENTITY_H,BaseFont.EMBEDDED)); //宋体
            baseFontMap.put("msyhbd",BaseFont.createFont(path+ "msyhbd.ttc,0", BaseFont.IDENTITY_H,BaseFont.EMBEDDED));//微软雅黑
            baseFontMap.put("Dengb",BaseFont.createFont(path+ "Dengb.ttf", BaseFont.IDENTITY_H,BaseFont.EMBEDDED));//等线

            //楷体字
//            baseFontMap.put("simkai",BaseFont.createFont("c://windows//fonts//simkai.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //方正舒体
//            baseFontMap.put("FZSTK",BaseFont.createFont("c://windows//fonts//FZSTK.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //方正姚体
//            baseFontMap.put("FZYTK",BaseFont.createFont("c://windows//fonts//FZYTK.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
            //仿宋体
//            baseFontMap.put("SIMFANG",BaseFont.createFont("c://windows//fonts//SIMFANG.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //黑体
//            baseFontMap.put("SIMHEI",BaseFont.createFont("c://windows//fonts//SIMHEI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //华文彩云
//            baseFontMap.put("STCAIYUN",BaseFont.createFont("c://windows//fonts//STCAIYUN.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //华文仿宋
//            baseFontMap.put("STFANGSO",BaseFont.createFont("c://windows//fonts//STFANGSO.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //华文细黑
//            baseFontMap.put("STXIHEI",BaseFont.createFont("c://windows//fonts//STXIHEI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //华文新魏
//            baseFontMap.put("STXINWEI",BaseFont.createFont("c://windows//fonts//STXINWEI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //华文行楷
//            baseFontMap.put("STXINGKA",BaseFont.createFont("c://windows//fonts//STXINGKA.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //华文中宋
//            baseFontMap.put("STZHONGS",BaseFont.createFont("c://windows//fonts//STZHONGS.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
//            //隶书
//            baseFontMap.put("SIMLI",BaseFont.createFont("c://windows//fonts//SIMLI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));
            //宋体&新宋体    (这种字体的输出不了.有问题)
            //BaseFont bfComic = BaseFont.createFont("c://windows//fonts//SIMSUN.TTC", BaseFont.NOT_EMBEDDED, BaseFont.NOT_EMBEDDED);
            //宋体-方正超大字符集（有问题）
            //BaseFont bfComic = BaseFont.createFont("c://windows//fonts//SURSONG.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            //幼圆
//            baseFontMap.put("SIMYOU",BaseFont.createFont("c://windows//fonts//SIMYOU.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取标准样式
     * @return
     */
    public static Font getTitle(Integer size){
        return new Font(baseFontMap.get("bfChinese"), size, Font.UNDEFINED);
    }
    public static Font getTitleCommon(float size, String bfType, Integer fontTyep, BaseColor color){
        Font bfChinese = new Font(baseFontMap.getOrDefault(bfType, baseFontMap.get("bfChinese")), size, fontTyep == null ? Font.NORMAL : fontTyep);
        if(color!=null) {
            bfChinese.setColor(color);
        }
        return bfChinese;
    }

    /**
     * 获取加粗样式
     * @return
     */
    public static Font getBold(Integer size){
        return new Font(baseFontMap.get("bfChinese"), size,Font.BOLD);
    }
    public static Font getBold2(Integer size){
        return new Font(baseFontMap.get("bfChinese"), size,Font.NORMAL);
    }
    /**
     * 获取楷体，size为12的常规样式
     * @return
     */
    public static Font getText(Integer size){
//        return new Font(baseFontMap.get("simkai"), size, Font.UNDEFINED);
        return new Font(baseFontMap.get("bfChinese"), size, Font.UNDEFINED);
    }

    /**
     * 获取楷体，size为12的常规样式
     * @return
     */
    public static Font getTwelveText(){
//        return new Font(baseFontMap.get("simkai"), 12, Font.UNDEFINED);
        return new Font(baseFontMap.get("bfChinese"), 6, Font.UNDEFINED);
    }

    /**
     * 获取楷体，size为14的常规样式
     * @return
     */
    public static Font getFourteenText(){
//        return new Font(baseFontMap.get("simkai"), 14, Font.UNDEFINED);
        return new Font(baseFontMap.get("bfChinese"), 14, Font.UNDEFINED);
    }

    /**
     * 获取常规样式
     * @param baseFont
     * @return
     */
    public static Font getUndefined(BaseFont baseFont){
        return new Font(baseFont, 12, Font.UNDEFINED);
    }

    /**
     * 获取常规样式
     * @param baseFont
     * @return
     */
    public static Font getNormal(BaseFont baseFont){
        return new Font(baseFont, 12, Font.NORMAL);
    }

    /**
     * 获取加粗样式
     * @param baseFont
     * @return
     */
    public static Font getBold(BaseFont baseFont){
        return new Font(baseFont, 12, Font.BOLD);
    }

    /**
     * 获取斜体样式
     * @param baseFont
     * @return
     */
    public static Font getItalic(BaseFont baseFont){
        return new Font(baseFont, 12, Font.ITALIC);
    }

    /**
     * 获取下划线样式
     * @param baseFont
     * @return
     */
    public static Font getUnderline(BaseFont baseFont){
        return new Font(baseFont, 12, Font.UNDERLINE);
    }

    /**
     * 获取删除线样式
     * @param baseFont
     * @return
     */
    public static Font getStrikethru(BaseFont baseFont){
        return new Font(baseFont, 12, Font.STRIKETHRU);
    }

    /**
     * 获取下划线加删除线线样式
     * @param baseFont
     * @return
     */
    public static Font getDefaultsize(BaseFont baseFont){
        return new Font(baseFont, 12, Font.DEFAULTSIZE);
    }


    /**
     * 获取常规样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getUndefined(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.UNDEFINED);
    }

    /**
     * 获取常规样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getNormal(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.NORMAL);
    }

    /**
     * 获取加粗样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getBold(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.BOLD);
    }

    /**
     * 获取斜体样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getItalic(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.ITALIC);
    }

    /**
     * 获取下划线样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getUnderline(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.UNDERLINE);
    }

    /**
     * 获取删除线样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getStrikethru(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.STRIKETHRU);
    }

    /**
     * 获取下划线加删除线线样式
     * @param baseFont
     * @param size
     * @return
     */
    public static Font getDefaultsize(BaseFont baseFont,int size){
        return new Font(baseFont, size, Font.DEFAULTSIZE);
    }

    public static Font getTwelveText2() {
        return new Font(baseFontMap.get("bfChinese"), 8.5f, Font.BOLD);
    }

    public static Font getTwelveText1() {
        return new Font(baseFontMap.get("bfChinese"), 8.5f, Font.BOLD);
    }
}
