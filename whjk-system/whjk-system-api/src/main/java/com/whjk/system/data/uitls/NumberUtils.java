package com.whjk.system.data.uitls;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * <p>
 * 数字工具
 * </p>
 *
 * @author hl
 * @since 2023/7/21/021 17:27
 */
public class NumberUtils {
    /**
     * 判断一个字符串是不是整数、浮点数、科学计数（正则表达式）
     *
     * @param str
     * @return
     */
    public static boolean calculation(String str) {
        if (null == str || "".equals(str)) {
            return false;
        }
        String regx = "[+-]*\\d+\\.?\\d*[Ee]*[+-]*\\d+";
        Pattern pattern = Pattern.compile(regx);
        boolean isNumber = pattern.matcher(str).matches();
        if (isNumber) {
            return isNumber;
        }
        regx = "^[-\\+]?[.\\d]*$";
        pattern = Pattern.compile(regx);
        return pattern.matcher(str).matches();
    }

    /**
     * 判断值是否在这个范围
     *
     * @param low   低值
     * @param high  高值
     * @param value 值
     * @return 1正常2偏高3偏低
     */
    public static Integer range(String value, String low, String high) {
        //有一个不满足是数值型，则返回正常,不做判断
        if (!(calculation(value) && calculation(low) && calculation(high))) {
            return 1;
        }
        BigDecimal valueT = new BigDecimal(value);
        BigDecimal lowT = new BigDecimal(low);
        BigDecimal highT = new BigDecimal(high);
        if (valueT.compareTo(lowT) < 0) {
            System.out.println("偏低");
            return 3;
        }
        if (valueT.compareTo(highT) > 0) {
            System.out.println("偏高");
            return 2;
        }
        return 1;
    }
}
