package com.whjk.system.data.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/13/013 16:27
 */
@Data
@Accessors(chain = true)
public class PersonDto extends PageDto {
    @ApiModelProperty(value = "体检日期")
    private String peDate;
    @ApiModelProperty(value = "体检号")
    private String regSerialNo;
    @ApiModelProperty(value = "身份证")
    private String idCard;
    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "体检状态(0:预约,1:登记,2:检查中,3:已终检)")
    private String peStatus;
    @ApiModelProperty(value = "登记类型(1:个人,2:团队)")
    private String regType;
    @ApiModelProperty(value = "流程（0-未开始1-已登记,2-已缴费,3-全科结果录入4-已回收指引单,5-已总检 6-报告打印 7-用户接收报告）")
    private String process;
    @ApiModelProperty(value = "单位id")
    private String teamId;
    @ApiModelProperty(value = "分组id")
    private String groupId;



}
