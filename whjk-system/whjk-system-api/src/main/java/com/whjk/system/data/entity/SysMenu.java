package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 系统菜单表
 * </p>
 *
 * @author hl
 * @since 2023-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
@ApiModel(value = "SysMenu对象", description = "系统菜单表")
public class SysMenu extends PageDto implements Serializable {

    private static final Long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "父id")
    @TableField("pid")
    private String pid;

    @NotBlank(message = "名称不能为空", groups = {review.class})
    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;

    @NotBlank(message = "编码不能为空", groups = {review.class})
    @ApiModelProperty(value = "编码")
    @TableField("code")
    private String code;

    @ApiModelProperty(value = "菜单类型（字典 0目录 1菜单 2按钮）")
    @TableField("type")
    private Integer type;

    @ApiModelProperty(value = "图标")
    @TableField("icon")
    private String icon;

    @ApiModelProperty(value = "路由地址")
    @TableField("router")
    private String router;

    @ApiModelProperty(value = "组件地址")
    @TableField("component")
    private String component;

    @ApiModelProperty(value = "权限标识")
    @TableField("permission")
    private String permission;

    // @NotBlank(message = "应用分类不能为空", groups = {review.class})
    @ApiModelProperty(value = "应用分类（应用编码）")
    @TableField("application")
    private String application;

    @ApiModelProperty(value = "打开方式（字典 0无 1组件 2内链 3外链）")
    @TableField("open_type")
    private Integer openType;

    @ApiModelProperty(value = "是否可见（Y-是，N-否）")
    @TableField("visible")
    private String visible;

    @ApiModelProperty(value = "链接地址")
    @TableField("link")
    private String link;

    @ApiModelProperty(value = "重定向地址")
    @TableField("redirect")
    private String redirect;

    @ApiModelProperty(value = "权重（字典 1系统权重 2业务权重）")
    @TableField("weight")
    private Integer weight;

    @ApiModelProperty(value = "排序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty(value = "状态（字典 0正常 1停用 2删除）")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除标记,1:已删除,0:正常")
    @TableField("del_flag")
    private String delFlag;

    @TableField(exist = false)
    private List<SysMenu> childList;
}
