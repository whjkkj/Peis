package com.whjk.system.data.uitls.pdfUtils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * PdfTable
 *
 * @author ayuanz
 * @date 2021/7/28
 */
public class PdfTable {

    //边框 分别对应左右上下
    public static float[] borX = {0, 0, 0, 1};
    public static float[] borSX = {0, 0, 1f, 1f};
    public static float[] borY = {0, 1f, 0, 0};
    public static float[] borX1 = {0, 0, 0, 0.5f};
    public static float[] borS = {0, 0, 1, 0};
    public static float[] borZS = {1, 0, 1, 0};
    public static float[] borZYS = {1, 1, 1, 0};
    public static float[] borZSX = {1, 0, 1, 1};
    public static float[] borZX = {1, 0, 0, 1};
    public static float[] borZYX = {1, 1, 0, 1};
    public static float[] borYS = {0, 1, 1, 0};
    public static float[] borZ = {1, 0, 0, 0};
    public static float[] bor = {1, 1, 1, 1};
    public static float[] borSJC = {0, 0, 2, 0};
    public static float[] notBor = {0, 0, 0, 0};
    public static float[] padding1 = {4, 2};
    public static float[] padding2 = {2, 2};
    public static float[] padding4 = {1, 1};
    public static float[] paddingNot = {0, 0};
    public static float[] padding3 = {0, 2};
    public static float[] padding6 = {0, 5};
    public static float[] padding5 = {0, 1};
    public static float[] padding16 = {16, 0};
    public static float[] paddingTen = {10, 5};
    public static float[] padding7= {10, 0, 6, 0};
    public static float[] padding8 = {1, 1, 2, 1};
    public static float[] paddingFive = {10, 10};
    public static float[] paddingS = {0, 10};
    public static float[] paddingbig = {50, 50};
    public static float[] paddingF = {0, 4.5F};
    public static float[] paddingX = {20, 0, 1.5f, 5};
    public static float[] paddingZS = {20, 0, 5, 0};

    public static float[] paddingImgBig = {200, 0, 5, 0};
    public static float[] paddingM = {0, 0, 1.5f, 5};
    public static float[] padding10 = {0, 0,10,10};

    public static float[] padding55 = {0, 0,5,5};
    //字间距
    public static float[] leading = {2f,1.5f};
    //灰
    public static BaseColor baseColor = new BaseColor(235, 235, 235);
    //红
    public static BaseColor baseColorRed = new BaseColor(255, 0, 128);
    //青
    public static BaseColor baseColorBlue = new BaseColor(231, 253, 253);
    //浅红
    public static BaseColor baseColorLightRed = new BaseColor(255, 230, 230);
    //灰
    public static BaseColor baseColorLightGray = new BaseColor(211, 208, 199);
    public static  BaseColor darkBlack = new BaseColor(85, 85, 85);
    public static  BaseColor baseColorDarkBlue = new BaseColor(18, 53, 122);
    //眉脚线 浅蓝色
    public static  BaseColor baseColorBabyBlue = new BaseColor(145, 201, 238);
    public static  BaseColor baseColorhuis = new BaseColor(192, 192, 192);
    public static  BaseColor Blue = new BaseColor(6, 35, 89);

    /**
     * 创建单元格(指定字体)
     *
     * @param value
     * @param font
     * @return
     */
    public static PdfPCell createCell(String value, Font font) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPhrase(new Phrase(value, font));
        return cell;
    }

    /**
     * 无边框，并指定字体
     *
     * @param value
     * @param font
     * @return
     */
    public static PdfPCell createCellNotBor(String value, Font font) {
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(value, font));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        return cell;
    }

    /**
     * 创建单元格(指定字体) 并指定边框和上下边距
     *
     * @param value
     * @param font
     * @return
     */
    public static PdfPCell createCellDiyBor(String value, Font font, float[] borderWidth, float[] paddingSize) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPhrase(new Phrase(value, font));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(paddingSize[0]);
        cell.setPaddingBottom(paddingSize[1]);
        cell.setColspan(1);
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平..）
     *
     * @param value
     * @param font
     * @param align
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平..）
     *
     * @param value
     * @param font
     * @param align
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align, BaseColor color) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setBackgroundColor(color);
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、单元格跨x列合并）
     *
     * @param value
     * @param font
     * @param align
     * @param colspan
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align, int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value, font));
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、单元格跨x列合并）
     *
     * @param value 单元格值
     * @param fontSize 字体大小
     * @param align 样式
     * @param rowspan 行合并
     * @param colspan 列合并
     * @return
     */
    public static PdfPCell createCell(String value, int fontSize, int align,int rowspan,int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setRowspan(rowspan);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value, PdfFont.getText(fontSize)));
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、单元格跨x列合并）
     *
     * @param value 单元格值
     * @param fontSize 字体大小
     * @param align 样式
     * @param rowspan 行合并
     * @param colspan 列合并
     * @param height 行高
     * @return
     */
    public static PdfPCell createCell(String value, int fontSize, int align,int rowspan,int colspan,int height) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setRowspan(rowspan);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value, PdfFont.getText(fontSize)));
        cell.setMinimumHeight(height);
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、单元格跨x列合并）
     *
     * @param value
     * @param font
     * @param align
     * @param colspan
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align, int colspan, BaseColor color) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value, font));
        cell.setBackgroundColor(color);
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、单元格跨x列合并、设置单元格内边距）
     *
     * @param value
     * @param font
     * @param align
     * @param colspan
     * @param boderFlag
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align, int colspan, boolean boderFlag) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value, font));
        cell.setPadding(3.0f);
        if (!boderFlag) {
            cell.setBorder(0);
            cell.setPaddingTop(15.0f);
            cell.setPaddingBottom(8.0f);
        } else if (boderFlag) {
            cell.setBorder(0);
            cell.setPaddingTop(0.0f);
            cell.setPaddingBottom(15.0f);
        }
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平..、边框宽度：0表示无边框、内边距）
     *
     * @param value
     * @param font
     * @param align
     * @param borderWidth
     * @param colspan
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align, float[] borderWidth, int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setColspan(colspan);
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平..、边框宽度：0表示无边框、内边距）
     *
     * @param value
     * @param font
     * @param align
     * @param borderWidth
     * @param paddingSize
     * @param colspan
     * @return
     */
    public static PdfPCell createCell(String value, Font font, int align, float[] borderWidth, float[] paddingSize, int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(paddingSize[0]);
        cell.setPaddingBottom(paddingSize[1]);
        cell.setColspan(colspan);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(1);
        return cell;
    }

    public static PdfPCell createCell(Image value, Integer size, Integer align, float[] borderWidth) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setImage(value);
        //cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(1);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, BaseColor color) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(1);
        cell.setBackgroundColor(color);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, BaseColor color, int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        cell.setBackgroundColor(color);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        Font text = PdfFont.getText(size);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[2]);
        cell.setPaddingBottom(padding[3]);
        cell.setPaddingLeft(padding[0]);
        cell.setPaddingRight(padding[1]);
        cell.setColspan(1);
        return cell;
    }
    public static PdfPCell createCell(Image image, Integer size, Integer align, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setImage(image);
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[2]);
        cell.setPaddingBottom(padding[3]);
        cell.setPaddingLeft(padding[0]);
        cell.setPaddingRight(padding[1]);
        cell.setColspan(1);
        cell.setFixedHeight(30);
        return cell;
    }
    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, float[] padding,float[] leading) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[2]);
        cell.setPaddingBottom(padding[3]);
        cell.setPaddingLeft(padding[0]);
        cell.setPaddingRight(padding[1]);
        cell.setColspan(1);
        cell.setLeading(leading[0],leading[1]);
        return cell;
    }
    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, float[] padding, BaseColor color) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[2]);
        cell.setPaddingBottom(padding[3]);
        cell.setPaddingLeft(padding[0]);
        cell.setPaddingRight(padding[1]);
        cell.setColspan(1);
        cell.setBackgroundColor(color);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCellHeight(String value, Integer size, Integer align, int colspan, float[] borderWidth, float[] padding,float height) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        return cell;
    }
    public static PdfPCell createCellPadding(String value, Integer size, Integer align, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[2]);
        cell.setPaddingBottom(padding[3]);
        cell.setPaddingLeft(padding[0]);
        cell.setPaddingRight(padding[1]);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCellBold(String value, Integer size, Integer align, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getBold(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        return cell;
    }

    /**
     * 字体加粗上下间距1
     * @param value
     * @param size
     * @param align
     * @param colspan
     * @param borderWidth
     * @param padding
     * @return
     */
    public static PdfPCell createCellBoldLeading(String value, Integer size, Integer align, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getBold(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        //字体上下间距
        cell.setLeading(1,1);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCellHeight(String value, Integer size, Integer align, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(1);
        cell.setFixedHeight(30f);
        return cell;
    }
    public static PdfPCell createCellHeight(String value, Font font, Integer align, int height, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        return cell;
    }
    public static PdfPCell createCellHeight(String value, Integer size, Integer align, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        cell.setFixedHeight(30f);
        return cell;
    }

    public static PdfPCell createCellHeight(String value, Integer size, Integer align, int height, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, int colspan,Integer top) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(top);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, int colspan,Integer top,Integer left) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(top);
        cell.setPaddingBottom(5);
        cell.setPaddingLeft(left);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCell(String value, Integer size, Integer align, float[] borderWidth, int colspan, BaseColor color) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        cell.setBackgroundColor(color);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(1);
        return cell;
    }

    public static PdfPCell createCell(String value, Integer size, Integer align, Integer colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); //垂直居中
        cell.setHorizontalAlignment(align); //水平居中
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan); //合并单元格
        return cell;
    }
    public static PdfPCell createCellSizeColor(String value, Integer size, Integer align, float[] borderWidth, float[] padding, BaseColor color) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        Font text = PdfFont.getText(size);
        text.setColor(color);
        cell.setPhrase(new Phrase(value, text));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[2]);
        cell.setPaddingBottom(padding[3]);
        cell.setPaddingLeft(padding[0]);
        cell.setPaddingRight(padding[1]);
        cell.setColspan(1);
        return cell;
    }
    public static PdfPCell createCellHeight(String value, Integer size, Integer align, Integer colspan, int height) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        return cell;
    }

    /**
     * 下边框
     *
     * @param value
     * @return
     */
    public static PdfPCell createCellX(String value) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPhrase(new Phrase(value, PdfFont.getText(15)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(1);
        cell.setPaddingTop(4);
        cell.setPaddingBottom(4);
        cell.setColspan(1);
        return cell;
    }
    public static PdfPCell createCellX(String value,int size) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(1);
        cell.setPaddingTop(4);
        cell.setPaddingBottom(4);
        cell.setColspan(1);
        return cell;
    }
    /**
     * 没有边框
     *
     * @param value
     * @return
     */
    public static PdfPCell createCellNotBor(String value) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPhrase(new Phrase(value, PdfFont.getText(15)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(4);
        cell.setPaddingBottom(4);
        cell.setColspan(1);
        return cell;
    }
    public static PdfPCell createCellNotBor(String value,Integer align,Integer colspan) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(15)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(4);
        cell.setPaddingBottom(4);
        cell.setColspan(colspan);
        return cell;
    }
    public static PdfPCell createCellNotBor(String value,Integer align) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(12)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(4);
        cell.setPaddingBottom(4);
        cell.setColspan(1);
        return cell;
    }
    public static PdfPCell createCellNotBor(String value,Integer align,BaseColor color,Integer paddingLeft) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getText(10)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(10);
        cell.setPaddingBottom(10);
        cell.setPaddingLeft(paddingLeft);
        cell.setColspan(1);
        cell.setBackgroundColor(color);
        return cell;
    }
    public static PdfPCell createCellNotBor(String value, float size, String bfType, Integer align) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        Phrase elements = new Phrase(value, PdfFont.getTitleCommon(size, bfType, Font.NORMAL, null));
        cell.setPhrase(elements);
        cell.setLeading(2f, 1.174f); //行间距
        //cell.setSpaceCharRatio(0.05f); //字间距
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(4);
        cell.setPaddingBottom(4);
        cell.setColspan(1);
        return cell;
    }
    public static PdfPCell createCellImage(Image value, Integer colspan, Integer align, float[] borderWidth, Integer borderColor) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setImage(value);
        //cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        if(borderColor!=null)cell.setBorderColor(PdfTable.baseColorBabyBlue);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        return cell;
    }
    /**
     * 创建指定列宽比
     *
     * @param widths
     * @return
     */
    public static PdfPTable createTable(int[] widths) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(PageSize.A4.getWidth() - 100);
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    public static PdfPTable createTable(List<Integer> widths) {
        Integer[] array = widths.toArray(new Integer[widths.size()]);
        int[] arrarWidth = Arrays.stream(array).mapToInt(Integer::valueOf).toArray();
        PdfPTable table = new PdfPTable(widths.size());
        try {
            table.setTotalWidth(PageSize.A4.getHeight()-10);
            table.setWidths(arrarWidth);
            table.setLockedWidth(true);
            table.setWidthPercentage(100f);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    /**
     * 创建指定列宽比
     *
     * @param widths
     * @return
     */
    public static PdfPTable createTableTotalWidth(int[] widths,Integer totalWidth) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(PageSize.A4.getWidth() - totalWidth);
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }
    public static PdfPTable createTable(int[] widths,Integer height) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(PageSize.A4.getWidth() - 100);
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(height);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }
    /**
     * 创建指定列宽比
     *
     * @param widths
     * @return
     */
    public static PdfPTable createTableSQD(int[] widths) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(142);
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    public static PdfPTable createJKZTable(int[] widths) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(PageSize.ID_1.getWidth() - 10);
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    public static PdfPTable createJKZMTable(int[] widths) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(PageSize.ID_1.getWidth());
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    public static PdfPTable createJFD(int[] widths) {
        PdfPTable table = new PdfPTable(widths.length);
        try {
            table.setTotalWidth(PageSize.A5.getWidth());
            table.setWidths(widths);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setFixedHeight(-10);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    /**
     * 旋转PDF文件
     * @param sourceFile 源PDF文件路径
     * @param targetFile 目标PDF文件路径
     * @param angle 旋转角度
     * @param rotatedPageNums 需要旋转的页码
     */
    public static void rotate(String sourceFile, String targetFile, int angle, List<Integer> rotatedPageNums) {
        PdfReader reader = null;
        Document document = null;
        FileOutputStream outputStream = null;
        try {
            // 读取源文件
            reader = new PdfReader(sourceFile);
            // 创建新的文档
            document = new Document();
            // 创建目标PDF文件
            outputStream = new FileOutputStream(targetFile);
            PdfCopy pdfCopy = new PdfSmartCopy(document, outputStream);

            // 获取源文件的页数
            int pages = reader.getNumberOfPages();
            document.open();
            PdfDictionary pdfDictionary;

            // 注意此处的页码是从1开始
            for (int page = 1; page <= pages; page++) {
                pdfDictionary = reader.getPageN(page);

                if (null == rotatedPageNums || rotatedPageNums.isEmpty()) {
                    pdfDictionary.put(PdfName.ROTATE, new PdfNumber(angle));
                } else if (rotatedPageNums.contains(page)) {
                    pdfDictionary.put(PdfName.ROTATE, new PdfNumber(angle));
                }

                pdfCopy.addPage(pdfCopy.getImportedPage(reader, page));
            }
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }

            if (document != null) {
                document.close();
            }

            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 旋转PDF文件
     * @param sourceFile 源PDF文件路径
     * @param targetFile 目标PDF文件路径
     * @param angle 旋转角度
     * @param fromPageNum 起始页码
     * @param toPageNum 结束页码
     */
    public static void rotate(String sourceFile, String targetFile, int angle, int fromPageNum, int toPageNum) {
        PdfReader reader = null;
        Document document = null;
        FileOutputStream outputStream = null;
        try {
            // 读取源文件
            reader = new PdfReader(sourceFile);
            // 创建新的文档
            document = new Document();
            // 创建目标PDF文件
            outputStream = new FileOutputStream(targetFile);
            PdfCopy pdfCopy = new PdfSmartCopy(document, outputStream);

            // 获取源文件的页数
            int pages = reader.getNumberOfPages();
            document.open();
            PdfDictionary pdfDictionary;

            // 注意此处的页码是从1开始
            for (int page = 1; page <= pages; page++) {
                pdfDictionary = reader.getPageN(page);
                // 如果页面是在起始页码和结束页码之间的，则进行旋转
                if (page >= fromPageNum && page <= toPageNum) {
                    pdfDictionary.put(PdfName.ROTATE, new PdfNumber(angle));
                }

                pdfCopy.addPage(pdfCopy.getImportedPage(reader, page));
            }
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }

            if (document != null) {
                document.close();
            }

            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 旋转PDF文件中的所有横版
     * @param sourceFile 源PDF文件路径
     * @param targetFile 目标PDF文件路径
     * @param angle 旋转角度
     */
    public static void rotateHorizontal(String sourceFile, String targetFile, int angle) {
        PdfReader reader = null;
        Document document = null;
        FileOutputStream outputStream = null;
        try {
            // 读取源文件
            reader = new PdfReader(sourceFile);
            // 创建新的文档
            document = new Document();
            // 创建目标PDF文件
            outputStream = new FileOutputStream(targetFile);
            PdfCopy pdfCopy = new PdfSmartCopy(document, outputStream);

            // 获取源文件的页数
            int pages = reader.getNumberOfPages();
            document.open();
            PdfDictionary pdfDictionary;

            // 注意此处的页码是从1开始
            for (int page = 1; page <= pages; page++) {
                pdfDictionary = reader.getPageN(page);

                // 根据页面的宽度
                float pageWidth = reader.getPageSize(page).getWidth();
                if (pageWidth > 600F) {
                    pdfDictionary.put(PdfName.ROTATE, new PdfNumber(angle));
                }

                pdfCopy.addPage(pdfCopy.getImportedPage(reader, page));
            }
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }

            if (document != null) {
                document.close();
            }

            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 旋转PDF文件中的所有竖版
     * @param sourceFile 源PDF文件路径
     * @param targetFile 目标PDF文件路径
     * @param angle 旋转角度
     */
    public static void rotateVertical(String sourceFile, String targetFile, int angle) {
        PdfReader reader = null;
        Document document = null;
        FileOutputStream outputStream = null;
        try {
            // 读取源文件
            reader = new PdfReader(sourceFile);
            // 创建新的文档
            document = new Document();
            // 创建目标PDF文件
            outputStream = new FileOutputStream(targetFile);
            PdfCopy pdfCopy = new PdfSmartCopy(document, outputStream);

            // 获取源文件的页数
            int pages = reader.getNumberOfPages();
            document.open();
            PdfDictionary pdfDictionary;

            // 注意此处的页码是从1开始
            for (int page = 1; page <= pages; page++) {
                pdfDictionary = reader.getPageN(page);

                // 根据页面的高度
                float pageHeight = reader.getPageSize(page).getHeight();
                if (pageHeight > 600F) {
                    pdfDictionary.put(PdfName.ROTATE, new PdfNumber(angle));
                }

                pdfCopy.addPage(pdfCopy.getImportedPage(reader, page));
            }
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }

            if (document != null) {
                document.close();
            }

            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * @Author lcc
     * @Description 设置页面横向
     * @Date  2022/11/09 10:26
     * @param
     * @return
     **/
    public static  void setPageSizeHen(Document document){
        //横向
        Rectangle pageSize = new Rectangle(PageSize.A4.getHeight(), PageSize.A4.getWidth());
        pageSize.rotate();
        document.setPageSize(pageSize);
    }
    /**
     * @Author lcc
     * @Description 设置页面竖向
     * @Date  2022/11/09 10:26 10:26
     * @param
     * @return
     **/
    public static  void setPageSizeShu(Document document){
        //竖向
        Rectangle pageSize = new Rectangle(PageSize.A4.getWidth(), PageSize.A4.getHeight());
        pageSize.rotate();
        document.setPageSize(pageSize);
    }

    public static PdfPCell createCellHeight1(String value, Integer size, Integer align, int height, int colspan, float[] borderWidth, float[] padding) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, PdfFont.getTwelveText1()));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(padding[0]);
        cell.setPaddingBottom(padding[1]);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        return cell;
    }
    public static PdfPCell createCell(String value, Font font, int align, int rowspan,int colspan,int height) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setRowspan(rowspan);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        return cell;
    }
    public static PdfPCell createCell(String value, Font font, int align, int rowspan,int colspan,int height,float[] borderWidth) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setRowspan(rowspan);
        cell.setColspan(colspan);
        cell.setFixedHeight(height);
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        return cell;
    }

    public static PdfPCell createCell(String value, Font font, int align,float height) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setMinimumHeight(height);
        return cell;
    }
    public static PdfPCell createCellImage2(Image value, Integer colspan,Integer rowspan, Integer align, float[] borderWidth, Integer borderColor) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setImage(value);
        //cell.setPhrase(new Phrase(value, PdfFont.getText(size)));
        cell.setBorderWidthLeft(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(5);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        return cell;
    }
    public static PdfPCell createCell(String value, Font font, int align, float[] borderWidth
            , float[] paddingSize, int colspan, Integer BorderColorBottom) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(paddingSize[0]);
        cell.setPaddingBottom(paddingSize[1]);
        if(BorderColorBottom!=null)cell.setBorderColor(PdfTable.baseColorBabyBlue);
        cell.setColspan(colspan);
        return cell;
    }
}
