package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 诊断结果报告
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_package_rel_info")
@ApiModel(value="WhjkPackageRelInfo对象", description="诊断结果报告")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WhjkPackageRelInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "登记关联项目表id")
    @TableField("rel_id")
    private String relId;

    @ApiModelProperty(value = "基础项目id")
    @TableField("jcxm_id")
    private String jcxmId;

    @ApiModelProperty(value = "登记id")
    @TableField("pe_id")
    private String peId;

    @ApiModelProperty(value = "组合项目id")
    @TableField("zhxm_id")
    private String zhxmId;

    @ApiModelProperty(value = "检查部位")
    @TableField("check_part")
    private String checkPart;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "是否阳性（0-否，1-是）")
    @TableField("is_positive")
    private Integer isPositive;

    @ApiModelProperty(value = "是否正常（0-正常 1-不正常 2-偏高 3-偏低4-高于极限 5低于极限 6-高-7低）")
    @TableField("is_abnormal")
    private Integer isAbnormal;

    @ApiModelProperty(value = "检查结果")
    @TableField("check_result")
    private String checkResult;
}
