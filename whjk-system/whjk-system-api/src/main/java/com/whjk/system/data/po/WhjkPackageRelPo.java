package com.whjk.system.data.po;

import com.whjk.system.data.entity.WhjkPackageRel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/5/005 9:40
 */
@Data
@ApiModel(value = "WhjkPackageRelVo", description = "WhjkPackageRelVo")
public class WhjkPackageRelPo extends WhjkPackageRel {
    @ApiModelProperty(value = "检查类型（1-医生检查，2-检验项目lis，3-功能检查pacs）")
    private String jclx;
    @ApiModelProperty(value = "项目名称")
    private String zhmc;
    @ApiModelProperty(value = "组合编号")
    private String zhbh;
    @ApiModelProperty(value = "提示")
    private String tsxx;
    @ApiModelProperty(value = "套餐名称")
    private String tcmc;

}
