package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_zdjywh_rule")
@ApiModel(value = "WhjkZdjywhRule对象", description = "逻辑规则")
public class WhjkZdjywhRule extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @TableField("zdjywh_id")
    private String zdjywhId;

    @NotNull(message = "组合项目id不能为空", groups = {BaseDto.add.class})
    @TableField("zhxm_id")
    @ApiModelProperty(value = "组合项目id")
    private String zhxmId;

    @NotNull(message = "子项id不能为空", groups = {BaseDto.add.class})
    @TableField("jcxm_id")
    @ApiModelProperty(value = "子项id")
    private String jcxmId;

    @NotNull(message = "规则不能为空", groups = {BaseDto.add.class})
    @TableField("type")
    @ApiModelProperty(value = "规则 EQ=,NE!=,GT>,GE>=,LT<,LE<=,LIKE包含")
    private String type;

    @NotNull(message = "规则值不能为空", groups = {BaseDto.add.class})
    @TableField("regular_value")
    @ApiModelProperty(value = "规则值")
    private String regularValue;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;


}
