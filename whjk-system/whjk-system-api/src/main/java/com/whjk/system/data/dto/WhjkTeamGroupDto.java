package com.whjk.system.data.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.whjk.system.data.entity.WhjkTeamGroup;
import com.whjk.system.data.entity.WhjkTeamGroupItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

@Data
@ApiModel(value = "WhjkTeamGroupDto", description = "whjkTeamGroupDto")
public class WhjkTeamGroupDto extends WhjkTeamGroup {

    @ApiModelProperty(value = "组下项目")
    private List<WhjkTeamGroupItem> itemList;

    @ApiModelProperty(value = "单位名称")
    @TableField(exist = false)
    private String teamName;

}
