package com.whjk.system.data.vo;


import com.deepoove.poi.data.PictureType;
import com.whjk.system.data.annotation.DocParam;

import com.whjk.system.data.uitls.DocTestType;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 指引单
 */
@Data
public class SVo {
    /*
      体检人基本属性
    */
    /** 体检人头像 */
    @DocParam(type = DocTestType.IMG_URL, imgSize = {120, 140}, imgType = PictureType.PNG)
    private Object headImg;
    private Object idCard;
    private Object sex;
    private Object mobile;
    private Object hospitalName;
    private Object orderTotal;
    /** 体检人条码 */
    @DocParam(type = DocTestType.BASE64, imgSize = {150, 50}, imgType = PictureType.PNG)
    private Object barCodeImg;
    /** 姓名 */
    private Object name;
    /** 昵称 */
    private Object nickname;
    /** 年龄 */
    private Object age;
    /**  */
    private Object date;
    /** 打印日期 */
    private Object printDate;
    /** 体检编号 */
    private Object testNum;
    /** 体检人单位名称 */
    private Object unitName;
    /** 婚否 */
    private Object isMarry;
    /** 复检项目名称 */
    private Object reviewName;

    /*
      常规检查，体检表，基本属性
    */
    /** 过敏史 */
    private Object guoMinShi;
    /** 既往史 */
    private Object jiWangShi;
    /** 身高 */
    private Object height;
    /** 体重 */
    private Object weight;
    /** 血压 */
    private Object bloodPressure;
    /** 舒张压 */
    private Object bloodLow;
    /** 收缩压 */
    private Object bloodHight;
    /** 辨色力 */
    private Object colorVision;
    /** 左视力 */
    private Object nakedVisionLeft;
    /** 右视力 */
    private Object nakedVisionRight;
    /** 心率，脉搏，脉率 */
    private Object heartRate;
    /** 内科 */
    private Object neiKe;
    /** 外科 */
    private Object waiKe;
    /** 其它 */
    private Object other;
    /** 辅助检查 */
    private Object fuZhuJianCha;
    /** 脊柱四肢 */
    private Object jiZhuSiZhi;
    /** 淋巴 */
    private Object linBa;
    /** 心肺 */
    private Object xinFei;
    /** 心 */
    private Object xin;
    /** 肺 */
    private Object fei;
    /** 肝 */
    private Object gan;
    /** 脾 */
    private Object pi;
    /** 皮肤 */
    private Object piFu;
    /** 四肢 */
    private Object siZhi;
    /** 外科其他 */
    private Object waiKeQiTa;
    /** 体检结论 */
    private Object checkOpinion;
    /** 腹部 */
    private Object fuBu;
    /** 推荐单位 */
    private Object tuiJianUnit;
    /** DR胸片 */
    private Object dr;


    /*
      CT，DR，彩超，影像科，报告基本属性
    */
    /** 申请科室 */
    private Object sectionOffice;
    /** 申请医生 */
    private Object doctor;
    /** 标本种类名称 */
    private Object specimen;
    /** 报告编号 */
    private Object reportNum;
    /** 出报告医生 */
    private Object reportDoctor;
    /** 审核医生 */
    private Object reviewDoctor;
    /** 申请时间 */
    private Object createTime;
    /** 检验时间 （检验科报告使用，日期和时间）*/
    private Object inspectTime;
    /** 检查日期（CT，DR，彩超，影像报告使用，只日期） */
    private Object inspectDate;
    /** 检查部位 */
    private Object projectNameTemplate;
    /** 检查方法 */
    private Object checkMethod;
    /** 影像所见 */
    private Object inspectResult;
    /** 诊断意见 */
    private Object opinion;

    private boolean showArray;

    private List<goodVo> goods;
    /** 检验科报告，检验结果明细集合 */
    private List<InspectVo> tableMonitoring;

}
