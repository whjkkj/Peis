package com.whjk.system.data.vo;

import com.whjk.system.data.dto.WhjkPackageRelInfoDto;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.whjk.system.data.entity.WhjkZdjywhInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "MedicalAdviceVo", description = "医生异常建议")
public class MedicalAdviceVo {

    @ApiModelProperty(value = "诊断建议id")
    private String zdjyId;

    @ApiModelProperty(value = "科普说明")
    private String kpsm;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "建议内容")
    private List<WhjkZdjywhInfo> jy;


}


