package com.whjk.system.data.po;

import com.whjk.system.data.entity.WhjkPackageRelInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/11/011 11:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WhjkPackageRelInfoPo extends WhjkPackageRelInfo {
    @ApiModelProperty(value = "项目名称")
    private String xmmc;
    @ApiModelProperty(value = "组合编号")
    private String xmbh;

    @ApiModelProperty(value = "dw")
    private String dw;
    @ApiModelProperty(value = "szxx")
    private String szxx;
    @ApiModelProperty(value = "szsx")
    private String szsx;
}
