package com.whjk.system.data.uitls;

public enum DocTestType {
    /**
     * 文本
     */
    TEXT(1, "文本"),
    /**
     * 图片base64
     */
    BASE64(2, "BASE64"),

    IMG_URL(3, "图片本地地址");

    public final int code;
    public final String type;

    DocTestType(int code, String type) {
        this.code = code;
        this.type = type;
    }

    public int getCode() {
        return this.code;
    }

    public String getType() {
        return this.type;
    }


}
