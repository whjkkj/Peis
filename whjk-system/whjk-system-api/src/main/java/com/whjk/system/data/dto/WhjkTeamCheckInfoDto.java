package com.whjk.system.data.dto;


import com.whjk.system.data.entity.WhjkTeamCheckInfo;
import com.whjk.system.data.entity.WhjkTeamGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

@Data
@ApiModel(value = "WhjkTeamCheckInfoDto", description = "whjkTeamGroupDto")
public class WhjkTeamCheckInfoDto extends WhjkTeamCheckInfo {

    @ApiModelProperty(value = "单位下父分组")
    private List<WhjkTeamGroup> groupList;

}
