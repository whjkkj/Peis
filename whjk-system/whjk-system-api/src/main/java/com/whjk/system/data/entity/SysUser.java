package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author hl
 * @since 2023-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
@ApiModel(value = "SysUser对象", description = "系统用户表")
public class SysUser extends PageDto implements Serializable {

    private static final Long serialVersionUID = 1L;
    @NotBlank(message = "主键不能为空", groups = {update.class})
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;
    @NotBlank(message = "账号不能为空", groups = {review.class})
    @ApiModelProperty(value = "账号")
    @TableField("account")
    private String account;
    @NotBlank(message = "密码不能为空", groups = {review.class})
    @ApiModelProperty(value = "密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "昵称")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty(value = "姓名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "头像")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty(value = "生日")
    @TableField("birthday")
    private LocalDate birthday;

    @ApiModelProperty(value = "性别(字典 1男 2女 3未知)")
    @TableField("sex")
    private Integer sex;

    @ApiModelProperty(value = "邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty(value = "手机")
    @TableField("phone")
    private String phone;

    @ApiModelProperty(value = "电话")
    @TableField("tel")
    private String tel;

    @ApiModelProperty(value = "最后登陆IP")
    @TableField("last_login_ip")
    private String lastLoginIp;

    @ApiModelProperty(value = "最后登陆时间")
    @TableField("last_login_time")
    private LocalDateTime lastLoginTime;

    @ApiModelProperty(value = "管理员类型（0超级管理员 1非管理员）")
    @TableField("admin_type")
    private Integer adminType;

    @ApiModelProperty(value = "状态（字典 0正常 1冻结 2删除）")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除标记,1:已删除,0:正常")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "用户角色id")
    @TableField(exist = false)
    @NotBlank(message = "用户角色id不能为空", groups = {add.class})
    private String roleId;
    @ApiModelProperty(value = "用户角色")
    @TableField(exist = false)
    private String roleName;
}
