package com.whjk.system.data.vo;

import com.whjk.system.data.dto.WhjkPackageRelInfoDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(value = "PackageRelInfoVo对象", description = "历次结果报告")
public class PackageRelInfoVo extends WhjkPackageRelInfoDto {

    @ApiModelProperty("组合项目名称")
    private String zhmc;

    @ApiModelProperty("诊断结果")
    private String zdjg;

    @ApiModelProperty("检查时间")
    private LocalDateTime checkTime;
}