package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 个人诊断明细表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_zdmx")
@ApiModel(value="WhjkZdmx对象", description="个人诊断明细表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WhjkZdmx implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "个人登记ID")
    @TableField("pe_id")
    private String peId;

    @ApiModelProperty(value = "组合项目ID")
    @TableField("zhxm_id")
    private String zhxmId;

    @ApiModelProperty(value = "基础项目ID")
    @TableField("jcxm_id")
    private String jcxmId;

    @ApiModelProperty(value = "诊断结果")
    @TableField("zdjg")
    private String zdjg;

    @TableField(value ="create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value ="create_user", fill = FieldFill.INSERT)
    private String createUser;

    @TableField(value ="update_time",fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @TableField(value ="update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "项目登记ID")
    @TableField("rel_id")
    private String relId;

    @ApiModelProperty(value = "诊断建议维护主表ID")
    @TableField("zdjywh_id")
    private String zdjywhId;

    @ApiModelProperty(value = "删除标识 0-未删除 1-删除")
    @TableField("del_flag")
    private String delFlag;


}
