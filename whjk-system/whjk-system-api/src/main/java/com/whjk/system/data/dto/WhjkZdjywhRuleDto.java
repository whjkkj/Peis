package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkZdjywhRule;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/11/011 14:01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "WhjkTeamGroupDto", description = "whjkTeamGroupDto")
public class WhjkZdjywhRuleDto extends WhjkZdjywhRule {
    @ApiModelProperty(value = "诊断名称")
    private String zyzd;

    @ApiModelProperty(value = "项目名称")
    private String zhmc;

    @ApiModelProperty(value = "子项名称")
    private String xmmc;
}
