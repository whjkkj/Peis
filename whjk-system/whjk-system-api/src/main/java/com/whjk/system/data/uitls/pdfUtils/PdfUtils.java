package com.whjk.system.data.uitls.pdfUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;

public class PdfUtils {
    public static void setParagraphNull(float before, float after, Document document) throws Exception {
        Paragraph paragraph = new Paragraph("");
        //设置段落上空白
        paragraph.setSpacingBefore(before);
        //设置段落下空白
        paragraph.setSpacingAfter(after);
        document.add(paragraph);
//        setParagraph("",PdfFont.getTwelveText(),0,0,0,0,before,after);
    }

    public static void setParagraph(String text, Font font, Document document) throws Exception {
        Paragraph paragraph = new Paragraph(text, font);
        //设置文字居中 0靠左   1，居中     2，靠右
        paragraph.setAlignment(1);
        //设置左缩进
        paragraph.setIndentationLeft(-25);
        //设置右缩进
        paragraph.setIndentationRight(-25);
        document.add(paragraph);
//        setParagraph(text,font,1,null,null,0,0,0);
    }

    public static void setParagraph(String text, Font font, Integer align, Document document) throws Exception {
        Paragraph paragraph = new Paragraph(text, font);
        //设置文字居中 0靠左   1，居中     2，靠右
        paragraph.setAlignment(align);
        //设置左缩进
        paragraph.setIndentationLeft(-25);
        //设置右缩进
        paragraph.setIndentationRight(-25);
        //设置首行缩进
//        paragraph.setFirstLineIndent(24);
        //行间距
//        paragraph.setLeading(20f);
        //设置段落上空白
//        paragraph.setSpacingBefore(5f);
        //设置段落下空白
//        paragraph.setSpacingAfter(10f);

        document.add(paragraph);
//        setParagraph(text,font,align,0,0,0,0,0);
    }

    public static void setParagraph(String text, Font font, Integer align, Integer left, float[] leading, Document document) throws Exception {
        Paragraph paragraph = new Paragraph(text, font);
        //设置文字居中 0靠左   1，居中     2，靠右
        paragraph.setAlignment(align);
        //设置左缩进
        paragraph.setIndentationLeft(-25);
        //设置右缩进
        paragraph.setIndentationRight(-25);
        //设置首行缩进
        paragraph.setFirstLineIndent(left);
        //行间距
//        paragraph.setLeading(20f);
        //设置段落上空白
//        paragraph.setSpacingBefore(5f);
        //设置段落下空白
//        paragraph.setSpacingAfter(10f);
        paragraph.setLeading(leading[0], leading[1]);
        document.add(paragraph);
//        setParagraph(text,font,align,0,0,0,0,0);
    }
    public static void setParagraph(String text, Font font, Integer align, Integer left, Document document) throws Exception {
        Paragraph paragraph = new Paragraph(text, font);
        //设置文字居中 0靠左   1，居中     2，靠右
        paragraph.setAlignment(align);
        //设置左缩进
        paragraph.setIndentationLeft(-25);
        //设置右缩进
        paragraph.setIndentationRight(-25);
        //设置首行缩进
        paragraph.setFirstLineIndent(left);
        //行间距
//        paragraph.setLeading(20f);
        //设置段落上空白
//        paragraph.setSpacingBefore(5f);
        //设置段落下空白
//        paragraph.setSpacingAfter(10f);
        document.add(paragraph);
//        setParagraph(text,font,align,0,0,0,0,0);
    }
}
