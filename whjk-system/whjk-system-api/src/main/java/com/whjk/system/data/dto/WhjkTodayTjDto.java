package com.whjk.system.data.dto;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WhjkTodayTjDto", description = "WhjkTodayTjDto")
public class WhjkTodayTjDto extends PageDto {

    @ApiModelProperty(value = "组合id")
    private String xmId;

    @ApiModelProperty(value = "用户姓名")
    private String name;

    @ApiModelProperty(value = "检查时间")
    private String peDate;

    @ApiModelProperty(value = "用户手机号")
    private String phone;
}
