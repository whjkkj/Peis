package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkPackageRel;
import com.whjk.system.data.entity.WhjkZhxm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WhjkRelZhxmDto", description = "WhjkRelZhxmDto")
public class WhjkRelZhxmDto extends WhjkZhxm{

    @ApiModelProperty(value = "体检登记项目明细关联表id")
    private String relId;

    @ApiModelProperty(value = "体检状态")
    private String checkStatus;
}
