package com.whjk.system.data.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 随访详情
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_arrange_detail")
@ApiModel(value="WhjkArrangeDetail对象", description="随访详情")
public class WhjkArrangeDetail implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "随访ID")
    @TableField("arr_id")
    private String arrId;

    @ApiModelProperty(value = "随访通知者 1-本人  2-亲属")
    @TableField("arr_notify")
    private String arrNotify;

    @ApiModelProperty(value = "随访结果 1-本院就诊 2-他院就诊")
    @TableField("arr_result")
    private String arrResult;

    @ApiModelProperty(value = "随访时间")
    @TableField("arr_time")
    private LocalDateTime arrTime;

    @ApiModelProperty(value = "第几次随访")
    @TableField("arr_count")
    private BigDecimal arrCount;


}
