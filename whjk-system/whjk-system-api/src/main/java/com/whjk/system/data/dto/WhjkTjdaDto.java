package com.whjk.system.data.dto;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WhjkTjdaDto", description = "WhjkTjdaDto")
public class WhjkTjdaDto extends PageDto {

    @ApiModelProperty(value = "项目ID")
    private String idCard;

    @ApiModelProperty(value = "姓名")
    private String name;


}


