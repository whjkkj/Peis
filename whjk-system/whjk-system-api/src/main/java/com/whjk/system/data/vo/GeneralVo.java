package com.whjk.system.data.vo;

import com.whjk.system.data.dto.WhjkZdmxDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GeneralVo{

    @ApiModelProperty(value = "综述")
    private String peConclusion;

    @ApiModelProperty(value = "建议")
    private String peAdvice;

    @ApiModelProperty(value = "诊断列表以及建议")
    private List<WhjkZdmxDto> whjkZdmxDtoList;
}
