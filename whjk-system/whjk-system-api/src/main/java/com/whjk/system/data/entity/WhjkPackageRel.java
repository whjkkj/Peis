package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 体检登记项目明细关联表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_package_rel")
@ApiModel(value = "WhjkPackageRel对象", description = "体检登记项目明细关联表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WhjkPackageRel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "登记id")
    @TableField("pe_id")
    private String peId;

    @ApiModelProperty(value = "项目id")
    @TableField("xm_id")
    private String xmId;

    @ApiModelProperty(value = "套餐id")
    @TableField(value = "tc_id", updateStrategy = FieldStrategy.IGNORED)
    private String tcId;

    @ApiModelProperty(value = "项目类型1套餐2自选")
    @TableField("add_flag")
    private String addFlag;

    @ApiModelProperty(value = "实际价格(保存时)")
    @TableField("sjjg")
    private BigDecimal sjjg;

    @ApiModelProperty(value = "标准价格(保存时)")
    @TableField("bzjg")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "折扣")
    @TableField("dz")
    private BigDecimal dz;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "1:删除，0：正常")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "缴费状态（0-未缴费，1-已缴费，2-已退费，3-申请退费中）")
    @TableField("pay_status")
    private String payStatus;

    @ApiModelProperty(value = "缴费方式（1-个人支付，2-单位支付）")
    @TableField("pay_mode")
    private String payMode;


    @ApiModelProperty(value = "检查状态（0-未检查，1-已检查,2-弃检,3-未保存）")
    @TableField("check_status")
    private String checkStatus;

    @ApiModelProperty(value = "采样状态（0未采样 1已采样）")
    @TableField("sampling_status")
    private String samplingStatus;

    @ApiModelProperty(value = "未检原因")
    @TableField("uncheck_reason")
    private String uncheckReason;

    @ApiModelProperty(value = "体检小结")
    @TableField("summarize")
    private String summarize;

    @ApiModelProperty(value = "检查医生姓名")
    @TableField("check_doctor")
    private String checkDoctor;

    @ApiModelProperty(value = "额外费用-类型（0-个人支付，1-单位支付）")
    @TableField("other_type")
    private String otherType;

    @ApiModelProperty(value = "额外费用")
    @TableField("other_price")
    private BigDecimal otherPrice;

    @ApiModelProperty(value = "额外费用-缴费状态（0-未缴费，1-已缴费）")
    @TableField("other_status")
    private String otherStatus;

    @ApiModelProperty(value = "检查时间")
    @TableField("check_time")
    private LocalDateTime checkTime;

    @ApiModelProperty(value = "支付订单号")
    @TableField("order_number")
    private String orderNumber;

    @ApiModelProperty(value = "检查所见")
    @TableField("auxiliara_result")
    private String auxiliaraResult;

    @ApiModelProperty(value = "检查部位")
    @TableField("check_part")
    private String checkPart;

    @ApiModelProperty(value = "是否复查0否1是")
    @TableField("review_type")
    private String reviewType;

    @ApiModelProperty(value = "复查信息")
    @TableField("review_msg")
    private String reviewMsg;

    @ApiModelProperty(value = "复查状态0未通知1已通知")
    @TableField("review_status")
    private String reviewStatus;

    @ApiModelProperty(value = "总检复查审核状态0确认1未确认")
    @TableField("ins_review")
    private String insReview;

    @ApiModelProperty(value = "建议复查时间")
    @TableField("review_times")
    private LocalDateTime reviewTimes;

    @ApiModelProperty(value = "最后通知人")
    @TableField("last_notice")
    private String lastNotice;

    @ApiModelProperty(value = "通知时间")
    @TableField("notification_times")
    private LocalDateTime notificationTimes;

    @ApiModelProperty(value = "通知结果")
    @TableField("notification_result")
    private String notificationResult;

    @ApiModelProperty(value = "短信次数")
    @TableField("messages_number")
    private BigDecimal messagesNumber;

    @ApiModelProperty(value = "pacs图片标识")
    @TableField("study_no")
    private String studyNo;

    @ApiModelProperty(value = "pacs是否获取0否1是")
    @TableField("pacs_type")
    private String pacsType;

    @ApiModelProperty(value = "pacs推送时间（pacs获取7天内的，7天外需要刷新）")
    @TableField("pacs_time")
    private LocalDateTime pacsTime;

    @ApiModelProperty(value = "合格标记0异常1未见异常")
    @TableField("mark")
    private String mark;

    @ApiModelProperty(value = "审查者医生")
    @TableField("review_doctor")
    private String reviewDoctor;


}
