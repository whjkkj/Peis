package com.whjk.system.data.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ApiController
 *
 * @author hl
 * @date 2023/7/5
 */
@Data
public class ApiDto {

    /**
     * 1-缴费成功回调，2-退款回调
     */
    @ApiModelProperty(value = "1-缴费成功回调，2-退款回调")
    private String payType;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String peId;
    /**
     * 登记流水号
     */
    @ApiModelProperty(value = "登记流水号")
    private String regSerialNo;

    /**
     * 发票号
     */
    @ApiModelProperty(value = "发票号")
    private String invoice;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private String money;
}
