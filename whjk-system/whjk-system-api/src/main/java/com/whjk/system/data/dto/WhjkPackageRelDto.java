package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkPackageRel;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/12/012 14:10
 */
@ApiModel(value = "WhjkPackageRelDto", description = "WhjkPackageRelDto")
@Data
public class WhjkPackageRelDto extends WhjkPackageRel {

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "子项")
    private List<WhjkPackageRelInfoDto> packageRelInfoList;

    @ApiModelProperty(value = "诊断")
    private List<WhjkZdmxDto> zdmxList;
}
