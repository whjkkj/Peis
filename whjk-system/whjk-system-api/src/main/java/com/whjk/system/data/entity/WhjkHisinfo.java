package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * His缴费记录表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_hisinfo")
@ApiModel(value="WhjkHisinfo对象", description="His缴费记录表")
public class WhjkHisinfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "(公司支付默认就是缴费中，后续不需要管)")
      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "缴费状态（0-未缴费，1-缴费中，2-已缴费，3-缴费失败，4-已退款）")
    @TableField("status")
    private String status;

    @ApiModelProperty(value = "登记ID(如果是公司支付这里表示teamId)")
    @TableField("pe_id")
    private String peId;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "缴费金额")
    @TableField("money")
    private BigDecimal money;

    @ApiModelProperty(value = "His返回的发票号")
    @TableField("invoice")
    private String invoice;

    @ApiModelProperty(value = "支付流水号")
    @TableField("serial_number")
    private String serialNumber;

    @ApiModelProperty(value = "his就诊卡号")
    @TableField("patient_id")
    private String patientId;

    @ApiModelProperty(value = "0个人支付，1公司支付")
    @TableField("pay_status")
    private String payStatus;


}
