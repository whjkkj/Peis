package com.whjk.system.data.uitls.pdfUtils;

import cn.hutool.core.date.DateUtil;
import com.itextpdf.text.Document;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.*;

public class GuideReportFooter extends PdfPageEventHelper {
    // 总页数
    private PdfTemplate totalPage;

    private String now = DateUtil.today();

    public GuideReportFooter() {

    }

    // 打开文档时，创建一个总页数的模版
    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        PdfContentByte cb =writer.getDirectContent();
        totalPage = cb.createTemplate(30, 16);
    }
    // 一页加载完成触发，写入页眉和页脚
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        try {
            //非首页页脚
            PdfPTable yejiao = PdfTable.createTable(new int[] { 1, 1, 1});

            yejiao.addCell(PdfTable.createCell(now,10,0,PdfTable.borS));
            yejiao.addCell(PdfTable.createCell("第" + writer.getPageNumber() + "页",10,1,PdfTable.borS));
            yejiao.addCell(PdfTable.createCell("因为专业 所以信赖",10,2,PdfTable.borS));
            yejiao.writeSelectedRows(0, -1, 50,50, writer.getDirectContent());
        } catch (Exception de) {
            throw new ExceptionConverter(de);
        }
    }


    /**
     * 全部完成后，将总页数的pdf模版写到指定位置
     * @param writer
     * @param document
     */
    /*@Override
    public void onCloseDocument(PdfWriter writer,Document document) {
        String text = "总" + (writer.getPageNumber()) + "页";
        ColumnText.showTextAligned(totalPage, Element.ALIGN_LEFT, new Paragraph(text,PdfFont.getNormal(PdfFont.baseFontMap.get("bfChinese"),8)), 2, 2, 0);
    }*/

}
