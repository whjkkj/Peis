package com.whjk.system.data.dto;

import com.whjk.core.common.dto.BaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;


@Data
public class importExcelDto {

    @ApiModelProperty(value = "单位ID")
    private String teamId;

    @ApiModelProperty(value = "分组ID")
    @NotBlank(message = "分组ID不能为空", groups = {BaseDto.uploadList.class})
    private String groupId;

    @ApiModelProperty(value = "体检类别")
    private String peTypeId;

    @ApiModelProperty(value = "文件")
    @NotBlank(message = "文件不能为空", groups = {BaseDto.uploadList.class})
    private MultipartFile file;

}