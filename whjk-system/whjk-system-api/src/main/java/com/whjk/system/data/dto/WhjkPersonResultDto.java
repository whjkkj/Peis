package com.whjk.system.data.dto;

import com.whjk.system.data.vo.PackageRelVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "WhjkPersonResultDto", description = "WhjkPersonResultDto")
public class WhjkPersonResultDto {

    @ApiModelProperty(value = "体检项目信息")
    private PackageRelVo whjkPackageRel;

    @ApiModelProperty(value = "体检子项及结果")
    private List<WhjkPackageRelInfoDto> itemList;

    @ApiModelProperty(value = "诊断列表")
    private List<WhjkZdmxDto> tjZdmxList;
}
