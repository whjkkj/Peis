package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 体检科室表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_ks")
@ApiModel(value="WhjkKs对象", description="体检科室表")
public class WhjkKs extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "科室编号")
    @TableField("ksbh")
    private String ksbh;

    @ApiModelProperty(value = "科室名称")
    @TableField("ksmc")
    private String ksmc;

    @ApiModelProperty(value = "科室类型 （1-检查科室，2-检验科室，3-功能科室）")
    @TableField("kslx")
    private String kslx;

    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;

    @ApiModelProperty(value = "显示顺序")
    @TableField("xssx")
    private Integer xssx;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "是否启用（0-启用，1-未启用）")
    @TableField("sfqy")
    private String sfqy;

    @ApiModelProperty(value = "科室地址")
    @TableField("address")
    private String address;

    @TableField(exist = false)
    private List<WhjkZhxm> zhxmList;

    @TableField(exist = false)
    private List<WhjkXm> xmList;
}
