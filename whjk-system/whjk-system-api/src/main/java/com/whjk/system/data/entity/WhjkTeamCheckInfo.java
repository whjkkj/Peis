package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 体检单位
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_team_check_info")
@ApiModel(value="WhjkTeamCheckInfo对象", description="体检单位")
public class WhjkTeamCheckInfo extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "单位编号")
    @TableField("team_code")
    private String teamCode;

    @ApiModelProperty(value = "单位简称")
    @TableField("short_name")
    private String shortName;

    @ApiModelProperty(value = "单位名称")
    @NotNull(message = "单位名称不能为空", groups = {add.class})
    @TableField("team_name")
    private String teamName;

    @ApiModelProperty(value = "拼音简码")
    @TableField("pinyin_code")
    private String pinyinCode;

    @ApiModelProperty(value = "五笔简码")
    @TableField("wubi_code")
    private String wubiCode;

    @ApiModelProperty(value = "联系人")
    @TableField("contact_name")
    private String contactName;

    @ApiModelProperty(value = "联系电话")
    @TableField("contact_phone")
    private String contactPhone;

    @ApiModelProperty(value = "传真")
    @TableField("fax")
    private String fax;

    @ApiModelProperty(value = "邮政编码")
    @TableField("postal_code")
    private String postalCode;

    @ApiModelProperty(value = "联系地址")
    @TableField("concat_address")
    private String concatAddress;

    @ApiModelProperty(value = "银行账号")
    @TableField("bank_code")
    private String bankCode;

    @ApiModelProperty(value = "企业性质")
    @TableField("enterprise_nature")
    private String enterpriseNature;

    @ApiModelProperty(value = "编制人数")
    @TableField("compile_num")
    private BigDecimal compileNum;

    @ApiModelProperty(value = "机构代码")
    @TableField("organization_code")
    private String organizationCode;

    @ApiModelProperty(value = "企业规模(字段18)")
    @TableField("enterprise_scale")
    private String enterpriseScale;

    @ApiModelProperty(value = "经济类型(字段17)")
    @TableField("economic_type")
    private String economicType;

    @ApiModelProperty(value = "行业分类(字段16)")
    @TableField("industry_classify")
    private String industryClassify;

    @ApiModelProperty(value = "职工总人数")
    @TableField("total_emp")
    private BigDecimal totalEmp;

    @ApiModelProperty(value = "女职工人数")
    @TableField("female_empnum")
    private BigDecimal femaleEmpnum;

    @ApiModelProperty(value = "生产工人数")
    @TableField("pro_empnum")
    private BigDecimal proEmpnum;

    @ApiModelProperty(value = "生产女职工人数")
    @TableField("profemale_empnum")
    private BigDecimal profemaleEmpnum;

    @ApiModelProperty(value = "接害工人数")
    @TableField("harmful_empnum")
    private BigDecimal harmfulEmpnum;

    @ApiModelProperty(value = "接害女职工人数")
    @TableField("harmfemale_empnum")
    private BigDecimal harmfemaleEmpnum;

    @ApiModelProperty(value = "接害女职工人数")
    @TableField("remark")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "所属地区(字段21)")
    @TableField("region")
    private String region;

    @ApiModelProperty(value = "单位待检人数")
    @TableField(exist = false)
    private Integer waitCount;

}
