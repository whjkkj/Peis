package com.whjk.system.data.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.whjk.system.data.entity.WhjkPackageRel;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkTemplate;
import com.whjk.system.data.vo.SVo;
import com.whjk.system.data.vo.ZhxmVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@ApiModel(value = "WhjkTeamGroupDto", description = "whjkTeamGroupDto")
public class WhjkTemplateDto extends WhjkTemplate {

    @ApiModelProperty(value = "模板内容数据")
    @TableField(exist = false)
    private String templateData;

    private SVo sVo;
}
