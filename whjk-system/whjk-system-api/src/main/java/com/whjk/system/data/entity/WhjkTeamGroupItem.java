package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 单位分组对应所选项目表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_team_group_item")
@ApiModel(value="WhjkTeamGroupItem对象", description="单位分组对应所选项目表")
public class WhjkTeamGroupItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "单位ID")
    @TableField("team_id")
    private String teamId;

    @ApiModelProperty(value = "分组ID")
    @TableField("group_id")
    private String groupId;

    @ApiModelProperty(value = "项目编号")
    @TableField("item_id")
    private String itemId;

    @ApiModelProperty(value = "项目名称")
    @TableField("item_name")
    private String itemName;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;
    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;
    @ApiModelProperty(value = "套餐ID")
    @TableField("pac_id")
    private String pacId;

    @ApiModelProperty(value = "标准价格")
    @TableField("bzjg")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "打折")
    @TableField("dz")
    private BigDecimal dz;

    @ApiModelProperty(value = "实际价格")
    @TableField("sjjg")
    private BigDecimal sjjg;

    @ApiModelProperty(value = "是否套餐包含的项目 1是 2否")
    @TableField("add_Flag")
    private String addFlag;

}
