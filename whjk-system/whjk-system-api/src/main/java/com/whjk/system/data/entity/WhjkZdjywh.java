package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 诊断建议主表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_zdjywh")
@ApiModel(value = "WhjkZdjywh对象", description = "诊断建议主表")
public class WhjkZdjywh extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @NotBlank(message = "id不能为空", groups = {BaseDto.update.class})
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @NotBlank(message = "诊断名称不能为空", groups = {BaseDto.add.class,BaseDto.update.class})
    @ApiModelProperty(value = "诊断名称")
    @TableField("zyzd")
    private String zyzd;

    @ApiModelProperty(value = "诊断描述")
    @TableField("zdms")
    private String zdms;

    @ApiModelProperty(value = "拼音简码")
    @TableField("qyjm")
    private String qyjm;

    @ApiModelProperty(value = "五笔简码")
    @TableField("wbjm")
    private String wbjm;

    @ApiModelProperty(value = "顺序")
    @TableField("sx")
    private String sx;

    @ApiModelProperty(value = "科普说明")
    @TableField("kpsm")
    private String kpsm;

    @ApiModelProperty(value = "职业病建议")
    @TableField("zybjy")
    private String zybjy;

    @ApiModelProperty(value = "适合性别（0女1男2全部）")
    @TableField("gender_appropriate")
    private String genderAppropriate;

    @ApiModelProperty(value = "判断条件(0满足一个，1全部满足)")
    @TableField("judgment_condition")
    private String judgmentCondition;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;


}
