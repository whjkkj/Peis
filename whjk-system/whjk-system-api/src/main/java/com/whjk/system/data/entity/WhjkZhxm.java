package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 组合项目主表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_zhxm")
@ApiModel(value="WhjkZhxm对象", description="组合项目主表")
public class WhjkZhxm extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "体检科室id")
    @TableField("tjks_id")
    private String tjksId;

    @ApiModelProperty(value = "组合编号")
    @TableField("zhbh")
    private String zhbh;

    @ApiModelProperty(value = "组合名称")
    @TableField("zhmc")
    private String zhmc;

    @ApiModelProperty(value = "显示顺序")
    @TableField("xssx")
    private BigDecimal xssx;

    @ApiModelProperty(value = "是否妇科（0-否，1-是）")
    @TableField("sffk")
    private String sffk;

    @ApiModelProperty(value = "拼音简码")
    @TableField("pyjm")
    private String pyjm;

    @ApiModelProperty(value = "五笔简码")
    @TableField("wbjm")
    private String wbjm;

    @ApiModelProperty(value = "临床类型ID")
    @TableField("lclx_id")
    private String lclxId;

    @ApiModelProperty(value = "自定义码")
    @TableField("zdym")
    private String zdym;

    @ApiModelProperty(value = "汇总项目id")
    @TableField("hzxm_id")
    private String hzxmId;

    @ApiModelProperty(value = "结果获取（0-手工，1-导入）")
    @TableField("jghq")
    private String jghq;

    @ApiModelProperty(value = "是否启用（0-启用，1-未启用）")
    @TableField("sfqy")
    private String sfqy;

    @ApiModelProperty(value = "性别(2-女 1-男 %-不限)")
    @TableField("xb")
    private String xb;

    @ApiModelProperty(value = "标准价格")
    @TableField("bzjg")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "打折")
    @TableField("dz")
    private BigDecimal dz;

    @ApiModelProperty(value = "实际价格")
    @TableField("sjjg")
    private BigDecimal sjjg;

    @ApiModelProperty(value = "检查类型（1-医生检查，2-检验项目lis，3-功能检查pacs）")
    @TableField("jclx")
    private String jclx;

    @ApiModelProperty(value = "是否需要标本（0-否，1-是）")
    @TableField("sfxybm")
    private String sfxybm;

    @ApiModelProperty(value = "标本类型id")
    @TableField("bmlx_id")
    private String bmlxId;

    @ApiModelProperty(value = "是否需要申请单（0-否，1-是）")
    @TableField("sfsqd")
    private String sfsqd;

    @ApiModelProperty(value = "是否抽血（0-否，1-是）")
    @TableField("sfcx")
    private String sfcx;

    @ApiModelProperty(value = "是否餐前（0餐后1餐前2餐前憋尿3餐后憋尿）")
    @TableField("sfcq")
    private String sfcq;

    @ApiModelProperty(value = "临床建议")
    @TableField("lcjy")
    private String lcjy;

    @ApiModelProperty(value = "提示信息")
    @TableField("tsxx")
    private String tsxx;

    @ApiModelProperty(value = "正常小结")
    @TableField("zcxj")
    private String zcxj;

    @ApiModelProperty(value = "导检前置科室编号")
    @TableField("djqzksbh")
    private String djqzksbh;

    @ApiModelProperty(value = "最低折扣")
    @TableField("zdzk")
    private BigDecimal zdzk;

    @ApiModelProperty(value = "对应设备")
    @TableField("dysb")
    private String dysb;

    @ApiModelProperty(value = "执行科室id")
    @TableField("zxks_id")
    private String zxksId;

    @ApiModelProperty(value = "平均检查时间")
    @TableField("pjJCSJ")
    private String pjjcsj;

    @ApiModelProperty(value = "申请单接口编号")
    @TableField("sqdJKBH")
    private String sqdjkbh;

    @ApiModelProperty(value = "报告单接口编号")
    @TableField("bgdJKBH")
    private String bgdjkbh;

    @ApiModelProperty(value = "接口类型")
    @TableField("jklx")
    private String jklx;

    @ApiModelProperty(value = "功能类型")
    @TableField("gnlx")
    private String gnlx;

    @ApiModelProperty(value = "图片显示方式（0-A4，1-普通）")
    @TableField("tpxsfs")
    private String tpxsfs;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "pasc")
    @TableField("pasc_type")
    private String pascType;

    @ApiModelProperty(value = "Lis对应项目编码")
    @TableField("lis_code")
    private String lisCode;

    @ApiModelProperty(value = "pacs类别")
    @TableField("pacs_device_name")
    private String pacsDeviceName;

    @ApiModelProperty(value = "是否外送0否1是")
    @TableField("delivery")
    private String delivery;


}
