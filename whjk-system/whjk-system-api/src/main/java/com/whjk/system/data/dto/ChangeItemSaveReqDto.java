package com.whjk.system.data.dto;

import com.whjk.system.data.vo.ZhxmVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel(value = "项目变更保存请求实体")
public class ChangeItemSaveReqDto {
    @ApiModelProperty(value = "个人登记ID")
    @NotNull(message = "个人登记ID不能为空.")
    private String personId;

    @ApiModelProperty(value = "套餐ID")
    private String tcId;

    @ApiModelProperty(value = "变更项目明细")
    private List<ZhxmVo> changeItemList;
}
