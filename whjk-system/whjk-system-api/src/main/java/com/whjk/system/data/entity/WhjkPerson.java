package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whjk.core.common.dto.PageDto;
import com.whjk.system.data.annotation.ExcelDropdown;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 个人体检人员信息
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_person")
@ApiModel(value = "WhjkPerson对象", description = "个人体检人员信息")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WhjkPerson extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "姓名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "姓名拼音")
    @TableField("py")
    private String py;

    @ApiModelProperty(value = "用户图像")
    @TableField("user_image")
    private String userImage;

    @ApiModelProperty(value = "身份证号")
    @TableField("id_card")
    private String idCard;

    @ApiModelProperty(value = "登记流水号")
    @TableField("reg_serial_no")
    private String regSerialNo;

    @ApiModelProperty(value = "档案号")
    @TableField("file_no")
    private String fileNo;

    @ApiModelProperty(value = "个人费用")
    @TableField("person_amount")
    private BigDecimal personAmount;

    @ApiModelProperty(value = "团队费用")
    @TableField("team_amount")
    private BigDecimal teamAmount;

    @NotBlank(message = "体检状态不能为空", groups = {recordList.class})
    @ApiModelProperty(value = "体检状态(0:预约,1:登记,2:检查中,3:已终检)")
    @TableField("pe_status")
    private String peStatus;

    @ApiModelProperty(value = "流程（0-未开始1-已登记,2-已缴费,3-全科结果录入4-已回收指引单,5-已总检 6-报告打印 7-用户接收报告）")
    @TableField("process")
    private String process;

    @ApiModelProperty(value = "收费标志(0未收，1已收, 2部分已收)")
    @TableField("charge_status")
    private String chargeStatus;

    @ApiModelProperty(value = "体检次数")
    @TableField("pe_times")
    private Integer peTimes;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "体检日期")
    @TableField("pe_date")
    private LocalDateTime peDate;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd") //出参
    @ApiModelProperty(value = "生日")
    @TableField("birthday")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "性别(1:男 0-女)")
    @TableField("gender")
    private String gender;

    @ApiModelProperty(value = "年龄")
    @TableField("age")
    private Integer age;

    @ApiModelProperty(value = "1:已婚，0:未婚，2:离异，3丧偶，4其他")
    @TableField("marriage")
    private String marriage;

    @ApiModelProperty(value = "电话")
    @TableField("phone")
    private String phone;

    @ApiModelProperty(value = "民族")
    @TableField("nation")
    private String nation;

    @ApiModelProperty(value = "体检结果(1:合格,0:不合格)")
    @ExcelDropdown(dictExp = "0=不合格,1=合格")
    @TableField("pe_result")
    private String peResult;

    @ApiModelProperty(value = "业务类型")
    @TableField("biz_type_id")
    private String bizTypeId;


    @ApiModelProperty(value = "体检类型")
    @TableField("pe_type_id")
    private String peTypeId;

    @ApiModelProperty(value = "联系地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "联系邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty(value = "介绍人")
    @TableField("introducer")
    private String introducer;

    @ApiModelProperty(value = "总检完成标志(0待总检 1 已总检)")
    @TableField("ins_mark")
    private String insMark;

    @ApiModelProperty(value = "总检医生名称")
    @TableField(value = "ins_doctor", updateStrategy = FieldStrategy.IGNORED)
    private String insDoctor;

    @ApiModelProperty(value = "总检医生编号")
    @TableField("ins_doctor_code")
    private String insDoctorCode;

    @ApiModelProperty(value = "导检单状态(1:已回收，0:未回收)")
    @TableField("guide_sheet")
    private String guideSheet;

    @ApiModelProperty(value = "开单医生 - 打印指引单的医生")
    @TableField("audit_doctor")
    private String auditDoctor;

    @ApiModelProperty(value = "开单医生编号")
    @TableField("audit_doctor_code")
    private String auditDoctorCode;

    @ApiModelProperty(value = "开单医生开始打印指引单时间")
    @TableField("audit_time")
    private LocalDateTime auditTime;
    @NotBlank(message = "登记类型(1:个人,2:团队)不能为空", groups = {recordList.class})
    @ApiModelProperty(value = "登记类型(1:个人,2:团队)")
    @TableField("reg_type")
    private String regType;

    @ApiModelProperty(value = "团队ID")
    @TableField("team_id")
    private String teamId;

    @ApiModelProperty(value = "分组ID")
    @TableField("group_id")
    private String groupId;

    @ApiModelProperty(value = "部门ID")
    @TableField("dept_id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    @TableField("dept_name")
    private String deptName;

    @ApiModelProperty(value = "套餐Id")
    @TableField("tc_id")
    private String tcId;

    @ApiModelProperty(value = "加项金额")
    @TableField("add_amount")
    private BigDecimal addAmount;

    @ApiModelProperty(value = "登记日期")
    @TableField("register_time")
    private LocalDateTime registerTime;

    @ApiModelProperty(value = "收费时间")
    @TableField("charge_time")
    private LocalDateTime chargeTime;

    @ApiModelProperty(value = "指引单回收时间")
    @TableField("recy_time")
    private LocalDateTime recyTime;

    @ApiModelProperty(value = "指引单回收医生")
    @TableField("recy_doctor")
    private String recyDoctor;

    @ApiModelProperty(value = "全科记录结果录入完结时间")
    @TableField("entry_time")
    private LocalDateTime entryTime;

    @ApiModelProperty(value = "报告打印时间")
    @TableField("print_time")
    private LocalDateTime printTime;
    @ApiModelProperty(value = "报告打印医生")
    @TableField("print_doctor")
    private String printDoctor;

    @ApiModelProperty(value = "总检日期")
    @TableField(value = "ins_date", updateStrategy = FieldStrategy.IGNORED)
    private LocalDateTime insDate;

    @ApiModelProperty(value = "报告领取时间")
    @TableField("receive_time")
    private LocalDateTime receiveTime;

    @ApiModelProperty(value = "报告领取操作医生")
    @TableField("receive_doctor")
    private String receiveDoctor;

    @ApiModelProperty(value = "团检预约到期时间")
    @TableField("reg_due_time")
    private LocalDateTime regDueTime;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "预约医生")
    @TableField("pre_reg_doctor")
    private String preRegDoctor;

    @ApiModelProperty(value = "登记医生/报道医生")
    @TableField("register_doctor")
    private String registerDoctor;

    @ApiModelProperty(value = "预约时间")
    @TableField("reserve_time")
    private LocalDateTime reserveTime;

    @ApiModelProperty(value = "支付订单号")
    @TableField("order_number")
    private String orderNumber;

    @ExcelDropdown(dictExp = "0=后台用户,1=体检预约平台")
    @ApiModelProperty(value = "单位是否收费0否1是")
    @TableField("steam_toll")
    private String steamToll;

    @ApiModelProperty(value = "民族code")
    @TableField("nation_code")
    private String nationCode;

    @ApiModelProperty(value = "来源 1-后台用户 2-体检预约平台3-")
    @TableField("source")
    private String source;

    @ApiModelProperty(value = "证件类型1身份证2待定")
    @TableField("zjlx")
    private String zjlx;

    @ApiModelProperty(value = "检查类型，0健康1放射2职业病诊断")
    @TableField("examine_type")
    private String examineType;

    @ApiModelProperty(value = "复检上级id")
    @TableField("p_id")
    private String pId;

    @ApiModelProperty(value = "体检结论（综述）")
    @TableField("pe_conclusion")
    private String peConclusion;

    @ApiModelProperty(value = "总检建议")
    @TableField("pe_advice")
    private String peAdvice;

    @ApiModelProperty(value = "初审说明")
    @TableField("initial_review")
    private String initialReview;

    @ApiModelProperty(value = "总审说明")
    @TableField("general_review")
    private String generalReview;

    @ApiModelProperty(value = "终审说明")
    @TableField("final_review")
    private String finalReview;

    @ApiModelProperty(value = "初审医生")
    @TableField(value = "initial_doctor", updateStrategy = FieldStrategy.IGNORED)
    private String initialDoctor;

    @ApiModelProperty(value = "总审医生")
    @TableField(value = "general_doctor", updateStrategy = FieldStrategy.IGNORED)
    private String generalDoctor;

    @ApiModelProperty(value = "终审医生")
    @TableField(value = "final_doctor", updateStrategy = FieldStrategy.IGNORED)
    private String finalDoctor;

    @ApiModelProperty(value = "初审日期")
    @TableField(value = "initial_date", updateStrategy = FieldStrategy.IGNORED)
    private LocalDateTime initialDate;

    @ApiModelProperty(value = "总审日期")
    @TableField(value = "general_date", updateStrategy = FieldStrategy.IGNORED)
    private LocalDateTime generalDate;

    @ApiModelProperty(value = "终审日期")
    @TableField(value = "final_date", updateStrategy = FieldStrategy.IGNORED)
    private LocalDateTime finalDate;
}
