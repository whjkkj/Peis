package com.whjk.system.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "AnomalyOverviewVo", description = "异常综述")
public class AnomalyOverviewVo {

    @ApiModelProperty(value = "科室名称")
    private String ksmc;

    @ApiModelProperty(value = "组合项目列表")
    private List<BeforeContrastResultVo> anomalyZHDtoList;


}


