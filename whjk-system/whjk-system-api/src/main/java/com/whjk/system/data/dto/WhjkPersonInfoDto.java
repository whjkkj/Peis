package com.whjk.system.data.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WhjkPersonInfoDto", description = "WhjkPersonInfoDto")
public class WhjkPersonInfoDto {
    @ApiModelProperty(value = "组合项目id")
    private String zhxmId;
    @ApiModelProperty(value = "组合项目名称")
    private String zhxmName;
    @ApiModelProperty(value = "标准价格")
    private String bzjg;
    @ApiModelProperty(value = "折扣")
    private String dz;
    @ApiModelProperty(value = "实际价格")
    private String sjjg;
    @ApiModelProperty(value = "项目类型1套餐2自选")
    private String type;
}
