package com.whjk.system.data.vo;

import lombok.Data;

@Data
public class goodVo {
    private String t;
    private String officeName;
    private String address;
    private String discountPrice;
    private String projectName;
    private String monitorName;
}
