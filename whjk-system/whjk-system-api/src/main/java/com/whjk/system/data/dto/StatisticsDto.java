package com.whjk.system.data.dto;


import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WhjkRelZhxmDto", description = "WhjkRelZhxmDto")
public class StatisticsDto extends PageDto {

@ApiModelProperty(value = "项目ID")
private String xmId;

@ApiModelProperty(value = "检查医生")
private String checkDoctor;

@ApiModelProperty(value = "体检时间")
private String PeDate;

}


