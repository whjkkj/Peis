package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.vo.ZhxmVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "WhjkPersonDto", description = "WhjkTcZhxmDto")
public class WhjkPersonDto extends WhjkPerson {
    @ApiModelProperty(value = "项目")
    private List<ZhxmVo> changeItemList;

    @ApiModelProperty(value = "套餐名称")
    private String tcmc;

    @ApiModelProperty(value = "业务类型名称")
    private String ywlxmc;

    @ApiModelProperty(value = "体检类型名称")
    private String tjlxmc;

    @ApiModelProperty(value = "单位名称")
    private String teamName;

    @ApiModelProperty(value = "分组名称")
    private String groupName;

    @ApiModelProperty(value = "zhxmid集合")
    private List<String> zhxmIds;

    @ApiModelProperty(value = "是否复检true复检，false没复检")
    private Boolean type;

    @ApiModelProperty(value = "随访次数")
    private Integer arrCount;
}
