package com.whjk.system.data.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;



@Data
@ApiModel(value = "PackageRel", description = "医生患者已检人数")
public class PackageRelCountVo{

    @ApiModelProperty("医生名称")
    private String checkDoctor;

    @ApiModelProperty("组合项目名称" )
    private String zhmc;

    @ApiModelProperty("统计人数")
    private  Integer CountPeople;
}