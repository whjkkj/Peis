package com.whjk.system.data.vo;

import com.whjk.system.data.entity.WhjkPackageRel;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "PackageRelVo对象", description = "总检报告")
public class PackageRelVo extends WhjkPackageRel {

    @ApiModelProperty("组合项目名称")
    private String zhmc;

    @ApiModelProperty("检查类型")
    private String jclx;

    @ApiModelProperty("性别")
    private String gender;

    @ApiModelProperty("标本名称")
    private String bbmc;

    @ApiModelProperty("结果报告")
    private List<PackageRelInfoVo> packageRelInfoVoList;
}
