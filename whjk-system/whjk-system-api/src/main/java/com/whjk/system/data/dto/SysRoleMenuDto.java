package com.whjk.system.data.dto;

import com.whjk.core.common.dto.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@ApiModel(value = "SysRoleMenuDto", description = "SysRoleMenuDto")
public class SysRoleMenuDto {
    @NotBlank(message = "角色id不能为空", groups = {BaseDto.list.class})
    @ApiModelProperty(value = "角色id")
    private String roleId;
    @NotEmpty(message = "菜单ID集合不能为空", groups = {BaseDto.list.class})
    @ApiModelProperty(value = "菜单ID集合")
    private List<String> menuId;
}
