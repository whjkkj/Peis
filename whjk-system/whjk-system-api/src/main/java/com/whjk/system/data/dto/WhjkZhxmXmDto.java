package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkZhxm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "WhjkZhxmXmDto", description = "WhjkZhxmXmDto")
public class WhjkZhxmXmDto extends WhjkZhxm {

    @ApiModelProperty(value = "项目集合")
    private List<String> xmIds;
}
