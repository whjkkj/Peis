package com.whjk.system.data.dto;

import com.whjk.core.common.dto.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "WhjkTeamGroupDto", description = "whjkTeamGroupDto")
public class WhjkCjjgwhDto{

    @ApiModelProperty(value = "项目名称")
    private String ksmc;

    @NotBlank(message = "常见结果不能为空", groups = {BaseDto.add.class, BaseDto.update.class})
    @ApiModelProperty(value = "结果")
    private String mx;

    @ApiModelProperty(value = "是否高低")
    private String sfgd;

    @ApiModelProperty(value = "是否进入小结")
    private String sfjrxj;

    @ApiModelProperty(value = "是否阳性")
    private String sfyx;

    @NotNull(message = "体检项目id不能为空", groups = {BaseDto.add.class})
    @ApiModelProperty(value = "体检项目id")
    private String tjxmId;

    @ApiModelProperty(value = "性别")
    private String xb;

    @ApiModelProperty(value = "单位项目名称是否进入小结")
    private String xmmcxsxj;

    @ApiModelProperty(value = "诊断名称id")
    private String zdId;

}
