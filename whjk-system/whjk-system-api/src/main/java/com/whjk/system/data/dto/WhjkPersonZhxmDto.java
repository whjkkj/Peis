package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkZhxm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "WhjkPersonZhxmDto", description = "WhjkPersonZhxmDto")
public class WhjkPersonZhxmDto{

    @ApiModelProperty(value = "用户信息")
    private WhjkPerson person;

    @ApiModelProperty(value = "检查类型-医生检查")
    private List<WhjkRelZhxmDto> ysjcZhxmList;

    @ApiModelProperty(value = "检查类型-检验项目")
    private List<WhjkRelZhxmDto> jyxmZhxmList;

    @ApiModelProperty(value = "检查类型-功能检查")
    private List<WhjkRelZhxmDto> gnjcZhxmList;
}
