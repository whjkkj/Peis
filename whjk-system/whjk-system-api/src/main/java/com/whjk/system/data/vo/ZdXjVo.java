package com.whjk.system.data.vo;

import com.whjk.system.data.dto.WhjkZdjywhDto;
import com.whjk.system.data.entity.WhjkZdjywh;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "ZdXjVo对象", description = "诊断小结")
public class ZdXjVo {

    @ApiModelProperty(value = "结论")
    private String conclusion;

    @ApiModelProperty(value = "诊断集合")
    private List<WhjkZdjywhDto> zdjgList;
}
