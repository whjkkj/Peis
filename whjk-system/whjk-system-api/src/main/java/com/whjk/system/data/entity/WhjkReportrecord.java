package com.whjk.system.data.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.whjk.core.common.dto.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 体检报告领取记录表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_reportrecord")
@ApiModel(value="WhjkReportrecord对象", description="体检报告领取记录表")
public class WhjkReportrecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;
    @NotBlank(message = "个人登记ID不能为空", groups = {BaseDto.recordList.class})
    @ApiModelProperty(value = "个人登记ID")
    @TableField("pe_id")
    private String peId;
    @NotBlank(message = "1-自取/ 2-邮寄不能为空", groups = {BaseDto.recordList.class})
    @ApiModelProperty(value = "1-自取/ 2-邮寄")
    @TableField("receive_way")
    private String receiveWay;

    @ApiModelProperty(value = "1-（待取/待邮寄） 2-已取/已邮寄")
    @TableField("receive_status")
    private String receiveStatus;

    @ApiModelProperty(value = "领取人")
    @TableField("receiver")
    private String receiver;

    @ApiModelProperty(value = "发放人")
    @TableField("issuser")
    private String issuser;

    @ApiModelProperty(value = "发放时间")
    @TableField("iss_time")
    private LocalDateTime issTime;

    @ApiModelProperty(value = "收件电话")
    @TableField("phone")
    private String phone;

    @ApiModelProperty(value = "邮寄地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "快递单号")
    @TableField("exp_no")
    private String expNo;

    @ApiModelProperty(value = "是否确认0否1是")
    @TableField("confirm_type")
    private String confirmType;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;
}
