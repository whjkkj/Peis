package com.whjk.system.data.vo;

import com.whjk.system.data.entity.WhjkTemplate;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TemplateVo extends WhjkTemplate {

    @ApiModelProperty(value = "组合名称")
    private String zhmc;
}
