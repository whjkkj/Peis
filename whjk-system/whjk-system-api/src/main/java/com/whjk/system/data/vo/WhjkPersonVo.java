package com.whjk.system.data.vo;

import java.math.BigDecimal;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
/**
 * <p>
 * 个人体检人员信息
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
public class WhjkPersonVo{
    @ExcelProperty(value = "体检号", index = 0)
    @ApiModelProperty(value = "体检号")
    private String ww;

    @ExcelProperty(value = "登记流水号", index = 1)
    @ApiModelProperty(value = "登记流水号")
    private String regSerialNo;

    @ExcelProperty(value = "来源", index = 2)
    @ApiModelProperty(value = "来源 1-后台用户 2-体检预约平台3-")
    private String source;

    @ExcelProperty(value = "姓名", index = 3)
    @ApiModelProperty(value = "姓名")
    private String name;

    @ExcelProperty(value = "身份证号", index = 4)
    @ApiModelProperty(value = "身份证号")
    private String idCard;

    @ExcelProperty(value = "体检类型", index = 5)
    @ApiModelProperty(value = "体检类型")
    private String peTypeId;

    @ExcelProperty(value = "收费状态", index = 6)
    @ApiModelProperty(value = "收费标志(0未收，1已收, 2部分已收)")
    private String chargeStatus;

    @ExcelProperty(value = "性别", index = 7)
    @ApiModelProperty(value = "性别(1:男 0-女)")
    private String gender;

    @ExcelProperty(value = "婚否", index = 8)
    @ApiModelProperty(value = "1:已婚，0:未婚，2:离异，3丧偶，4其他")
    private String marriage;

    @ExcelProperty(value = "年龄", index = 9)
    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ExcelProperty(value = "电话", index = 10)
    @ApiModelProperty(value = "电话")
    private String phone;

    @ExcelProperty(value = "个人费用(元)", index = 11)
    @ApiModelProperty(value = "个人费用")
    private BigDecimal personAmount;

    @ExcelProperty(value = "团队费用(元)", index = 12)
    @ApiModelProperty(value = "团队费用")
    private BigDecimal teamAmount;

    @ExcelProperty(value = "体检状态", index = 13)
    @ApiModelProperty(value = "体检状态(0:预约,1:登记,2:检查中,3:已终检)")
    private String peStatus;

    @ExcelProperty(value = "登记(预约)时间", index = 14)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ExcelProperty(value = "体检日期", index = 15)
    @ApiModelProperty(value = "体检日期")
    private LocalDateTime peDate;


    @ExcelProperty(value = "总检日期", index = 16)
    @ApiModelProperty(value = "总检日期")
    private LocalDateTime insDate;

    @ExcelProperty(value = "导检单状态", index = 17)
    @ApiModelProperty(value = "导检单状态(1:已回收，0:未回收)")
    private String guideSheet;

    @ExcelProperty(value = "单位", index = 18)
    @ApiModelProperty(value = "团队名称")
    @TableField(exist = false)
    private String teamName;

    @ExcelProperty(value = "分组", index = 19)
    @ApiModelProperty(value = "分组名称")
    @TableField(exist = false)
    private String groupName;

    @ExcelProperty(value = "创建人", index = 20)
    @ApiModelProperty(value = "创建人")
    private String createUser;

    @ExcelProperty(value = "总检医生", index = 21)
    @ApiModelProperty(value = "总检医生名称")
    private String insDoctor;


    @ExcelProperty(value = "检查类型", index = 22)
    @ApiModelProperty(value = "检查类型，0健康1放射2职业病诊断")
    private String examineType;


}
