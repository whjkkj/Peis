package com.whjk.system.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel(value = "TjItemVo对象", description = "项目信息")
public class ZhxmVo {

    @ApiModelProperty(value = "体检登记项目明细id")
    private String relId;

    @ApiModelProperty(value = "项目ID")
    @NotNull(message = "项目ID不能为空.")
    private String zhxmId;
    @ApiModelProperty(value = "项目编号")
    private String zhbh;
    @ApiModelProperty(value = "项目名称")
    private String zhmc;
    @ApiModelProperty(value = "检查类型（1-医生检查，2-检验项目lis，3-功能检查pacs）")
    private String jclx;
    @ApiModelProperty(value = "缴费状态（0-未缴费，1-已缴费,2-已退费）")
    private String payStatus;

    @ApiModelProperty(value = "检查状态（0-未检查，1-已检查,2-弃检,3-暂存）")
    private String checkStatus;

    @ApiModelProperty(value = "是否套餐项目 1是 2否")
    private String addFlag;

    @ApiModelProperty(value = "项目实际金额")
    private BigDecimal sjjg;

    @ApiModelProperty(value = "项目标准金额")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "打折")
    private BigDecimal dz;

    @ApiModelProperty(value = "套餐ID")
    private String tcId;
    @ApiModelProperty(value = "套餐名称")
    private String tcmc;
}