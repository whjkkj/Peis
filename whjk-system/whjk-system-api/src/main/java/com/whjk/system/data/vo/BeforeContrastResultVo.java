package com.whjk.system.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "BeforeContrastResultVo对象", description = "历次结果对比")
public class BeforeContrastResultVo {

    @ApiModelProperty("体检组合项目id")
    private String xmId;

    @ApiModelProperty("体检组合项目名称")
    private String zhmc;

    @ApiModelProperty("体检组合项目类型")
    private String jclx;

    @ApiModelProperty("标题list")
    private List<Map<String,String>> titleList;

    @ApiModelProperty("结果List")
    private List jgList;
}
