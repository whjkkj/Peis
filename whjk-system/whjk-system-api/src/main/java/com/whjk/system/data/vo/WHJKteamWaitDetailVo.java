package com.whjk.system.data.vo;


import com.whjk.system.data.entity.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "WHJKteamWaitDetailVo", description = "WhjkPackageRelVo")
public class WHJKteamWaitDetailVo extends WhjkTeamGroup {

    @ApiModelProperty(value = "待检用户")
    private List<WhjkPerson> waitRegList;

    @ApiModelProperty(value = "项目")
    private List<WhjkTeamGroupItem> zhxms;
}
