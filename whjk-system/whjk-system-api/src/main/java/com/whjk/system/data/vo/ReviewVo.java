package com.whjk.system.data.vo;

import com.whjk.system.data.dto.WhjkZdmxDto;
import com.whjk.system.data.entity.WhjkPackageRel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ReviewVo extends WhjkPackageRel {

    @ApiModelProperty("组合名称")
    private String zhmc;

    @ApiModelProperty("诊断以及建议")
    private List<WhjkZdmxDto> zdmxDtoList;
}
