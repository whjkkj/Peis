package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 套餐关联表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_tc_info")
@ApiModel(value="WhjkTcInfo对象", description="套餐关联表")
public class WhjkTcInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "套餐id")
    @TableField("tc_id")
    private String tcId;

    @ApiModelProperty(value = "组合项目id")
    @TableField("zhxm_id")
    private String zhxmId;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "标准价格（项目的实际价格）")
    @TableField("bzjg")
    private BigDecimal bzjg;

    @ApiModelProperty(value = "折扣（套餐的折扣）")
    @TableField("dz")
    private BigDecimal dz;

    @ApiModelProperty(value = "实际价格")
    @TableField("sjjg")
    private BigDecimal sjjg;


}
