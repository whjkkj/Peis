package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 单位分组信息表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_team_group")
@ApiModel(value="WhjkTeamGroup对象", description="单位分组信息表")
public class WhjkTeamGroup extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
      @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "上级分组")
    @TableField("group_id")
    private String groupId;
    @NotBlank(message = "单位ID不能为空", groups = {add.class})
    @ApiModelProperty(value = "单位ID")
    @TableField("team_id")
    private String teamId;

    @ApiModelProperty(value = "分组编号")
    @TableField("group_number")
    private String groupNumber;

    @ApiModelProperty(value = "业务类型")
    @TableField("bus_type")
    private String busType;

    @NotBlank(message = "分组名称不能为空", groups = {add.class})
    @ApiModelProperty(value = "分组名称")
    @TableField("group_name")
    private String groupName;

    @ApiModelProperty(value = "是否启用.0:启用 1不启用")
    @TableField("sfqy")
    private String sfqy;

    @ApiModelProperty(value = "体检类别")
    @TableField("medical_category")
    private String medicalCategory;

    @ApiModelProperty(value = "性别(0-女 1-男 %-不限)")
    @TableField("gender")
    private String gender;

    @ApiModelProperty(value = "年龄上限")
    @TableField("age_up")
    private BigDecimal ageUp;

    @ApiModelProperty(value = "年龄下限")
    @TableField("age_down")
    private BigDecimal ageDown;

    @ApiModelProperty(value = "婚姻状况:0未婚 1已婚 2不限")
    @TableField("marital_status")
    private String maritalStatus;

    @ApiModelProperty(value = "分组类型")
    @TableField("group_type")
    private String groupType;

    @ApiModelProperty(value = "结算人数")
    @TableField("settlement_num")
    private BigDecimal settlementNum;

    @DecimalMin(value = "0", message = "标准价格不能为空", groups = {add.class})
    @ApiModelProperty(value = "标准价格")
    @TableField("bzjg")
    private BigDecimal bzjg;

    @DecimalMin(value = "0", message = "标准价格不能为空", groups = {add.class})
    @ApiModelProperty(value = "实际价格")
    @TableField("sjjg")
    private BigDecimal sjjg;

    @ApiModelProperty(value = "项目折扣")
    @TableField("dz")
    private BigDecimal dz;

    @ApiModelProperty(value = "加项折扣")
    @TableField("jxdz")
    private BigDecimal jxdz;

    @ApiModelProperty(value = "分组支付方式:0个人  1单位")
    @TableField("group_paytype")
    private String groupPaytype;

    @ApiModelProperty(value = "加项支付方式:0个人  1单位")
    @TableField("additem_paytype")
    private String additemPaytype;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "套餐ID")
    @TableField(value = "pac_id", updateStrategy = FieldStrategy.IGNORED)
    private String pacId;

    @ApiModelProperty(value = "套餐名称")
    @TableField("pac_name")
    private String pacName;

    @ApiModelProperty(value = "来源 1-后台人员 2-体检预约平台")
    @TableField("source")
    private String source;

    @ApiModelProperty(value = "子分组")
    @TableField(exist = false)
    private List<WhjkTeamGroup> childList;
}
