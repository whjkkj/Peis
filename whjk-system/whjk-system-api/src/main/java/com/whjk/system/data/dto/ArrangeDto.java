package com.whjk.system.data.dto;

import com.whjk.system.data.entity.WhjkArrange;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ArrangeDto extends WhjkArrange {

    @ApiModelProperty("回访时间")
    private String arrDate;
}
