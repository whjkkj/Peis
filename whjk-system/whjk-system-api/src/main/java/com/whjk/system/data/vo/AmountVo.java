package com.whjk.system.data.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AmountVo {
    @ApiModelProperty(value = "单位费用")
    private BigDecimal teamAmount = BigDecimal.ZERO;
    @ApiModelProperty(value = "个人费用")
    private BigDecimal personAmount = BigDecimal.ZERO;
    @ApiModelProperty(value = "false-未缴费，true-已缴费")
    private Boolean paySign = true;
}
