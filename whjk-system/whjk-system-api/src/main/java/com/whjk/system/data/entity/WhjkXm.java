package com.whjk.system.data.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.whjk.core.common.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 体检项目表
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("whjk_xm")
@ApiModel(value="WhjkXm对象", description="体检项目表")
public class WhjkXm extends PageDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "项目编号")
    @TableField("xmbh")
    private String xmbh;

    @ApiModelProperty(value = "项目名称")
    @TableField("xmmc")
    private String xmmc;

    @ApiModelProperty(value = "性别(0-女 1-男 %-不限)")
    @TableField("xb")
    private String xb;

    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;

    @ApiModelProperty(value = "临床类型id")
    @TableField("lclx_id")
    private String lclxId;

    @ApiModelProperty(value = "显示顺序")
    @TableField("xssx")
    private BigDecimal xssx;

    @ApiModelProperty(value = "单价")
    @TableField("dj")
    private BigDecimal dj;

    @ApiModelProperty(value = "单位")
    @TableField("dw")
    private String dw;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private String updateUser;

    @ApiModelProperty(value = "删除状态（0-未删除，1-删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "正常结果")
    @TableField("zcjg")
    private String zcjg;

    @ApiModelProperty(value = "结果类型(0-字符型 1-数值型)")
    @TableField("jglx")
    private String jglx;

    @ApiModelProperty(value = "是否启用(0-启用 1-不启用)")
    @TableField("sfqy")
    private String sfqy;

    @ApiModelProperty(value = "最小极限值")
    @TableField("zxjxz")
    private String zxjxz;

    @ApiModelProperty(value = "最大极限值")
    @TableField("zdjxz")
    private String zdjxz;

    @ApiModelProperty(value = "体检科室ID")
    @TableField("tjks_id")
    private String tjksId;

    @ApiModelProperty(value = "List对应项目编码")
    @TableField("lis_code")
    private String lisCode;


    @ApiModelProperty(value = "组合项目id")
    @TableField(exist = false)
    private String zhxmId;
}
