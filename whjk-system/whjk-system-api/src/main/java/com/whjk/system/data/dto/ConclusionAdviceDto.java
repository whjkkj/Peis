package com.whjk.system.data.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ConclusionAdviceDto {

    @ApiModelProperty(value = "用户id")
    private String peId;

    @ApiModelProperty(value = "用户综述")
    private String peConclusion;

    @ApiModelProperty(value = "用户建议")
    private String peAdvice;
}
