package com.whjk.system.data.dto;


import com.whjk.core.common.dto.BaseDto;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.whjk.system.data.entity.WhjkZdjywhInfo;
import com.whjk.system.data.entity.WhjkZdjywhRule;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "WhjkTeamGroupDto2", description = "whjkTeamGroupDto2")
public class WhjkZdjywhDto extends WhjkZdjywh {

    @ApiModelProperty(value = "诊断建议从表")
    private List<WhjkZdjywhInfo> infoList;

    @ApiModelProperty(value = "体检科室ID")
    private String tjksId;
    @ApiModelProperty(value = "组合项目id")
    private String zhxmId;
    @NotNull(message = "逻辑条件不能为空", groups = {BaseDto.add.class,BaseDto.update.class})
    @Valid
    @ApiModelProperty(value = "逻辑条件")
    private List<WhjkZdjywhRuleDto> zdjywhRuleList;

}
