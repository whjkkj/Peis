package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkTjbgzswh;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 体检报告展示维护(大文本信息维护) Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTjbgzswhMapper extends BaseMapper<WhjkTjbgzswh> {

}
