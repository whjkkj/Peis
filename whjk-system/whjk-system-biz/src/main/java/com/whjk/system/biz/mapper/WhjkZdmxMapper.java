package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.whjk.system.data.dto.WhjkZdmxDto;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.whjk.system.data.entity.WhjkZdmx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 个人诊断明细表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Mapper
public interface WhjkZdmxMapper extends BaseMapper<WhjkZdmx> {

    List<WhjkZdjywh> selectPersonDiagnosis(String peId);

    List<WhjkZdmxDto> selectRelZdmx(@Param(Constants.WRAPPER) QueryWrapper<Object> eq);
}
