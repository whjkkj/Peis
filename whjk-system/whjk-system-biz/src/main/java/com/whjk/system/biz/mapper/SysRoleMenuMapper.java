package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRoleMenu;

import java.util.List;

/**
 * <p>
 * 系统角色菜单表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    List<SysMenu> selectMenuByRoleId(String roleId);
}
