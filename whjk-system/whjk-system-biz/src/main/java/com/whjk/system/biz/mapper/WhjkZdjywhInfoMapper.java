package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkZdjywhInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诊断建议从表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZdjywhInfoMapper extends BaseMapper<WhjkZdjywhInfo> {

    WhjkZdjywhInfo selectSlaveTable(String zdjyId);
}
