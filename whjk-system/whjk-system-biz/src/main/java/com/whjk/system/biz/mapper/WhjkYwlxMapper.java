package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkYwlx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 业务类型设置 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkYwlxMapper extends BaseMapper<WhjkYwlx> {

}
