package com.whjk.system.biz.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.WhjkSwitch;


/**
 * <p>
 * 开关表
 * </p>
 *
 * @author Cs
 * @since 2023-07-21
 */
public interface WhjkSwitchService extends IService<WhjkSwitch> {


}
