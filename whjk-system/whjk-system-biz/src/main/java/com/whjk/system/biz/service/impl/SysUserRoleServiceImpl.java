package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.biz.mapper.SysUserRoleMapper;
import com.whjk.system.biz.service.SysUserRoleService;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUserRole;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户角色表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Override
    public SysRole queryUserRole(String UserId) {
        //查询用户角色
        return this.baseMapper.selectUserByRoleId(UserId);
    }


}
