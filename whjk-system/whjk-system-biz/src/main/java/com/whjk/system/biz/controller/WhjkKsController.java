package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkKsService;
import com.whjk.system.data.entity.WhjkKs;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 体检科室表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-ks")
@Api(tags = "体检科室")
public class WhjkKsController {

    @Autowired
    private WhjkKsService whjkKsService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkKs>> page(@RequestBody WhjkKs pageDto){
        return Result.ok(whjkKsService.pageList(pageDto));
    }

    @PostMapping("/treeList")
    @ApiOperation("体检组合项目列表")
    public Result<List<WhjkKs>> treeList(@RequestBody WhjkKs whjkKs){
        return Result.ok(whjkKsService.ksTreeList(whjkKs));
    }

    @PostMapping("/xm/treeList")
    @ApiOperation("体检基础项目列表")
    public Result<List<WhjkKs>> xmTreeList(@RequestBody WhjkKs whjkKs){
        return Result.ok(whjkKsService.xmTreeList(whjkKs));
    }

    @PostMapping("/add")
    @ApiOperation("添加体检科室")
    public Result<T> add(@RequestBody WhjkKs whjkKs){
        whjkKsService.save(whjkKs);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result update(@RequestBody WhjkKs whjkKs){
        whjkKsService.updateById(whjkKs);
        return Result.ok();
    }


    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkKs> getById(@PathVariable("id") String id){
        return Result.ok(whjkKsService.getById(id));
    }
}

