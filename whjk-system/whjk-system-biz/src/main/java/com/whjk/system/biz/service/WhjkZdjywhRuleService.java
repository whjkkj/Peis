package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.dto.WhjkZdjywhRuleDto;
import com.whjk.system.data.entity.WhjkZdjywhRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 规则 服务类
 * </p>
 *
 * @author hl
 * @since 2023-07-11
 */
public interface WhjkZdjywhRuleService extends IService<WhjkZdjywhRule> {


    List<WhjkZdjywhRuleDto> listByZhxm(QueryWrapper<Object> eq);
}
