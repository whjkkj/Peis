package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.biz.mapper.SysUserMapper;
import com.whjk.system.biz.service.SysUserService;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUser;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    
    @Override
    public List<SysRole> queryUserRole(SysUser sysUser) {
        //查询用户角色
        return this.baseMapper.selectUserRole(sysUser.getId());
    }

    @Override
    public List<SysMenu> queryUserMenu(SysUser sysUser) {
        SysUser User = this.baseMapper.selectOne(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getAccount,sysUser.getAccount()));
        return this.baseMapper.selectUserMenu(User.getId());
    }

    @Override
    public Page<SysUser> pageList(Page<Object> objectPage, QueryWrapper<Object> eq) {
        return this.baseMapper.pageList(objectPage,eq);
    }
}
