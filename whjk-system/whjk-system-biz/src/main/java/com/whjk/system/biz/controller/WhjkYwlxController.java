package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkYwlxService;
import com.whjk.system.data.entity.WhjkKs;
import com.whjk.system.data.entity.WhjkYwlx;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 业务类型设置 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-ywlx")
@Api(tags = "业务类型")
public class WhjkYwlxController {

    @Autowired
    private WhjkYwlxService whjkYwlxService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkYwlx>> page(@RequestBody WhjkYwlx pageDto){
        return Result.ok(whjkYwlxService.pageList(pageDto));
    }

    @PostMapping("/list")
    @ApiOperation("列表")
    public Result<List<WhjkYwlx>> list(){
        return Result.ok(whjkYwlxService.list(new LambdaQueryWrapper<WhjkYwlx>().eq(WhjkYwlx::getDelFlag,0)));
    }


    @PostMapping("/add")
    @ApiOperation("添加业务类型")
    public Result<T> add(@RequestBody WhjkYwlx whjkYwlx){
        whjkYwlxService.save(whjkYwlx);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result update(@RequestBody WhjkYwlx whjkYwlx){
        whjkYwlxService.updateById(whjkYwlx);
        return Result.ok();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkYwlx> getById( @PathVariable("id") String id){
        return Result.ok(whjkYwlxService.getById(id));
    }
}

