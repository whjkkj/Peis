package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkTxzd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 提醒诊断设置 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTxzdMapper extends BaseMapper<WhjkTxzd> {

}
