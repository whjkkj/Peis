package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkBblx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标本类型维护表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkBblxMapper extends BaseMapper<WhjkBblx> {

}
