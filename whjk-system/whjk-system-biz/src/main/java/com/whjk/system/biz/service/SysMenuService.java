package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.SysMenu;

import java.util.List;

/**
 * <p>
 * 系统菜单表 服务类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysMenuService extends IService<SysMenu> {

    List<SysMenu> selectUserRoleMenu(String roleId);

    Page<SysMenu> pageList(SysMenu pageDto);

    List<SysMenu> listMenu();

    List<SysMenu> allListMenu();
}
