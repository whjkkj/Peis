package com.whjk.system.biz.controller;

import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.ApiService;
import com.whjk.system.data.dto.ApiDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/5/005 9:50
 */
@RestController
@RequestMapping("api")
@Api(tags = " api调用")
public class ApiController {
    @Autowired
    ApiService apiService;

    /**
     * His系统回调接口
     */
    @PostMapping("payCallback")
    @ApiOperation(value = "收费变更", notes = "收费变更")
    public Result<T> payCallback(@RequestBody ApiDto apiDto) {
        apiService.payCallback(apiDto);
        return Result.ok();
    }
    /**
     * His系统回调接口
     */
    @PostMapping("refund")
    @ApiOperation(value = "退费", notes = "退费")
    public Result<T> refund(@RequestBody ApiDto apiDto) {
        apiService.refund(apiDto);
        return Result.ok();
    }
}
