package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.biz.mapper.WhjkPersonMapper;
import com.whjk.system.data.dto.ArrangeDto;
import com.whjk.system.data.dto.PersonDto;
import com.whjk.system.data.dto.WhjkPersonDto;
import com.whjk.system.data.entity.WhjkArrange;
import com.whjk.system.biz.mapper.WhjkArrangeMapper;
import com.whjk.system.biz.service.WhjkArrangeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 * 体检回访 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkArrangeServiceImpl extends ServiceImpl<WhjkArrangeMapper, WhjkArrange> implements WhjkArrangeService {

    @Override
    public Page<WhjkPersonDto> arrPage(PersonDto person) {
        return baseMapper.arrPage(new Page<>(person.getCurrent(), person.getSize()), new QueryWrapper<>()
                .eq("t1.del_flag", 0)
                .eq("t1.pe_status",3)
                .like(StringUtils.isNotBlank(person.getRegSerialNo()), "t1.reg_serial_no", person.getRegSerialNo())
                .like(StringUtils.isNotBlank(person.getIdCard()), "t1.id_card", person.getIdCard())
                .like(StringUtils.isNotBlank(person.getName()), "t1.name", person.getName())
                .groupBy("t1.id")
                .orderByDesc("t1.create_time")
                .apply(StringUtils.isNotBlank(person.getPeDate()), "date_format(t1.pe_date,'%Y-%m-%d')='" + person.getPeDate() + "'")
        );
    }

    @Override
    public void saveArrange(ArrangeDto arrangeDto) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime LocalTime = LocalDateTime.parse(arrangeDto.getArrDate()+" 00:00:00",df);
        arrangeDto.setArrTime(LocalTime);
        this.save(arrangeDto);
    }
}
