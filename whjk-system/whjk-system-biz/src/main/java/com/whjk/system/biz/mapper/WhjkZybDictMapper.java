package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkZybDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 职业病字典 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZybDictMapper extends BaseMapper<WhjkZybDict> {

}
