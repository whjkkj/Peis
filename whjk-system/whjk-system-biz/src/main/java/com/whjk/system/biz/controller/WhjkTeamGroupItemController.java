package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkTeamGroupItemService;
import com.whjk.system.data.entity.WhjkTeamGroupItem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 单位分组对应所选项目表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-team-group-item")
@Api(tags = "单位分组对应所选项目")
public class WhjkTeamGroupItemController {
    @Autowired
    private WhjkTeamGroupItemService teamGroupItemService;



    /**
     * 功能描述：删除分组下的项目
     *
     * @param  teamGroupItems
     * @return  返回
     */
    @ApiOperation("删除分组下的项目")
    @PostMapping("deleteTeamGroupItem")
    public Result<?> deleteTeamGroupItem(@RequestBody List<WhjkTeamGroupItem> teamGroupItems) {
        List<WhjkTeamGroupItem> itemsToDelete = teamGroupItems.stream()
                .filter(item -> {
                    LambdaQueryWrapper<WhjkTeamGroupItem> queryWrapper = new LambdaQueryWrapper<>();
                    // 分组下是否存在该项目
                    queryWrapper.eq(WhjkTeamGroupItem::getTeamId, item.getTeamId())
                            .eq(WhjkTeamGroupItem::getGroupId, item.getGroupId())
                            .eq(WhjkTeamGroupItem::getItemId, item.getItemId());
                    return teamGroupItemService.count(queryWrapper) > 0;
                })
                .collect(Collectors.toList());

        if (!itemsToDelete.isEmpty()) {
            List<String> idsToDelete = itemsToDelete.stream()
                    .map(WhjkTeamGroupItem::getId)
                    .collect(Collectors.toList());
            teamGroupItemService.removeByIds(idsToDelete);
        }
        return Result.ok();
    }


}

