package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.biz.mapper.WhjkPackageRelMapper;
import com.whjk.system.biz.mapper.WhjkZdmxMapper;
import com.whjk.system.biz.service.WhjkZdjywhInfoService;
import com.whjk.system.biz.service.WhjkZdmxService;
import com.whjk.system.data.dto.WhjkZdmxDto;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.whjk.system.data.entity.WhjkZdjywhInfo;
import com.whjk.system.data.entity.WhjkZdmx;
import com.whjk.system.data.vo.GeneralVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 个人诊断明细表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkZdmxServiceImpl extends ServiceImpl<WhjkZdmxMapper, WhjkZdmx> implements WhjkZdmxService {

    @Autowired
    private WhjkZdjywhInfoService zdjywhInfoService;

    @Autowired
    private WhjkPackageRelMapper packageRelMapper;

    @Override
    public List<WhjkZdjywh> queryPersonDiagnosis(String peId) {
        return this.baseMapper.selectPersonDiagnosis(peId);
    }

    @Override
    public GeneralVo queryZdmxZdjywhInfo(String id) {
        List<WhjkZdjywhInfo> infoList = zdjywhInfoService.list(new LambdaQueryWrapper<WhjkZdjywhInfo>().eq(WhjkZdjywhInfo::getDelFlag, 0));
        GeneralVo generalVo = new GeneralVo();
        //回显综述
        StringBuilder peConclusion = new StringBuilder();
        List<String> conclusion = packageRelMapper.getConclusion(id);
        conclusion.forEach(f -> peConclusion.append(f));
        generalVo.setPeConclusion(peConclusion.toString());
        //诊断列表以及建议
        List<WhjkZdmxDto> zdmxDtoList = this.baseMapper.selectRelZdmx(new QueryWrapper<>()
                        .eq("wz.pe_id", id)
                        .eq("wz.del_flag", 0)
                        .eq("zhxm.del_flag", 0)
                        .eq("ks.del_flag", 0)
                        .eq("wpr.del_flag", 0))
                .stream().map(m -> {
                    WhjkZdmxDto zdmxDto = new WhjkZdmxDto();
                    BeanUtils.copyProperties(m, zdmxDto);
                    zdmxDto.setInfoList(
                            infoList.stream().filter(f -> f.getZdjyId().equals(zdmxDto.getZdjywhId()))
                                    .collect(Collectors.toList())
                    );
                    return zdmxDto;
                }).collect(Collectors.toList());
        generalVo.setWhjkZdmxDtoList(zdmxDtoList);
        //回显建议
        StringBuilder peAdvice = new StringBuilder();
        zdmxDtoList.forEach(i -> {
            peAdvice.append("【").append(i.getZdjg()).append("】").append("\n");
            int count = 1;
            for (WhjkZdjywhInfo j : i.getInfoList()) {
                peAdvice.append("(").append(count).append(")").append(j.getJynr()).append("\n");
                count++;
            }
        });
        generalVo.setPeAdvice(peAdvice.toString());
        return generalVo;
    }
}
