package com.whjk.system.biz.service;

import com.itextpdf.text.BadElementException;
import com.whjk.system.data.dto.PrintDto;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/4/004 16:40
 */
public interface PdfService {
    void printBarcode(PrintDto dto, HttpServletResponse response)throws Exception ;

    void printZYD(PrintDto printDTO, HttpServletResponse response) throws Exception;

    void printBF(String personId, String type, HttpServletResponse response)throws Exception;
}
