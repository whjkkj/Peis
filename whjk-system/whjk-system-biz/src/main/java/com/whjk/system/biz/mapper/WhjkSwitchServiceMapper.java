package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.WhjkSwitch;

/**
 * <p>
 * His缴费记录表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkSwitchServiceMapper extends BaseMapper<WhjkSwitch> {

}
