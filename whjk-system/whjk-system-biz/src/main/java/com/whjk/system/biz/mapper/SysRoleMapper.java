package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.whjk.system.data.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    SysRole selectUserRole(@Param(Constants.WRAPPER) QueryWrapper<Object> eq);
}
