package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkTxzd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 提醒诊断设置 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTxzdService extends IService<WhjkTxzd> {

}
