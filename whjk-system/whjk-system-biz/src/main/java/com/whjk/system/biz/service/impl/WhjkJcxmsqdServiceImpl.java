package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkJcxmsqd;
import com.whjk.system.biz.mapper.WhjkJcxmsqdMapper;
import com.whjk.system.biz.service.WhjkJcxmsqdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检查项目申请单 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkJcxmsqdServiceImpl extends ServiceImpl<WhjkJcxmsqdMapper, WhjkJcxmsqd> implements WhjkJcxmsqdService {

}
