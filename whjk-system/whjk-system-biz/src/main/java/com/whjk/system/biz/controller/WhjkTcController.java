package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkTcInfoService;
import com.whjk.system.biz.service.WhjkTcService;
import com.whjk.system.data.dto.WhjkTcZhxmDto;
import com.whjk.system.data.dto.WhjkZhxmXmDto;
import com.whjk.system.data.entity.WhjkTc;
import com.whjk.system.data.entity.WhjkTcInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 体检套餐主表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-tc")
@Api(tags = "体检套餐")
public class WhjkTcController {

    @Autowired
    private WhjkTcService whjkTcService;

    @Autowired
    private WhjkTcInfoService tcInfoService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkTc>> page(@RequestBody WhjkTc pageDto){
        return Result.ok(whjkTcService.pageList(pageDto));
    }

    @PostMapping("/list")
    @ApiOperation("列表")
    public Result<List<WhjkTc>> getlist(@RequestBody WhjkTc whjkTc){
        return Result.ok(whjkTcService.getList(whjkTc));
    }

    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@RequestBody WhjkTcZhxmDto whjkTcZhxmDto){
        whjkTcService.addTc(whjkTcZhxmDto);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result<T> update(@RequestBody WhjkTcZhxmDto whjkTcZhxmDto){
        tcInfoService.zhxmAdd(whjkTcZhxmDto);
        return Result.ok();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Result delete(@RequestBody WhjkTc whjkTc){
        whjkTcService.updateById(whjkTc);
        return Result.ok();
    }

    @PostMapping("/treeList")
    @ApiOperation("列表")
    public Result<List<WhjkTc>> list(@RequestBody WhjkTc whjkTc){
        return Result.ok(whjkTcService.list(new LambdaQueryWrapper<WhjkTc>()
                .eq(WhjkTc::getDelFlag,0)
                .like(StringUtils.isNotEmpty(whjkTc.getTcmc()),WhjkTc::getTcmc,whjkTc.getTcmc()))
        );
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkTc> getById(@PathVariable("id") String id){
        return Result.ok(whjkTcService.getById(id));
    }
}

