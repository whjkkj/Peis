package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkBblx;
import com.whjk.system.biz.mapper.WhjkBblxMapper;
import com.whjk.system.biz.service.WhjkBblxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 标本类型维护表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkBblxServiceImpl extends ServiceImpl<WhjkBblxMapper, WhjkBblx> implements WhjkBblxService {

    @Override
    public Page<WhjkBblx> pageList(WhjkBblx pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                    new LambdaQueryWrapper<WhjkBblx>().eq(WhjkBblx::getDelFlag,0)
                        .eq(StringUtils.isNotEmpty(pageDto.getXb()), WhjkBblx::getXb,pageDto.getXb())
                        .like(StringUtils.isNotEmpty(pageDto.getMc()), WhjkBblx::getMc,pageDto.getMc())
                        .orderByDesc(WhjkBblx::getCreateTime)
        );
    }
}
