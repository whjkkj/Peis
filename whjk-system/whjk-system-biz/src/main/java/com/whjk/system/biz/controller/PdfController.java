package com.whjk.system.biz.controller;

import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.PdfService;
import com.whjk.system.data.dto.PrintDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/4/004 16:40
 */
@RestController
@RequestMapping("pdf")
@Api(tags = "生成pdf")
public class PdfController {
    @Autowired
    PdfService pdfService;

    /**
     * 记录条码
     *
     * @param dto
     * @param response
     * @throws Exception
     */
    @PostMapping("/printBarcode")
    @ApiOperation(value = "打印lis条码")
    public Result<T> printBarcode(@RequestBody PrintDto dto, HttpServletResponse response) throws Exception {
        pdfService.printBarcode(dto, response);
        return Result.ok();
    }

    /**
     * 打印指引单
     * @param printDTO
     * @param response
     */
    @PostMapping("/printZYD")
    @ApiOperation(value = "打印指引单")
    public void printZYD(@RequestBody PrintDto printDTO, HttpServletResponse response) throws Exception {
        pdfService.printZYD(printDTO, response);
    }

    /**
     * 打印报告
     * @param response
     */
    @PostMapping("/printBF")
    @ApiOperation(value = "打印报告")
    public void printBF(String personId, String type, HttpServletResponse response) throws Exception {
        pdfService.printBF(personId,type, response);
    }
}
