package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkTodayTjDto;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.vo.TemplateVo;

/**
 * <p>
 * 模板 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTemplateMapper extends BaseMapper<WhjkTemplate> {

    Page<TemplateVo> templatePageList(Page<Object> objectPage, TemplateVo templateVo);

}
