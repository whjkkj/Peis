package com.whjk.system.biz.service;

import com.whjk.system.data.dto.WhjkZdmxDto;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.whjk.system.data.entity.WhjkZdmx;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.vo.GeneralVo;

import java.util.List;

/**
 * <p>
 * 个人诊断明细表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZdmxService extends IService<WhjkZdmx> {

   List<WhjkZdjywh> queryPersonDiagnosis(String peId);

   GeneralVo queryZdmxZdjywhInfo(String id);
}
