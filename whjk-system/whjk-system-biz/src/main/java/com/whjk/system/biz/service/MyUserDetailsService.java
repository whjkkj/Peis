package com.whjk.system.biz.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @date 2022/11/23
 */
public interface MyUserDetailsService extends UserDetailsService {

	/**
	 * 手机号 登录
	 *
	 * @param phone TYPE@CODE
	 * @return UserDetails
	 * @throws UsernameNotFoundException
	 */
	UserDetails loadUserBySocial(String phone) throws UsernameNotFoundException;
}
