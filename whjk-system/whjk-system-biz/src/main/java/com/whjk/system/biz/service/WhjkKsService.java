package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkKs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 体检科室表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkKsService extends IService<WhjkKs> {

    Page<WhjkKs> pageList(WhjkKs pageDto);

    List<WhjkKs> ksTreeList(WhjkKs whjkKs);

    List<WhjkKs> xmTreeList(WhjkKs whjkKs);

}
