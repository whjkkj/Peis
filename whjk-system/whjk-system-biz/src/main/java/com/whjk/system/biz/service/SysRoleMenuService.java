package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.dto.SysRoleMenuDto;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRoleMenu;

import java.util.List;

/**
 * <p>
 * 系统角色菜单表 服务类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    List<SysMenu> queryRoleMenu(String id);

    void addRoleMenu(SysRoleMenuDto sysRoleMenuDto);
}
