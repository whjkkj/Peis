package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkLx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 体检类型设置 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkLxMapper extends BaseMapper<WhjkLx> {

}
