package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkMsg;
import com.whjk.system.biz.mapper.WhjkMsgMapper;
import com.whjk.system.biz.service.WhjkMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 短信模板 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkMsgServiceImpl extends ServiceImpl<WhjkMsgMapper, WhjkMsg> implements WhjkMsgService {

}
