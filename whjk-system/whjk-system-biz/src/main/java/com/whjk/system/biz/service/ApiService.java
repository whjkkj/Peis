package com.whjk.system.biz.service;

import com.whjk.system.data.dto.ApiDto;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/5/005 9:53
 */
public interface ApiService {
    void payCallback(ApiDto apiDto);

    void refund(ApiDto apiDto);
}
