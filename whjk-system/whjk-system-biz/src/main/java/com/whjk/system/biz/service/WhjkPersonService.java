package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.dto.*;
import com.whjk.system.data.entity.WhjkPerson;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 个人体检人员信息 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkPersonService extends IService<WhjkPerson> {

    WhjkPersonDto registration(WhjkPersonDto whjkPersonDto);

    Page<WhjkPersonDto> personPage(PersonDto person);

    void reported(WhjkPersonDto whjkPersonDto);


    void singleChangeSave(ChangeItemSaveReqDto changeItemSaveReqDto);

    WhjkPersonZhxmDto getTree(String id);

    Page<WhjkPerson> TodayTjPage(WhjkTodayTjDto whjkTodayTjDto);

    WhjkPersonDto getByPersonId(String id);

    void personExcel(HttpServletResponse response, PersonDto person) throws Exception;

    void review(WhjkPersonDto whjkPersonDto);

    void importExcelByTemplate(importExcelDto importDTO) throws Exception;

    void saveAdvice(ConclusionAdviceDto conclusionAdviceDto);

    void backAdvice(String peId);

    Page<WhjkPerson> PersonFilePage(WhjkTjdaDto whjkTjdaDto);
}
