package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkYjsbwh;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医技设备维护 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkYjsbwhService extends IService<WhjkYjsbwh> {

}
