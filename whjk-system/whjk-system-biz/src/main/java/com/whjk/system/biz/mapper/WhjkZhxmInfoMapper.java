package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkXm;
import com.whjk.system.data.entity.WhjkZhxmInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 组合项目关联表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZhxmInfoMapper extends BaseMapper<WhjkZhxmInfo> {

    List<WhjkXm> xmList(String id);

}
