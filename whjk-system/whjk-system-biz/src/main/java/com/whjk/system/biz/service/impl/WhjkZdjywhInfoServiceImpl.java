package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkZdjywhInfo;
import com.whjk.system.biz.mapper.WhjkZdjywhInfoMapper;
import com.whjk.system.biz.service.WhjkZdjywhInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诊断建议从表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkZdjywhInfoServiceImpl extends ServiceImpl<WhjkZdjywhInfoMapper, WhjkZdjywhInfo> implements WhjkZdjywhInfoService {

    @Override
    public WhjkZdjywhInfo querySlaveTable(String zdjyId) {
        return this.baseMapper.selectSlaveTable(zdjyId);
    }
}
