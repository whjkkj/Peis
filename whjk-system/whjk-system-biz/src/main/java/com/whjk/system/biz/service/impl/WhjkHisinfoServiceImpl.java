package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkHisinfo;
import com.whjk.system.biz.mapper.WhjkHisinfoMapper;
import com.whjk.system.biz.service.WhjkHisinfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * His缴费记录表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkHisinfoServiceImpl extends ServiceImpl<WhjkHisinfoMapper, WhjkHisinfo> implements WhjkHisinfoService {

}
