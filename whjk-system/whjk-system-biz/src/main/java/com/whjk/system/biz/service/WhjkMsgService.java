package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkMsg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 短信模板 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkMsgService extends IService<WhjkMsg> {

}
