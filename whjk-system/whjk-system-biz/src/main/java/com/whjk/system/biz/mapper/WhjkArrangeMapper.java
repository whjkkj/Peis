package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkPersonDto;
import com.whjk.system.data.entity.WhjkArrange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 体检回访 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkArrangeMapper extends BaseMapper<WhjkArrange> {

    Page<WhjkPersonDto> arrPage(Page<Object> objectPage, @Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);
}
