package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.PageDto;
import com.whjk.system.biz.service.WhjkTeamCheckInfoService;
import com.whjk.system.data.dto.WhjkTeamCheckInfoDto;;

import com.whjk.system.data.entity.WhjkTeamCheckInfo;

import com.whjk.system.data.vo.WHJKteamWaitDetailVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 体检单位 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-team-check-info")
@Api(tags = "体检单位")
public class WhjkTeamCheckInfoController {

    @Autowired
    private WhjkTeamCheckInfoService teamCheckInfoService;

    /**
     * 功能描述：单位登记
     *
     * @param whjkTeamCheckInfo
     * @return 返回成功
     */
    @ApiOperation("单位登记")
    @PostMapping("add")
    public Result<T> addTeamCheckInfo(@RequestBody @Validated(WhjkTeamCheckInfo.add.class) WhjkTeamCheckInfo whjkTeamCheckInfo){
        teamCheckInfoService.addTeamCheckInfo(whjkTeamCheckInfo);
        return Result.ok();
    }

    /**
     * 功能描述：更新单位数据
     *
     * @param whjkTeamCheckInfo
     * @return 返回成功
     */
    @ApiOperation("更新单位数据")
    @PostMapping("update")
    public Result<T> updateTeamCheckInfo(@RequestBody WhjkTeamCheckInfo whjkTeamCheckInfo) {
        teamCheckInfoService.updateTeamCheckInfo(whjkTeamCheckInfo);
        return Result.ok();
    }


    /**
     * 功能描述：查询体检单位分页
     * 查询单位
     *
     * @param teamCheckInfo
     * @return 返回分页数据
     */
    @ApiOperation("查询体检单位数据分页")
    @PostMapping("page")
    public Result<Page<WhjkTeamCheckInfo>> queryUnit(@RequestBody WhjkTeamCheckInfo teamCheckInfo) {
        Page<WhjkTeamCheckInfo> whjkTeamCheckInfoPage = teamCheckInfoService.queryUnit(teamCheckInfo);
        return Result.ok(whjkTeamCheckInfoPage);
    }

    /**
     * 功能描述：查询单个单位下的所有分组
     *
     * @param teamId
     * @return 返回
     */
    @ApiOperation("查询单个单位下分组")
    @GetMapping("/getById/{teamId}")
    public Result<WhjkTeamCheckInfo> queryUnit(@PathVariable("teamId") String teamId) {
        return Result.ok(teamCheckInfoService.getOne(new LambdaQueryWrapper<WhjkTeamCheckInfo>().eq(WhjkTeamCheckInfo::getDelFlag,0)
                .eq(WhjkTeamCheckInfo::getId,teamId)));
    }

    /**
     * 功能描述：查询单个单位
     *
     * @param teamId
     * @return 返回
     */
    @ApiOperation("查询单个单位及其分组")
    @GetMapping("/page/{teamId}")
    public Result<WhjkTeamCheckInfoDto> queryUnitGroup(@PathVariable("teamId") String teamId) {
        return Result.ok(teamCheckInfoService.queryUnitGroup(teamId));
    }

    /**
     * 功能描述：查询所有单位下的分组
     *
     * @param
     * @return 返回所有单位和单位下的父分组以及子分组
     */
    @ApiOperation("查询所有单位下的分组")
    @PostMapping("/pageGroup")
    public Result<List<WhjkTeamCheckInfoDto>> queryTeamInfoList(@RequestBody PageDto pageDto) {
        List<WhjkTeamCheckInfoDto> whjkTeamCheckInfoDtos = teamCheckInfoService.queryTeamInfoList(pageDto);
        return Result.ok(whjkTeamCheckInfoDtos);
    }


    /**
     * 功能描述：查询所有单位下的团检待检人员
     *
     * @param
     * @return 返回团检待检人员人数以及单位名称以及ID
     */
    @ApiOperation("团检待检人员")
    @PostMapping("/teamWaitPerson")
    public Result<Page<WhjkTeamCheckInfo>> waitInspectionPerson(@RequestBody WhjkTeamCheckInfo teamCheckInfo) {
        return Result.ok(teamCheckInfoService.waitInspectionPerson(teamCheckInfo));
    }

    /**
     * 功能描述：查询单位下的团检待检详情
     *
     * @param
     * @return 返回
     */
    @ApiOperation("团检待检详情")
    @GetMapping("/teamWaitDetail/{groupId}")
    public Result<WHJKteamWaitDetailVo> teamWaitDetail(@PathVariable("groupId") String groupId) {
        return Result.ok(teamCheckInfoService.teamWaitDetail(groupId));
    }

}

