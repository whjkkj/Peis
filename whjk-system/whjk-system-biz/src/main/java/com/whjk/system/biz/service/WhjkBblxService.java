package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkBblx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 标本类型维护表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkBblxService extends IService<WhjkBblx> {

    Page<WhjkBblx> pageList(WhjkBblx pageDto);

}
