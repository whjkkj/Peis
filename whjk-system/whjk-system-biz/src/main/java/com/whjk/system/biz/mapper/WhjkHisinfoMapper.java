package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkHisinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * His缴费记录表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkHisinfoMapper extends BaseMapper<WhjkHisinfo> {

}
