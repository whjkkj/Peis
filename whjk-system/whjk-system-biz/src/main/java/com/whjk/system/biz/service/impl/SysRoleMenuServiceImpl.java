package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.biz.mapper.SysRoleMenuMapper;
import com.whjk.system.biz.service.SysRoleMenuService;
import com.whjk.system.data.dto.SysRoleMenuDto;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统角色菜单表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {
    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public List<SysMenu> queryRoleMenu(String RoleId) {
        return sysRoleMenuMapper.selectMenuByRoleId(RoleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addRoleMenu(SysRoleMenuDto sysRoleMenuDto) {
        SysRoleMenu sysRoleMenu = new SysRoleMenu();
        sysRoleMenu.setDelFlag("1");
        this.baseMapper.update(sysRoleMenu, new LambdaQueryWrapper<SysRoleMenu>()
                .eq(SysRoleMenu::getRoleId, sysRoleMenuDto.getRoleId()));
        List<String> menuId = sysRoleMenuDto.getMenuId();
        List<SysRoleMenu> collect = menuId.stream().map(f -> {
            SysRoleMenu roleMenu = new SysRoleMenu();
            roleMenu.setRoleId(sysRoleMenuDto.getRoleId());
            roleMenu.setMenuId(f);
            return roleMenu;
        }).collect(Collectors.toList());
        this.saveBatch(collect);
    }
}
