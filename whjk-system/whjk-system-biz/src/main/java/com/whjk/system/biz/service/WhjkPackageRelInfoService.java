package com.whjk.system.biz.service;

import com.whjk.system.data.dto.WhjkPersonResultDto;
import com.whjk.system.data.dto.WhjkZdjywhRuleDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.vo.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 诊断结果报告 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkPackageRelInfoService extends IService<WhjkPackageRelInfo> {

    WhjkPersonResultDto getPersonResult(String id);

    List<PackageRelInfoVo> getBeforeResult(String id);

    List<PackageRelVo> getSummarizeResult(String id);

    List<BeforeContrastResultVo> getContrastResult(String id);

    List<AnomalyOverviewVo> getAnomaly(String relId);

    List<MedicalAdviceVo> medicalAdvice(String relId);

    BeforeContrastResultVo  getResult(String peid, String zhxmId);

    List<BeforeContrastResultVo> summaryItem(String idCard);
}
