package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.core.security.config.FileUploadConfig;
import com.whjk.system.biz.service.*;
import com.whjk.system.data.dto.WhjkPersonResultDto;
import com.whjk.system.data.entity.*;
import com.whjk.system.data.uitls.DocTestUtil;
import com.whjk.system.data.vo.InspectVo;
import com.whjk.system.data.vo.SVo;
import com.whjk.system.data.vo.TemplateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 模板 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-template")
@Api(tags = "模板")
public class WhjkTemplateController {

    @Autowired
    private WhjkTemplateService whjkTemplateService;

    /**
     * 模板分页查询
     *
     * @param templateVo
     * @return
     */
    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<TemplateVo>> page(@RequestBody TemplateVo templateVo) {
        return Result.ok(whjkTemplateService.pageList(templateVo));
    }

    /**
     * 添加新模板
     *
     * @param whjkTemplate
     * @return
     */
    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@RequestBody WhjkTemplate whjkTemplate) {
        whjkTemplateService.save(whjkTemplate);
        return Result.ok();
    }

    /**
     * 修改模板
     *
     * @param whjkTemplate
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("修改")
    public Result update(@RequestBody WhjkTemplate whjkTemplate) {
        whjkTemplateService.updateById(whjkTemplate);
        return Result.ok();
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkTemplate> getById(@PathVariable("id") String id) {
        return Result.ok(whjkTemplateService.getById(id));
    }

    /**
     * 测试返回文件流
     *
     * @param response
     */
    @Inner
    @GetMapping("/aa")
    @ApiOperation("aa")
    public void aa(HttpServletResponse response) {
        returnFileIo(response);
    }

    private void returnFileIo(HttpServletResponse response) {
        ServletOutputStream out = null;
        FileInputStream in = null;
        File file = new File("E:\\data\\tempfile\\wordTemplate\\2021-10-25\\健康检查总检报告.docx");
        try {
            in = new FileInputStream(file);

            //设置文件ContentType类型
            response.setContentType("application/octet-stream");

            response.addHeader("Content-Length", String.valueOf(file.length()));

            //response.setContentType("application/pdf");
//            //设置文件头：最后一个参数是设置下载文件名
//            response.setHeader("Content-Disposition",
//                    "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
            out = response.getOutputStream();
            // 读取文件流
            int len = 0;
            byte[] buffer = new byte[1024 * 10];
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            out.flush();
        } catch (Exception e) {
            System.out.println("pdf文件流返回失败");
        } finally {
            try {
                out.close();
                in.close();
            } catch (Exception e) {
                System.out.println("关闭流失败");
            }
        }
    }

    /**
     * 功能描述：预览模板信息数据,写入数据
     *
     * @param id WhjkPackageRel对象Id
     */
    @ApiOperation("预览模板信息数据,写入数据")
    @PostMapping("/text")
    public Result<Object> text(@RequestParam("id") String id) throws Exception {
        Assert.isTrue(StringUtils.isNotBlank(id), "预览模板信息数据失败: 参数为空！");
        return Result.ok(whjkTemplateService.getTemplateText(id));
    }

}

