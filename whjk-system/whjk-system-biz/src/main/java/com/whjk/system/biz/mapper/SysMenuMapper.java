package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.whjk.system.data.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectUserRoleMenu(@Param(Constants.WRAPPER)QueryWrapper<Object> objectQueryWrapper);
}
