package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.SysRole;

/**
 * <p>
 * 系统角色表 服务类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysRoleService extends IService<SysRole> {

    SysRole selectUserRole(String id);
}
