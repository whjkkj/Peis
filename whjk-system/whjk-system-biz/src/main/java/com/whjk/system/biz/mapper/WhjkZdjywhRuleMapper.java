package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.whjk.system.data.dto.WhjkZdjywhRuleDto;
import com.whjk.system.data.entity.WhjkZdjywhRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 规则 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-07-11
 */
public interface WhjkZdjywhRuleMapper extends BaseMapper<WhjkZdjywhRule> {

    List<WhjkZdjywhRuleDto> listByZhxm(@Param(Constants.WRAPPER)QueryWrapper<Object> eq);
}
