package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.biz.mapper.SysRoleMapper;
import com.whjk.system.biz.service.SysRoleService;
import com.whjk.system.data.entity.SysRole;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Override
    public SysRole selectUserRole(String id) {
        return this.baseMapper.selectUserRole(new QueryWrapper<>().eq("ur.user_id",id));
    }
}
