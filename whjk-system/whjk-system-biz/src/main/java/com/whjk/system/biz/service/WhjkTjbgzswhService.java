package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkTjbgzswh;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 体检报告展示维护(大文本信息维护) 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTjbgzswhService extends IService<WhjkTjbgzswh> {

    Page<WhjkTjbgzswh> pageList(WhjkTjbgzswh pageDto);

}
