/*
Copyright [2020] [https://www.stylefeng.cn]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Guns采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Guns源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/stylefeng/guns-separation
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/stylefeng/guns-separation
6.若您的项目无法满足以上几点，可申请商业授权，获取Guns商业授权许可，请在官网购买授权，地址为 https://www.stylefeng.cn
 */
package com.whjk.system.biz.controller;


import com.whjk.system.biz.model.BaseParam;
import com.whjk.system.biz.timer.param.SysTimersParam;
import com.whjk.system.biz.timer.service.SysTimersService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 定时任务 控制器
 *
 * @author stylefeng
 * @date 2020/6/30 18:26
 */
@RestController
public class SysTimersController {

    @Resource
    private SysTimersService sysTimersService;

    /**
     * 分页查询定时任务
     *
     * @author stylefeng
     * @date 2020/6/30 18:26
     */
    @GetMapping("/sysTimers/page")
    public Object page(SysTimersParam sysTimersParam) {
        return sysTimersService.page(sysTimersParam);
    }

    /**
     * 获取全部定时任务
     *
     * @author stylefeng
     * @date 2020/6/30 18:26
     */
    @GetMapping("/sysTimers/list")
    public Object list(SysTimersParam sysTimersParam) {
        return sysTimersService.list(sysTimersParam);
    }

    /**
     * 查看详情定时任务
     *
     * @author stylefeng
     * @date 2020/6/30 18:26
     */
    @GetMapping("/sysTimers/detail")
    public Object detail(@Validated(BaseParam.detail.class) SysTimersParam sysTimersParam) {
        return sysTimersService.detail(sysTimersParam);
    }

    /**
     * 添加定时任务
     *
     * @author stylefeng
     * @date 2020/6/30 18:26
     */
    @PostMapping("/sysTimers/add")
    public Object add(@RequestBody @Validated(BaseParam.add.class) SysTimersParam sysTimersParam) {
        sysTimersService.add(sysTimersParam);
        return "ok";
    }

    /**
     * 删除定时任务
     *
     * @author stylefeng
     * @date 2020/6/30 18:26
     */
    @PostMapping("/sysTimers/delete")
    public Object delete(@RequestBody @Validated(BaseParam.delete.class) SysTimersParam sysTimersParam) {
        sysTimersService.delete(sysTimersParam);
        return "ok";
    }

    /**
     * 编辑定时任务
     *
     * @author stylefeng
     * @date 2020/6/30 18:26
     */
    @PostMapping("/sysTimers/edit")
    public Object edit(@RequestBody @Validated(BaseParam.edit.class) SysTimersParam sysTimersParam) {
        sysTimersService.edit(sysTimersParam);
        return "ok";
    }

    /**
     * 获取系统的所有任务列表
     *
     * @author stylefeng
     * @date 2020/7/1 14:34
     */
    @PostMapping("/sysTimers/getActionClasses")
    public Object getActionClasses() {
        return sysTimersService.getActionClasses();
    }

    /**
     * 启动定时任务
     *
     * @author stylefeng
     * @date 2020/7/1 14:34
     */
    @PostMapping("/sysTimers/start")
    public Object start(@RequestBody @Validated(BaseParam.start.class) SysTimersParam sysTimersParam) {
        sysTimersService.start(sysTimersParam);
        return "ok";
    }

    /**
     * 停止定时任务
     *
     * @author stylefeng
     * @date 2020/7/1 14:34
     */
    @PostMapping("/sysTimers/stop")
    public Object stop(@RequestBody @Validated(BaseParam.stop.class) SysTimersParam sysTimersParam) {
        sysTimersService.stop(sysTimersParam);
        return "ok";
    }

}