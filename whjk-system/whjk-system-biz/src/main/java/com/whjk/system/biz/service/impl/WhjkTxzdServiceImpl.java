package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkTxzd;
import com.whjk.system.biz.mapper.WhjkTxzdMapper;
import com.whjk.system.biz.service.WhjkTxzdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提醒诊断设置 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkTxzdServiceImpl extends ServiceImpl<WhjkTxzdMapper, WhjkTxzd> implements WhjkTxzdService {

}
