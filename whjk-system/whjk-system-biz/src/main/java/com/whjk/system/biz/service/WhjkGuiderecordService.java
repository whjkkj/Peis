package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkGuiderecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 指引单图片上传记录表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkGuiderecordService extends IService<WhjkGuiderecord> {

}
