package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUserRole;

/**
 * <p>
 * 系统用户角色表 服务类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    SysRole queryUserRole(String UserId);


}
