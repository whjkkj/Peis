package com.whjk.system.biz.timer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.biz.timer.entity.SysTimers;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @date 2020/6/30 18:26
 */
@Mapper
public interface SysTimersMapper extends BaseMapper<SysTimers> {

}
