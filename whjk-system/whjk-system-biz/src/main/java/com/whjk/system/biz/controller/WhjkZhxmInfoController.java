package com.whjk.system.biz.controller;

import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkZhxmInfoService;
import com.whjk.system.data.dto.WhjkZhxmXmDto;
import com.whjk.system.data.entity.WhjkXm;
import com.whjk.system.data.entity.WhjkZhxm;
import com.whjk.system.data.entity.WhjkZhxmInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 组合项目关联表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-zhxm-info")
@Api(tags = "体检组合子项关联")
public class WhjkZhxmInfoController {

    @Autowired
    private WhjkZhxmInfoService whjkZhxmInfoService;


    @PostMapping("/list")
    @ApiOperation("子项列表")
    public Result<List<WhjkXm>> list(@RequestBody WhjkZhxm whjkZhxm){
        return Result.ok(whjkZhxmInfoService.xmList(whjkZhxm));
    }


    @PostMapping("/delete")
    @ApiOperation("删除子项")
    public Result<T> delete(@RequestBody WhjkZhxmInfo whjkZhxmInfo){
        whjkZhxmInfoService.updateById(whjkZhxmInfo);
        return Result.ok();
    }

}

