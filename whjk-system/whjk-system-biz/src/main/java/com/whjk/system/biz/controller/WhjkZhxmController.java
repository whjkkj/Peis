package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkZhxmInfoService;
import com.whjk.system.biz.service.WhjkZhxmService;
import com.whjk.system.data.dto.WhjkZhxmXmDto;
import com.whjk.system.data.entity.WhjkZhxm;
import com.whjk.system.data.po.TcZhxmPo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 组合项目主表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-zhxm")
@Api(tags = "体检组合")
public class WhjkZhxmController {

    @Autowired
    private WhjkZhxmService whjkZhxmService;

    @Autowired
    private WhjkZhxmInfoService zhxmInfoService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkZhxm>> page(@RequestBody WhjkZhxm pageDto){
        return Result.ok(whjkZhxmService.pageList(pageDto));
    }

    @PostMapping("/po/page")
    @ApiOperation("PO分页")
    public Result<Page<TcZhxmPo>> page(@RequestBody TcZhxmPo pageDto){
        return Result.ok(whjkZhxmService.poPageList(pageDto));
    }

    @PostMapping("/po/list")
    @ApiOperation("PO列表")
    public Result<List<TcZhxmPo>> list(@RequestBody TcZhxmPo tcZhxmPo){
        return Result.ok(whjkZhxmService.poList(tcZhxmPo));
    }

    @PostMapping("/list")
    @ApiOperation("全列表")
    public Result<List<WhjkZhxm>> alllist(){
        return Result.ok(whjkZhxmService.list(new LambdaQueryWrapper<WhjkZhxm>().eq(WhjkZhxm::getDelFlag,0)));
    }

    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@RequestBody WhjkZhxmXmDto whjkZhxmXmDto){
        whjkZhxmService.addZhxm(whjkZhxmXmDto);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result<T> update(@RequestBody WhjkZhxmXmDto whjkZhxmXmDto){
        zhxmInfoService.xmAdd(whjkZhxmXmDto);
        return Result.ok();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Result delete(@RequestBody WhjkZhxm whjkZhxm){
        whjkZhxmService.updateById(whjkZhxm);
        return Result.ok();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkZhxm> getById(@PathVariable("id") String id){
        return Result.ok(whjkZhxmService.getById(id));
    }
}

