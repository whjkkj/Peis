package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkPersonDto;
import com.whjk.system.data.dto.WhjkRelZhxmDto;
import com.whjk.system.data.dto.WhjkTodayTjDto;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkPersonExcel;
import com.whjk.system.data.entity.WhjkXm;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 个人体检人员信息 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkPersonMapper extends BaseMapper<WhjkPerson> {

    List<WhjkXm> findJcxmInfoByZhId(@Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);

    Page<WhjkPersonDto> personPage(Page<Object> objectPage, @Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);

    List<WhjkRelZhxmDto> getPersonZhxms(String id);

    WhjkPersonDto getByIdVo(@Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);

    Page<WhjkPerson> todayTjPerson(Page<Object> objectPage, WhjkTodayTjDto todayTjDto);
}
