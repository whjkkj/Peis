package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkTc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 体检套餐主表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTcMapper extends BaseMapper<WhjkTc> {

}
