package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkTeamGroupItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 单位分组对应所选项目表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTeamGroupItemService extends IService<WhjkTeamGroupItem> {

}
