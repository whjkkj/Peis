package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkTcInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.WhjkZhxm;
import com.whjk.system.data.po.TcZhxmPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 套餐关联表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Mapper
public interface WhjkTcInfoMapper extends BaseMapper<WhjkTcInfo> {

    List<WhjkZhxm> zhxmList(String id);

    List<TcZhxmPo> allZhxmList();

}
