package com.whjk.system.biz.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkPackageRelInfoService;
import com.whjk.system.biz.service.WhjkPersonService;
import com.whjk.system.data.dto.WhjkTjdaDto;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.vo.AnomalyOverviewVo;
import com.whjk.system.data.vo.BeforeContrastResultVo;
import com.whjk.system.data.vo.MedicalAdviceVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 体检档案 前端控制器
 * </p>
 *
 * @author cs
 * @since 2023-08-01
 */
@RestController
@RequestMapping("/whjk-tjda")
public class WhjkTjdaController {

    @Autowired
    private WhjkPersonService whjkPersonService;

    @Autowired
    private WhjkPackageRelInfoService whjkPackageRelInfoService;

    @PostMapping("/page")
    @ApiOperation("体检人员档案")
    public Result<Page<WhjkPerson>> PersonFile(@RequestBody WhjkTjdaDto whjkTjdaDto){
        return Result.ok(whjkPersonService.PersonFilePage(whjkTjdaDto));
    }


    @GetMapping("/getInfo/{idCard}")
    @ApiOperation("个人基本信息")
    public Result<List<WhjkPerson>> PersonInfo(@PathVariable("idCard") String idCard){

        return Result.ok( whjkPersonService.list(new QueryWrapper<WhjkPerson>().eq("del_flag",0)
                .eq("ins_mark", 1)
                .eq("id_card",idCard)
        ));
    }

    @GetMapping("/getAnomaly/{peid}")
    @ApiOperation("异常综述")
    public Result<List<AnomalyOverviewVo>> getAnomaly(@PathVariable("peid") String peid){
        return Result.ok(whjkPackageRelInfoService.getAnomaly(peid));
    }


    @GetMapping("/medicalAdvice/{peid}")
    @ApiOperation("医生异常建议")
    public Result<List<MedicalAdviceVo>> medicalAdvice(@PathVariable("peid") String peid){
        return Result.ok(whjkPackageRelInfoService.medicalAdvice(peid));
    }

    @GetMapping("/getResult/{peid}/{zhxmId}")
    @ApiOperation("获取项目结果")
    public Result<BeforeContrastResultVo> getResult(@PathVariable("peid") String peid, @PathVariable("zhxmId") String zhxmId){
        return Result.ok(whjkPackageRelInfoService.getResult(peid,zhxmId));
    }

    @GetMapping("/getItem/{idCard}")
    @ApiOperation("获取体检汇总项目列")
    public Result<List<BeforeContrastResultVo>> summaryItem(@PathVariable("idCard") String idCard){
        return Result.ok(whjkPackageRelInfoService.summaryItem(idCard));
    }


}

