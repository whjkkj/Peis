package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.utils.PinYinUtil;
import com.whjk.system.biz.mapper.WhjkTcInfoMapper;
import com.whjk.system.biz.service.WhjkTcInfoService;
import com.whjk.system.data.dto.WhjkTcZhxmDto;
import com.whjk.system.data.entity.WhjkTc;
import com.whjk.system.biz.mapper.WhjkTcMapper;
import com.whjk.system.biz.service.WhjkTcService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.data.entity.WhjkTcInfo;
import com.whjk.system.data.po.TcZhxmPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 体检套餐主表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkTcServiceImpl extends ServiceImpl<WhjkTcMapper, WhjkTc> implements WhjkTcService {

    @Autowired
    private WhjkTcInfoService whjkTcInfoService;

    @Autowired
    private WhjkTcInfoMapper tcInfoMapper;

    @Override
    public Page<WhjkTc> pageList(WhjkTc pageDto) {
        //所有组合
        List<TcZhxmPo> zhxmList = tcInfoMapper.allZhxmList();
        Page<WhjkTc> page = this.page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkTc>().eq(WhjkTc::getDelFlag, 0)//按xssx和创建时间排序
                        .like(StringUtils.isNotEmpty(pageDto.getTcmc()), WhjkTc::getTcmc, pageDto.getTcmc())
                        .like(StringUtils.isNotEmpty(pageDto.getPinyinCode()), WhjkTc::getPinyinCode, pageDto.getPinyinCode())
                        .like(StringUtils.isNotEmpty(pageDto.getJc()), WhjkTc::getJc, pageDto.getJc())
                        .eq(StringUtils.isNotEmpty(pageDto.getSyfw()), WhjkTc::getSyfw, pageDto.getSyfw())
                        .orderByAsc(WhjkTc::getXssx)
                        .orderByDesc(WhjkTc::getCreateTime));
        page.getRecords().forEach(i -> {
            i.setZhxmList(zhxmList.stream().filter(j -> j.getTcId().equals(i.getId())).collect(Collectors.toList()));
        });
        return page;
    }

    @Override
    public List<WhjkTc> getList(WhjkTc pageDto) {
        //所有组合
        List<TcZhxmPo> zhxmList = tcInfoMapper.allZhxmList();
        //所有套餐
        List<WhjkTc> tcList = this.list(new LambdaQueryWrapper<WhjkTc>()
                .eq(WhjkTc::getDelFlag,0)//按xssx和创建时间排序
                .like(StringUtils.isNotEmpty(pageDto.getTcmc()),WhjkTc::getTcmc,pageDto.getTcmc())
                .like(StringUtils.isNotEmpty(pageDto.getPinyinCode()),WhjkTc::getPinyinCode,pageDto.getPinyinCode())
                .like(StringUtils.isNotEmpty(pageDto.getJc()),WhjkTc::getJc,pageDto.getJc())
                .eq(StringUtils.isNotEmpty(pageDto.getSyfw()),WhjkTc::getSyfw,pageDto.getSyfw())
                .orderByAsc(WhjkTc::getXssx)
                .orderByDesc(WhjkTc::getCreateTime));
        //套餐组合匹配
        return tcList.stream().peek(i -> {
            i.setZhxmList(zhxmList.stream().filter(j -> j.getTcId().equals(i.getId())).collect(Collectors.toList()));
        }).collect(Collectors.toList());
    }

    /**
     * 添加套餐信息以及套餐中组合
     * @param whjkTcZhxmDto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addTc(WhjkTcZhxmDto whjkTcZhxmDto) {
        //提前获取id和编号
        whjkTcZhxmDto.setId(Long.toString(IdWorker.getId(whjkTcZhxmDto)));
        whjkTcZhxmDto.setBh(whjkTcZhxmDto.getId());
        //将套餐名称转为拼音
        whjkTcZhxmDto.setPinyinCode(PinYinUtil.getPinyin(whjkTcZhxmDto.getTcmc()));
        //存入套餐表
        this.save(whjkTcZhxmDto);
        //组合集合存入关联表
        List<WhjkTcInfo> infoList = whjkTcZhxmDto.getWhjkZhxmList().stream().map(i -> {
            WhjkTcInfo info = new WhjkTcInfo();
            info.setTcId(whjkTcZhxmDto.getId());
            info.setZhxmId(i.getId());
            info.setBzjg(i.getBzjg());
            info.setDz(i.getDz());
            info.setSjjg(i.getSjjg());
            return info;
        }).collect(Collectors.toList());
        whjkTcInfoService.saveBatch(infoList);
    }
}
