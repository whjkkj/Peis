package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkJcxmsqdInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 检查项目申请单关联表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkJcxmsqdInfoService extends IService<WhjkJcxmsqdInfo> {

}
