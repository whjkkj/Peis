package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.biz.mapper.WhjkKsMapper;
import com.whjk.system.biz.service.WhjkKsService;
import com.whjk.system.biz.service.WhjkXmService;
import com.whjk.system.biz.service.WhjkZhxmService;
import com.whjk.system.data.entity.WhjkKs;
import com.whjk.system.data.entity.WhjkXm;
import com.whjk.system.data.entity.WhjkZhxm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 体检科室表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkKsServiceImpl extends ServiceImpl<WhjkKsMapper, WhjkKs> implements WhjkKsService {

    @Autowired
    private WhjkZhxmService whjkZhxmService;

    @Autowired
    private WhjkXmService whjkXmService;

    @Override
    public Page<WhjkKs> pageList(WhjkKs pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkKs>().eq(WhjkKs::getDelFlag, 0)
                        .like(StringUtils.isNotEmpty(pageDto.getKsmc()), WhjkKs::getKsmc, pageDto.getKsmc())
                        .eq(StringUtils.isNotEmpty(pageDto.getKslx()), WhjkKs::getKslx, pageDto.getKslx())
                        .eq(StringUtils.isNotEmpty(pageDto.getSfqy()), WhjkKs::getSfqy, pageDto.getSfqy())
                        .orderByAsc(WhjkKs::getXssx)
                        .orderByDesc(WhjkKs::getCreateTime)
        );
    }

    @Override
    public List<WhjkKs> ksTreeList(WhjkKs whjkKs) {
        List<WhjkKs> ksList = this.list(new LambdaQueryWrapper<WhjkKs>()
                .eq(WhjkKs::getDelFlag, 0)
                .like(StringUtils.isNotEmpty(whjkKs.getKsmc()), WhjkKs::getKsmc, whjkKs.getKsmc())
                .orderByAsc(WhjkKs::getKslx)
                .orderByAsc(WhjkKs::getXssx)
                .orderByDesc(WhjkKs::getCreateTime));
        List<WhjkZhxm> zhxmList = whjkZhxmService.list(new LambdaQueryWrapper<WhjkZhxm>()
                .eq(WhjkZhxm::getDelFlag, 0));
        // 遍历 ksList，并为每个对象设置对应的 zhxmList
        for (WhjkKs ks : ksList) {
            ks.setZhxmList(zhxmList.stream()
                    .filter(i -> ks.getId().equals(i.getTjksId()))
                    .collect(Collectors.toList()));
        }
        return ksList;
    }

    @Override
    public List<WhjkKs> xmTreeList(WhjkKs whjkKs) {
        List<WhjkKs> ksList = this.list(new LambdaQueryWrapper<WhjkKs>()
                .eq(WhjkKs::getDelFlag, 0)
                .like(StringUtils.isNotEmpty(whjkKs.getKsmc()), WhjkKs::getKsmc, whjkKs.getKsmc())
                .orderByAsc(WhjkKs::getKslx)
                .orderByAsc(WhjkKs::getXssx)
                .orderByDesc(WhjkKs::getCreateTime));
        List<WhjkXm> xmList = whjkXmService.list(new LambdaQueryWrapper<WhjkXm>().eq(WhjkXm::getDelFlag, 0));
        // 遍历 ksList，并为每个对象设置对应的 xmList
        for (WhjkKs ks : ksList) {
            ks.setXmList(xmList.stream()
                    .filter(i->ks.getId().equals(i.getTjksId()))
                    .collect(Collectors.toList()));
        }
        return ksList;
    }
}
