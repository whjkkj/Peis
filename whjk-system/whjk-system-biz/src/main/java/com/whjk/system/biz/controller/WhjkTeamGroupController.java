package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkTeamGroupService;
import com.whjk.system.data.dto.WhjkTeamGroupDto;
import com.whjk.system.data.entity.WhjkTeamGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 单位分组信息表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-team-group")
@Api(tags = "单位分组信息")
public class WhjkTeamGroupController {
    @Autowired
    private WhjkTeamGroupService teamGroupService;


    /**
     * 功能描述：新增单位分组
     *
     * @param teamGroupDto
     * @return 返回
     */
    @ApiOperation("新增单位分组")
    @PostMapping("add")
    public Result<WhjkTeamGroup> addTeamGroup(@Validated(WhjkTeamGroup.add.class) @RequestBody WhjkTeamGroupDto teamGroupDto) {
        //单位，是否存在该分组
        int count = teamGroupService.count(new LambdaQueryWrapper<WhjkTeamGroup>().eq(WhjkTeamGroup::getTeamId, teamGroupDto.getTeamId())
                .eq(WhjkTeamGroup::getGroupName, teamGroupDto.getGroupName()));
        Assert.isTrue(count == 0, "单位分组已存在");
        teamGroupService.addTeamGroup(teamGroupDto);
        return Result.ok();
    }

    /**
     * 功能描述：更新单位分组数据
     *
     * @param teamGroupDto
     * @return 返回
     */
    @ApiOperation("更新单位分组及项目")
    @PostMapping("update")
    public Result<WhjkTeamGroup> updateTeamGroup(@RequestBody WhjkTeamGroupDto teamGroupDto) {
        teamGroupService.updateGroupItem(teamGroupDto);
        return Result.ok();
    }


    /**
     * 功能描述：查询单位下的分组分页
     *
     * @param teamGroup
     * @return 返回分页数据
     */
    @ApiOperation("查询单位下的分组分页")
    @PostMapping("page")
    public Result<Page<WhjkTeamGroup>> queryTeamGroupList(@RequestBody WhjkTeamGroup teamGroup) {
        Page<WhjkTeamGroup> whjkTeamCheckInfoIPage = teamGroupService.queryTeamGroupListByPage(new Page<>(teamGroup.getCurrent(), teamGroup.getSize()), teamGroup);
        return Result.ok(whjkTeamCheckInfoIPage);
    }

    /**
     * 功能描述：查询分组下列表
     *
     * @param groupId
     * @return 返回
     */
    @ApiOperation("查询分组下列表")
    @GetMapping("/queryTeamGroupList/{id}")
    public Result<WhjkTeamGroupDto> queryTeamGroupList(@PathVariable("id") String groupId) {
        WhjkTeamGroupDto whjkTeamGroupDto = teamGroupService.queryTeamGroupList(groupId);
        return Result.ok(whjkTeamGroupDto);
    }
}

