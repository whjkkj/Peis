package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkBblxService;
import com.whjk.system.data.entity.WhjkBblx;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 标本类型维护表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-bblx")
@Api(tags = "标本类型")
public class WhjkBblxController {

    @Autowired
    private WhjkBblxService bblxService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkBblx>> page(@RequestBody WhjkBblx pageDto){
        return Result.ok(bblxService.pageList(pageDto));
    }

    @PostMapping("/list")
    @ApiOperation("列表")
    public Result<List<WhjkBblx>> list(){
        return Result.ok(bblxService.list(new LambdaQueryWrapper<WhjkBblx>().eq(WhjkBblx::getDelFlag,0)));
    }


    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@RequestBody WhjkBblx whjkBblx){
        bblxService.save(whjkBblx);
        return Result.ok();
    }


    @PostMapping("/update")
    @ApiOperation("修改")
    public Result update(@RequestBody WhjkBblx whjkBblx){
        bblxService.updateById(whjkBblx);
        return Result.ok();
    }


    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkBblx> getById(@PathVariable("id") String id){
        return Result.ok(bblxService.getById(id));
    }

}

