package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.whjk.system.biz.service.WhjkTcService;
import com.whjk.system.biz.service.WhjkZhxmService;
import com.whjk.system.data.dto.WhjkTcZhxmDto;
import com.whjk.system.data.entity.*;
import com.whjk.system.biz.mapper.WhjkTcInfoMapper;
import com.whjk.system.biz.service.WhjkTcInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 套餐关联表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkTcInfoServiceImpl extends ServiceImpl<WhjkTcInfoMapper, WhjkTcInfo> implements WhjkTcInfoService {

    @Autowired
    private WhjkTcService whjkTcService;

    /**
     * 套餐中组合列表
     * @param whjkTc
     * @return
     */
    @Override
    public List<WhjkZhxm> zhxmList(WhjkTc whjkTc) {
        return this.baseMapper.zhxmList(whjkTc.getId());
    }

    /**
     * 修改套餐中及套餐中的组合
     * @param whjkTcZhxmDto
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void zhxmAdd(WhjkTcZhxmDto whjkTcZhxmDto) {
        whjkTcService.updateById(whjkTcZhxmDto);
        //删除原来的zhxmList
        this.update(new UpdateWrapper<WhjkTcInfo>()
                .set("del_flag", "1")
                .eq("tc_id", whjkTcZhxmDto.getId())
                .eq("del_flag", 0));
        List<WhjkTcInfo> infoList = whjkTcZhxmDto.getWhjkZhxmList().stream().map(i ->{
            WhjkTcInfo info = new WhjkTcInfo();
            info.setTcId(whjkTcZhxmDto.getId());
            info.setZhxmId(i.getId());
            info.setBzjg(i.getBzjg());
            info.setSjjg(i.getSjjg());
            info.setDz(i.getDz());
            return info;
        }).collect(Collectors.toList());
        this.saveBatch(infoList);
    }

}
