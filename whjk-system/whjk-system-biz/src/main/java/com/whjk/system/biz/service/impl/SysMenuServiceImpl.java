package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.core.security.uitls.SecurityUtils;
import com.whjk.system.biz.mapper.SysMenuMapper;
import com.whjk.system.biz.service.SysMenuService;
import com.whjk.system.biz.service.SysRoleMenuService;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRoleMenu;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统菜单表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Autowired
    SysRoleMenuService roleMenuService;

    @Override
    public List<SysMenu> selectUserRoleMenu(String roleId) {
        return this.baseMapper.selectUserRoleMenu(new QueryWrapper<>()
                .select("sm.icon", "sm.router", "sm.component", "sm.permission", "sm.pid", "sm.code", "sm.name", "sm.redirect")
                .eq("sm.del_flag", 0)
                .eq("rm.del_flag", 0)
                .eq("sm.status", 0)
                .eq("rm.role_id", roleId));
    }

    @Override
    public Page<SysMenu> pageList(SysMenu pageDto) {
        // 构建分页查询条件
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()), new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getDelFlag, 0)
                .orderByAsc(SysMenu::getSort)// 先按sort字段升序排序，再按照创建时间降序排序
                .orderByDesc(SysMenu::getCreateTime)); // 添加按照创建时间降序排序)
    }


    /**
     * 查询菜单
     *
     * @return
     */
    @Override
    public List<SysMenu> listMenu() {
        String roleId = SecurityUtils.getUser().getRoleId();
        List<String> collect = roleMenuService.list(new LambdaQueryWrapper<SysRoleMenu>()
                .eq(SysRoleMenu::getRoleId, roleId)
                .eq(SysRoleMenu::getDelFlag,0)).stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
        List<SysMenu> list = this.list(new LambdaQueryWrapper<SysMenu>()
                .in(collect.size() > 0, SysMenu::getId, collect)
                .eq(SysMenu::getDelFlag, 0)
                .orderByAsc(SysMenu::getSort));
        return list.stream()
                //遍历查询父菜单
                .filter(f -> StringUtils.isBlank(f.getPid()))
                //获取父菜单下的子菜单
                .peek(f -> f.setChildList(childMenu(f.getCode(), list))).collect(Collectors.toList());
    }

    @Override
    public List<SysMenu> allListMenu() {
        List<SysMenu> list = this.list(new LambdaQueryWrapper<SysMenu>()
                .eq(SysMenu::getDelFlag, 0)
                .orderByAsc(SysMenu::getSort));
        return list.stream()
                //遍历查询父菜单
                .filter(f -> StringUtils.isBlank(f.getPid()))
                //获取父菜单下的子菜单
                .peek(f -> f.setChildList(childMenu(f.getCode(), list))).collect(Collectors.toList());
    }

    /**
     * 子菜单递归函数
     *
     * @param code
     * @param list
     * @return
     */
    public List<SysMenu> childMenu(String code, List<SysMenu> list) {
        return list.stream()
                //子菜单pid==父菜单code
                .filter(f -> code.equals(f.getPid()))
                //获取子菜单的子菜单
                .peek(f -> f.setChildList(childMenu(f.getCode(), list))).collect(Collectors.toList());
    }
}
