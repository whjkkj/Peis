package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.StatisticsDto;
import com.whjk.system.data.dto.WhjkPackageRelDto;
import com.whjk.system.data.entity.WhjkPackageRel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.po.WhjkPackageRelPo;
import com.whjk.system.data.vo.ReviewVo;
import com.whjk.system.data.vo.ZdXjVo;
import com.whjk.system.data.vo.PackageRelCountVo;

import java.util.List;


/**
 * <p>
 * 体检登记项目明细关联表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkPackageRelService extends IService<WhjkPackageRel> {

    List<WhjkPackageRelPo> selectByCheckStatus(QueryWrapper<Object> eq);

    ZdXjVo generateSection(WhjkPackageRelDto packageRelDto);

    void saveResults(WhjkPackageRelDto packageRelDto);

    Page<PackageRelCountVo> countDoctors(Page<Object> Page, StatisticsDto statisticsDto);

    List<WhjkPerson> ProjectPersonDetails(StatisticsDto statisticsDto);

    List<ReviewVo> getReviewList(String peId);
}
