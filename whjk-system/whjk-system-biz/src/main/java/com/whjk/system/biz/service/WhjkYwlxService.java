package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkYwlx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 业务类型设置 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkYwlxService extends IService<WhjkYwlx> {

    Page<WhjkYwlx> pageList(WhjkYwlx pageDto);
}
