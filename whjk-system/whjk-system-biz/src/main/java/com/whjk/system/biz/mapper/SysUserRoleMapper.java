package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUserRole;

/**
 * <p>
 * 系统用户角色表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    SysRole selectUserByRoleId(String userId);
}
