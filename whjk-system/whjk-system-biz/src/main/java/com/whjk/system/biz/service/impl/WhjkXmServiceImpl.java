package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.utils.NoUtils;
import com.whjk.system.data.entity.WhjkXm;
import com.whjk.system.biz.mapper.WhjkXmMapper;
import com.whjk.system.biz.service.WhjkXmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 体检项目表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkXmServiceImpl extends ServiceImpl<WhjkXmMapper, WhjkXm> implements WhjkXmService {

    @Autowired
    NoUtils noUtils;

    @Override
    public Page<WhjkXm> pageList(WhjkXm pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkXm>().eq(WhjkXm::getDelFlag,0)  //按xssx和创建时间排序
                    .eq(StringUtils.isNotEmpty(pageDto.getTjksId()),WhjkXm::getTjksId,pageDto.getTjksId())
                    .eq(StringUtils.isNotEmpty(pageDto.getLclxId()),WhjkXm::getLclxId,pageDto.getLclxId())
                    .like(StringUtils.isNotEmpty(pageDto.getXmmc()),WhjkXm::getXmmc,pageDto.getXmmc())
                    .orderByAsc(WhjkXm::getXssx)
                    .orderByDesc(WhjkXm::getCreateTime)
            );
    }

    @Override
    public List<WhjkXm> allList(WhjkXm whjkXm) {
        return this.list(new LambdaQueryWrapper<WhjkXm>()
                .eq(WhjkXm::getDelFlag,0)
                .eq(WhjkXm::getTjksId,whjkXm.getTjksId())
                .like(StringUtils.isNotEmpty(whjkXm.getXmmc()),WhjkXm::getXmmc,whjkXm.getXmmc())
        );
    }

    @Override
    public List<WhjkXm> allListByZhxmId(String zhxmId) {
        return this.baseMapper.allListByZhxmId(zhxmId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addXm(WhjkXm whjkXm) {
        whjkXm.setXmbh(noUtils.generateNumJcxm());
        this.save(whjkXm);
    }

}
