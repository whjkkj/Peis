package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkYwlx;
import com.whjk.system.biz.mapper.WhjkYwlxMapper;
import com.whjk.system.biz.service.WhjkYwlxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务类型设置 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkYwlxServiceImpl extends ServiceImpl<WhjkYwlxMapper, WhjkYwlx> implements WhjkYwlxService {

    @Override
    public Page<WhjkYwlx> pageList(WhjkYwlx pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkYwlx>().eq(WhjkYwlx::getDelFlag,0)
                        .like(StringUtils.isNotEmpty(pageDto.getMc()),WhjkYwlx::getMc,pageDto.getMc())
                        .eq(StringUtils.isNotEmpty(pageDto.getSfqy()),WhjkYwlx::getSfqy,pageDto.getSfqy())
                        .orderByDesc(WhjkYwlx::getCreateTime)
                );
    }
}
