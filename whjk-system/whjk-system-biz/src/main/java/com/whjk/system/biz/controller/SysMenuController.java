package com.whjk.system.biz.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.annotation.WebLog;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.SysMenuService;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.uitls.DocTestUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "菜单", tags = {"菜单"})
@RestController
@RequestMapping(value = "menu")
public class SysMenuController {
    @Autowired
    private SysMenuService menuService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<SysMenu>> page(@RequestBody SysMenu pageDto) {
        //return Result.ok(menuService.page(new Page<>(pageDto.getCurrent(), pageDto.getSize())));
        // 执行分页查询并返回结果
        return Result.ok(menuService.pageList(pageDto));
    }


    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@Validated(SysMenu.review.class) @RequestBody SysMenu sysMenu) {
        menuService.save(sysMenu);
        return Result.ok();
    }


    @PostMapping("/update")
    @ApiOperation("更新")
    public Result<T> update(@RequestBody SysMenu sysMenu) {
        menuService.updateById(sysMenu);
        return Result.ok();
    }

    @GetMapping("/list")
    @ApiOperation("用户权限菜单列表")
    public Result<List<SysMenu>> list() {
        return Result.ok(menuService.listMenu());
    }

    @GetMapping("/allList")
    @ApiOperation("菜单列表")
    public Result<List<SysMenu>> allList() {
        return Result.ok(menuService.allListMenu());
    }
}
