package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.core.security.uitls.SecurityUtils;
import com.whjk.system.biz.service.WhjkPersonService;
import com.whjk.system.biz.service.WhjkReportrecordService;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkReportrecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 * 体检报告领取记录表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Api(value = "体检报告领取记录表", tags = {"体检报告领取记录表"})
@RestController
@RequestMapping("/whjk-reportrecord")
public class WhjkReportrecordController {
    @Autowired
    WhjkReportrecordService reportrecordService;
    @Autowired
    WhjkPersonService personService;

    @PostMapping("/save")
    @ApiOperation("确认发放")
    public Result<T> save(@RequestBody @Validated(BaseDto.recordList.class) WhjkReportrecord reportrecord) {
        WhjkReportrecord one = reportrecordService.getOne(new LambdaQueryWrapper<WhjkReportrecord>()
                .eq(WhjkReportrecord::getPeId, reportrecord.getPeId())
                .eq(WhjkReportrecord::getDelFlag, 0));
        reportrecord.setIssuser(SecurityUtils.getUser().getName());
        reportrecord.setIssTime(LocalDateTime.now());
        if (one != null) {
            reportrecord.setId(one.getId());
            reportrecordService.updateById(reportrecord);
        } else {
            reportrecordService.save(reportrecord);
        }
        personService.updateById(WhjkPerson.builder().id(reportrecord.getPeId()).process("7").receiveDoctor(SecurityUtils.getUser().getName())
                .receiveTime(LocalDateTime.now()).build());
        return Result.ok();
    }

    @PostMapping("/getByPeId/{peId}")
    @ApiOperation("查询确认发放")
    public Result<WhjkReportrecord> getByPeId(@PathVariable("peId") String peId) {
        WhjkReportrecord one = reportrecordService.getOne(new LambdaQueryWrapper<WhjkReportrecord>()
                .eq(WhjkReportrecord::getPeId, peId)
                .eq(WhjkReportrecord::getDelFlag, 0));
        return Result.ok(one);
    }
}

