package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.utils.PinYinUtil;
import com.whjk.system.biz.service.WhjkXmService;
import com.whjk.system.biz.service.WhjkZdjywhService;
import com.whjk.system.data.dto.WhjkCjjgwhDto;
import com.whjk.system.data.entity.WhjkCjjgwh;
import com.whjk.system.biz.mapper.WhjkCjjgwhMapper;
import com.whjk.system.biz.service.WhjkCjjgwhService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.data.entity.WhjkXm;
import com.whjk.system.data.entity.WhjkZdjywh;
import io.micrometer.core.instrument.binder.BaseUnits;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 常见结果维护 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkCjjgwhServiceImpl extends ServiceImpl<WhjkCjjgwhMapper, WhjkCjjgwh> implements WhjkCjjgwhService {
    @Autowired
    private WhjkXmService whjkXmService;

    @Autowired
    private WhjkZdjywhService whjkZdjywhService;


    @Override
    public List<WhjkZdjywh> getListNew(String tjksId) {
        List<String> matchedTjxmIds = whjkXmService.listObjs(
                new LambdaQueryWrapper<WhjkXm>()
                        .select(WhjkXm::getXmbh)
                        .eq(WhjkXm::getDelFlag, 0)
                        .eq(WhjkXm::getTjksId, tjksId),
                obj -> (String) obj
        );

        List<String> matchedZdIds = this.baseMapper.selectList(
                new LambdaQueryWrapper<WhjkCjjgwh>()
                        .eq(WhjkCjjgwh::getDelFlag, 0)
                        .in(WhjkCjjgwh::getTjxmId, matchedTjxmIds)
        ).stream().map(WhjkCjjgwh::getZdId).distinct().collect(Collectors.toList());

        //List<WhjkZdjywh> whjkZdjywhs = whjkZdjywhService.listByIds(matchedZdIds);
        List<WhjkZdjywh> whjkZdjywhs;
        if (matchedZdIds.size() > 0) {
            whjkZdjywhs = whjkZdjywhService.listByIds(matchedZdIds);
        } else {
            whjkZdjywhs = new ArrayList<>();
        }

        return whjkZdjywhs;
    }

    @Override
    public void saveCommon(WhjkCjjgwhDto whjkCjjgwhDto) {
        int count = this.baseMapper.selectCount(new LambdaQueryWrapper<WhjkCjjgwh>().eq(WhjkCjjgwh::getDelFlag,0)
                .eq(WhjkCjjgwh::getTjxmId,whjkCjjgwhDto.getTjxmId())
                .eq(WhjkCjjgwh::getMx,whjkCjjgwhDto.getMx()));
        Assert.isTrue(count == 0, "结果已存在");

        WhjkCjjgwh whjkCjjgwh = new WhjkCjjgwh();
        BeanUtils.copyProperties(whjkCjjgwhDto, whjkCjjgwh);
        whjkCjjgwh.setPyjm(PinYinUtil.getPinyin(whjkCjjgwh.getMx()));

        //暂未未处理是否高低，是否进入小结
        // 调用Mapper层进行数据库插入操作
        this.baseMapper.insert(whjkCjjgwh);
    }

    @Override
    public Page<WhjkCjjgwh> commonPage(WhjkCjjgwh whjkCjjgwh) {
        return baseMapper.commonPage(new Page<>(whjkCjjgwh.getCurrent(), whjkCjjgwh.getSize()), new QueryWrapper<>()
                .eq("t1.del_flag", 0)
                .eq(StringUtils.isNotBlank(whjkCjjgwh.getTjxmId()), "t1.tjxm_Id", whjkCjjgwh.getTjxmId())
                .eq(StringUtils.isNotBlank(whjkCjjgwh.getZdId()), "t2.ZdId", whjkCjjgwh.getZdId()));
    }
}
