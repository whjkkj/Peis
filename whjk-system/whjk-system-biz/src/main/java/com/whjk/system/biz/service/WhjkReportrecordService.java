package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkReportrecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 体检报告领取记录表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkReportrecordService extends IService<WhjkReportrecord> {

}
