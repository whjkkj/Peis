package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkZybDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 职业病字典 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZybDictService extends IService<WhjkZybDict> {

}
