package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkXm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 体检项目表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkXmMapper extends BaseMapper<WhjkXm> {
    List<WhjkXm> allListByZhxmId(String zhxmId);
}
