package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.system.biz.service.WhjkCjjgwhService;
import com.whjk.system.data.dto.WhjkCjjgwhDto;
import com.whjk.system.data.dto.WhjkZdjywhDto;
import com.whjk.system.data.entity.WhjkCjjgwh;
import com.whjk.system.data.entity.WhjkTeamGroupItem;
import com.whjk.system.data.entity.WhjkZdjywh;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 常见结果维护 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@ApiOperation("常见结果维护")
@RestController
@RequestMapping("/whjk-cjjgwh")
public class WhjkCjjgwhController {

    @Autowired
    private WhjkCjjgwhService whjkCjjgwhService;


    /**
     * 功能描述：新增常见结果
     *
     * @param commonResult
     * @return 返回
     */
    @ApiOperation("新增常见结果")
    @PostMapping("add")
    public Result<WhjkCjjgwh> commonDiagnostic(@RequestBody @Validated({BaseDto.add.class}) WhjkCjjgwhDto commonResult) {
        whjkCjjgwhService.saveCommon(commonResult);
        return Result.ok();
    }

    /**
     * 功能描述：更新常见结果
     *
     * @param commonResult
     * @return 返回结果
     */
    @ApiOperation("更新")
    @PostMapping("update")
    public Result<WhjkCjjgwh> updateResult(@RequestBody @Validated({BaseDto.update.class}) WhjkCjjgwh commonResult) {
        whjkCjjgwhService.updateById(commonResult);
        return Result.ok();
    }

    /**
     * 功能描述：分页
     *
     * @param commonResult
     * @return 返回结果
     */
    @ApiOperation("分页常见结果")
    @PostMapping("page")
    public Result<Page<WhjkCjjgwh>> commonPage(@RequestBody WhjkCjjgwh commonResult) {
        return Result.ok(whjkCjjgwhService.commonPage(commonResult));
    }

    /**
     * 功能描述：更改是否启用小结
     *
     * @param commonResult
     * @return 返回结果
     */
    @ApiOperation("更新启用状态")
    @PostMapping("isEnable")
    public Result<WhjkCjjgwh> isEnable(@RequestBody WhjkCjjgwh commonResult) {
        return Result.ok();
    }


    /**
     * 功能描述：科室诊断名称
     *
     * @param tjksId
     * @return 返回结果
     */
    @ApiOperation("查看该科室的诊断")
    @GetMapping("getListNew/{tjksId}")
    public Result<List<WhjkZdjywh>> getListNew(@PathVariable(value = "tjksId") String tjksId) {
        return Result.ok(whjkCjjgwhService.getListNew(tjksId));
    }


}

