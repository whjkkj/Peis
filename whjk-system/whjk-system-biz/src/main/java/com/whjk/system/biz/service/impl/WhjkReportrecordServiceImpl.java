package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkReportrecord;
import com.whjk.system.biz.mapper.WhjkReportrecordMapper;
import com.whjk.system.biz.service.WhjkReportrecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体检报告领取记录表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkReportrecordServiceImpl extends ServiceImpl<WhjkReportrecordMapper, WhjkReportrecord> implements WhjkReportrecordService {

}
