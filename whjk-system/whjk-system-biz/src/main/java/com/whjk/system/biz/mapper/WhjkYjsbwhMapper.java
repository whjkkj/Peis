package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkYjsbwh;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医技设备维护 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkYjsbwhMapper extends BaseMapper<WhjkYjsbwh> {

}
