package com.whjk.system.biz.controller;


import com.google.gson.Gson;
import com.whjk.core.common.api.Result;
import com.whjk.core.security.component.UserAuthentication;
import com.whjk.system.biz.service.impl.MyUserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;


/**
 * @author hl
 * @date 2021-07-14 00:19:58
 */
@RestController
@RequestMapping("test")
@Api(tags = " 测试接口")
public class AppUserController {
    @Resource
    MyUserDetailsService myUserDetailsService;

    @ApiOperation("测试接口")
    @PostMapping("/a")
    public Result index() {
        return Result.ok("成功");
    }

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("123456"));
        System.out.println(new BCryptPasswordEncoder().matches("123", "$2a$10$1epp7UWNkM9np7QjfJgaPenzshslauiHBgN4AkaJkvwNlHXpFSi1O"));
        System.out.println(UUID.randomUUID());
    }

    @PostMapping("/role")
    //@PreAuthorize("hasAnyAuthority('ROLE_a')")
    public String roleInfo(int a) {
        Assert.isTrue(a == 1, "dsdsdsadas");
        return "需要获得ROLE_a权限，才可以访问";
    }


    // @PreAuthorize("hasAnyAuthority('kdream')")
    @PostMapping("/roles")
    @Secured("ROLE_bb")
    public String rolekdream() {
        UserAuthentication userDetails = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(new Gson().toJson(userDetails));
        System.out.println(userDetails.getExtendUserBean());
        return "需要获得kdream权限，才可以访问";
    }
}



