package com.whjk.system.biz.service;

import com.whjk.system.data.dto.WhjkTcZhxmDto;
import com.whjk.system.data.entity.WhjkTc;
import com.whjk.system.data.entity.WhjkTcInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.WhjkZhxm;

import java.util.List;

/**
 * <p>
 * 套餐关联表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTcInfoService extends IService<WhjkTcInfo> {

    List<WhjkZhxm> zhxmList(WhjkTc whjkTc);

    void zhxmAdd(WhjkTcZhxmDto whjkTcZhxmDto);

}
