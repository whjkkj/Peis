package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkZdmxService;
import com.whjk.system.data.dto.WhjkZdmxDto;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.whjk.system.data.entity.WhjkZdmx;
import com.whjk.system.data.vo.GeneralVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 个人诊断明细表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-zdmx")
@Api(tags = "个人诊断明细")
public class WhjkZdmxController {
    @Autowired
    private WhjkZdmxService whjkZdmxService;

    /**
     * 功能描述：新增个人明细诊断
     *
     * @param  whjkZdmx
     * @return  返回
     */
    @ApiOperation("新增个人明细诊断")
    @PostMapping("addZdjywhInfo")
    public Result<WhjkZdmx> addPersonDiagnosis(@RequestBody WhjkZdmx whjkZdmx) {
        whjkZdmxService.save(whjkZdmx);
        return Result.ok();
    }

    /**
     * 功能描述：更新个人明细诊断
     *
     * @param  whjkZdmx
     * @return  返回
     */
    @ApiOperation("更新个人明细诊断")
    @PostMapping("updateZdjywhInfo")
    public  Result<WhjkZdmx> updatePersonDiagnosis(@RequestBody WhjkZdmx whjkZdmx) {
        whjkZdmxService.updateById(whjkZdmx);
        return Result.ok();
    }

    /**
     * 功能描述：查询个人诊断明细
     *
     * @param  peId
     * @return  返回
     */
    @ApiOperation("查询个人诊断明细")
    @GetMapping("/queryZdjywhInfo/{peId}")
    public  Result<List<WhjkZdjywh>> queryPersonDiagnosis(@PathVariable("peId") String peId) {
        return Result.ok(whjkZdmxService.queryPersonDiagnosis(peId));
    }

    /**
     * 查询个人明细以及建议从表
     * @param id
     * @return
     */
    @ApiOperation("查询个人明细以及建议从表")
    @GetMapping("/zdmxZdjywhInfo/{id}")
    public  Result<GeneralVo> queryZdmxZdjywhInfo(@PathVariable("id") String id) {
        return Result.ok(whjkZdmxService.queryZdmxZdjywhInfo(id));
    }

    /**
     * 批量删除诊断明细
     * @param ids
     * @return
     */
    @ApiOperation("批量删除诊断明细")
    @PostMapping("/del")
    public  Result<T> zdmxDel(@RequestBody List<String> ids) {
        Assert.isTrue(ids.size() > 0, "数组不能为空");
        whjkZdmxService.update(WhjkZdmx.builder().delFlag("1").build(), new LambdaQueryWrapper<WhjkZdmx>()
                .in(WhjkZdmx::getId, ids));
        return Result.ok();
    }
}

