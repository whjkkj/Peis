package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkTeamGroupItem;
import com.whjk.system.biz.mapper.WhjkTeamGroupItemMapper;
import com.whjk.system.biz.service.WhjkTeamGroupItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 单位分组对应所选项目表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkTeamGroupItemServiceImpl extends ServiceImpl<WhjkTeamGroupItemMapper, WhjkTeamGroupItem> implements WhjkTeamGroupItemService {

}
