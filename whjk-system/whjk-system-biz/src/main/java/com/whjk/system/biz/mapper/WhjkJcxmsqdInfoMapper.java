package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkJcxmsqdInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 检查项目申请单关联表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkJcxmsqdInfoMapper extends BaseMapper<WhjkJcxmsqdInfo> {

}
