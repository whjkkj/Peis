package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.data.dto.WhjkPackageRelDto;
import com.whjk.system.data.dto.WhjkPackageRelInfoDto;
import com.whjk.system.data.dto.WhjkZdjywhDto;
import com.whjk.system.data.dto.WhjkZdjywhRuleDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 诊断建议主表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZdjywhService extends IService<WhjkZdjywh> {

    WhjkZdjywhDto queryMasterSlaveTable(String id);

    void updateZdjywh(WhjkZdjywhDto zdjywhDto);

    void addSuggestionTable(WhjkZdjywhDto zdjywhDto);

    Page<WhjkZdjywhDto> queryProposalForm(WhjkZdjywhDto proposalForm);

    Page<WhjkZdjywh> getByzhxmId(WhjkZdjywhRuleDto whjkZdjywhRuleDto);

    void deleteZdjywh(WhjkZdjywhDto zdjywhDto);

    List<WhjkZdjywhDto> getZdResult(WhjkPackageRelDto packageRelDto);
}
