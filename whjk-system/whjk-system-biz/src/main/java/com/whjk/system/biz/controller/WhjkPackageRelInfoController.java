package com.whjk.system.biz.controller;


import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkPackageRelInfoService;
import com.whjk.system.data.dto.WhjkPersonResultDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import com.whjk.system.data.vo.BeforeContrastResultVo;
import com.whjk.system.data.vo.PackageRelInfoVo;
import com.whjk.system.data.vo.PackageRelVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 诊断结果报告 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-package-rel-info")
@Api(value = "体检结果", tags = {"体检结果"})
public class WhjkPackageRelInfoController {

    @Autowired
    private WhjkPackageRelInfoService whjkPackageRelInfoService;

    /**
     * 查询体检子项结果
     * @param id
     * @return
     */
    @GetMapping("/personResult/{id}")
    @ApiOperation("根据relID查询本次体检子项结果")
    public Result<WhjkPersonResultDto> getPersonResult(@PathVariable("id") String id){
        return Result.ok(whjkPackageRelInfoService.getPersonResult(id));
    }

    /**
     * 查询历次体检子项结果
     * @param id
     * @return
     */
    @GetMapping("/beforeResult/{id}")
    @ApiOperation("根据relID查询历次体检子项结果")
    public Result<List<PackageRelInfoVo>> getBeforeResult(@PathVariable("id") String id){
        return Result.ok(whjkPackageRelInfoService.getBeforeResult(id));
    }

    /**
     * 查询体检总检结果
     * @param id
     * @return
     */
    @GetMapping("/summarizeResult/{id}")
    @ApiOperation("根据用户id查询该用户本次体检结果汇总")
    public Result<List<PackageRelVo>> getSummarizeResult(@PathVariable("id") String id){
        return Result.ok(whjkPackageRelInfoService.getSummarizeResult(id));
    }

    /**
     * 查询体检历次结果对比
     * @param id
     * @return
     */
    @GetMapping("/contrastResult/{id}")
    @ApiOperation("根据用户id查询该用户历次结果对比")
    public Result<List<BeforeContrastResultVo>> getContrastResult(@PathVariable("id") String id){
        return Result.ok(whjkPackageRelInfoService.getContrastResult(id));
    }
}

