package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.whjk.system.data.dto.WhjkPackageRelInfoDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.vo.BeforeContrastResultVo;
import com.whjk.system.data.vo.MedicalAdviceVo;
import com.whjk.system.data.vo.PackageRelInfoVo;
import com.whjk.system.data.vo.PackageRelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 诊断结果报告 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkPackageRelInfoMapper extends BaseMapper<WhjkPackageRelInfo> {

    List<String> getAllPeId(String id);

    List<WhjkPackageRelInfoDto> getSummarizeResult(String id);

    PackageRelVo getPackageRelVo(String id);

    List<WhjkPackageRelInfoDto> getXm(@Param(Constants.WRAPPER) QueryWrapper<Object> eq);

    List<BeforeContrastResultVo> getBeforeResultZhxm(List<String> ids);

    List<PackageRelInfoVo> getBeforeResultXm(List<String> peIds, String zhxmId);

    List<MedicalAdviceVo> queryMedicalAdvice(@Param(Constants.WRAPPER) QueryWrapper<Object> gt);
}
