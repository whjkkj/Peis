package com.whjk.system.biz.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.core.security.uitls.SecurityUtils;
import com.whjk.system.biz.service.SysUserRoleService;
import com.whjk.system.biz.service.SysUserService;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUser;
import com.whjk.system.data.entity.SysUserRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "系统用户", tags = {"系统用户"})
@RestController
@RequestMapping(value = "user")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService userRoleService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<SysUser>> page(@RequestBody SysUser pageDto) {
        return Result.ok(sysUserService.pageList(new Page<>(pageDto.getCurrent(), pageDto.getSize()), new QueryWrapper<>()
                .eq("su.del_flag", 0)));
    }

    @PostMapping("/info")
    @ApiOperation("获取用户信息")
    public Result<SysUser> info() {
        String id = SecurityUtils.getUser().getId();
        SysUser byId = sysUserService.getById(id);
        byId.setPassword("");
        return Result.ok(byId);
    }

    @PostMapping("/add")
    @ApiOperation("新增用户")
    public Result<T> addUser(@RequestBody @Validated(SysUser.add.class) SysUser sysUser) {
        if (StringUtils.isNotBlank(sysUser.getPassword())) {
            String encode = new BCryptPasswordEncoder().encode(sysUser.getPassword());
            sysUser.setPassword(encode);
        }
        sysUserService.save(sysUser);
        userRoleService.remove(new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getUserId, sysUser.getId()));
        SysUserRole userRole = new SysUserRole();
        userRole.setUserId(sysUser.getId());
        userRole.setRoleId(sysUser.getRoleId());
        userRoleService.save(userRole);
        return Result.ok();
    }


    @PostMapping("/update")
    @ApiOperation("修改用户")
    public Result updateUser(@RequestBody @Validated(SysUser.update.class) SysUser sysUser) {
        //修改用户信息
        sysUserService.updateById(sysUser);
        //修改用户权限
        SysUserRole userRole = new SysUserRole();
        userRole.setRoleId(sysUser.getRoleId());
        userRoleService.update(userRole, new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getUserId, sysUser.getId()));
        return Result.ok();
    }

    @PostMapping("/update/role")
    @ApiOperation("修改用户角色")
    public Result updateRole(@RequestBody @Validated(BaseDto.add.class) SysUser sysUser) {
        SysUserRole userRole = new SysUserRole();
        userRole.setRoleId(sysUser.getRoleId());
        userRoleService.update(userRole, new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getUserId, sysUser.getId()));
        return Result.ok();
    }

    @PostMapping("/update/password")
    @ApiOperation("修改密码")
    public Result updatePassword(@RequestBody SysUser sysUser) {
        if (StringUtils.isNotBlank(sysUser.getPassword())) {
            String encode = new BCryptPasswordEncoder().encode(sysUser.getPassword());
            sysUser.setPassword(encode);
        }
        sysUserService.updateById(sysUser);
        return Result.ok();
    }


    @PostMapping("/query/role")
    @ApiOperation("查询用户角色")
    public Result queryUserRole(@RequestBody SysUser sysUser) {
        List<SysRole> sysRoles = sysUserService.queryUserRole(sysUser);
        return Result.ok(sysRoles);
    }


    @PostMapping("/query/menu")
    @ApiOperation("查询用户角色菜单")
    public Result queryUserMenu(@RequestBody SysUser sysUser) {
        List<SysMenu> sysRoles = sysUserService.queryUserMenu(sysUser);
        return Result.ok(sysRoles);
    }
}