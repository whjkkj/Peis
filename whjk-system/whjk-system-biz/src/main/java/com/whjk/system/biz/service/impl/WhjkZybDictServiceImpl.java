package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkZybDict;
import com.whjk.system.biz.mapper.WhjkZybDictMapper;
import com.whjk.system.biz.service.WhjkZybDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 职业病字典 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkZybDictServiceImpl extends ServiceImpl<WhjkZybDictMapper, WhjkZybDict> implements WhjkZybDictService {

}
