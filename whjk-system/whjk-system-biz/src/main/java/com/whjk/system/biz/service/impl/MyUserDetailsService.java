package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whjk.core.security.component.UserAuthentication;
import com.whjk.core.security.entity.SecurityUserEntity;
import com.whjk.system.biz.service.SysMenuService;
import com.whjk.system.biz.service.SysRoleService;
import com.whjk.system.biz.service.SysUserService;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service("MyUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    SysUserService userService;
    @Autowired
    SysRoleService roleService;
    @Autowired
    SysMenuService menuService;

    //根据 账号查询用户信息
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //将来连接数据库根据账号查询用户信息
        //暂时采用模拟数据
        // UserDetails build = User.withUsername("admin").password("123").authorities("a").build();

        //数据库查询用户
        SysUser sysUser = userService.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getAccount, username)
                .eq(SysUser::getDelFlag, 0));
        Assert.notNull(sysUser, "账号不存在");
        //查询角色
        SysRole role = roleService.selectUserRole(sysUser.getId());
        Assert.notNull(role, "该用户没有角色!!");
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        List<SysMenu> menuList;
        try {
            menuList = menuService.selectUserRoleMenu(role.getId());
        } catch (Exception e) {
            throw new RuntimeException("菜单没配置!!!");
        }
        //查询菜单
        UserAuthentication myUser = new UserAuthentication(username, sysUser.getPassword(), authorities);
        myUser.setExtendUserBean(SecurityUserEntity.builder().id(sysUser.getId()).name(sysUser.getName()).account(sysUser.getAccount()).password(sysUser.getPassword()).roleId(role.getId()).build());
        myUser.setRoleCode(role.getCode());
        myUser.setMenu(menuList);
        // UserDetails build = User.withUsername(username).password(securityUserEntity.getPassword()).roles("a").build();
        //return new User(username, securityUserEntity.getPassword(), authorities);
        return myUser;
    }

}
