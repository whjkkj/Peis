package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.core.security.uitls.SecurityUtils;
import com.whjk.system.biz.service.WhjkPersonService;
import com.whjk.system.biz.service.WhjkTeamGroupService;
import com.whjk.system.data.dto.*;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.entity.WhjkTeamGroup;
import com.whjk.system.data.uitls.ExcelImportUtils;
import com.whjk.system.data.vo.InquiryTemplateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 个人体检人员信息 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Api(value = "体检人员信息", tags = {"体检人员信息"})
@RestController
@RequestMapping("/whjk-person")
public class WhjkPersonController {
    @Autowired
    private WhjkPersonService personService;

    @PostMapping("/registration")
    @ApiOperation("个人体检登记or团体预登记")
    public Result<WhjkPersonDto> registration(@RequestBody @Validated(BaseDto.recordList.class) WhjkPersonDto whjkPersonDto) {
        WhjkPersonDto registration = personService.registration(whjkPersonDto);
        return Result.ok(registration);
    }
    /**
     * 下载导入模板
     */
    @ApiOperation("下载导入模板")
    @GetMapping("/download")
    public void downloadTemplateInquiry(HttpServletResponse response) throws Exception {
        //要导出的数据
        List<InquiryTemplateVo> userList = new ArrayList<>();
        //导出-下载
        ExcelImportUtils<InquiryTemplateVo> excelImportUtils = new ExcelImportUtils<>(InquiryTemplateVo.class);
        //如下面两个方法颠倒位置，则下拉无效，先设置下拉，再下载。
       // excelImportUtils.setExcelDropdownValue("groupName", collect); //设置对应字段的下拉列表的值
        excelImportUtils.downLoad(response, userList, "导入模板", "预登记表");  //下载
    }

    /**
     * 功能描述：根据模板（分组）导入数据
     * <p>
     *
     * @return
     */
    @PostMapping("/importExcel")
    @ApiOperation("团体导入")
    public Result importExcelByTemplate(importExcelDto importDTO) throws Exception {
        personService.importExcelByTemplate(importDTO);
        return Result.ok();
    }

    @PostMapping("/reported")
    @ApiOperation("团体体检报道")
    public Result<T> reported(@RequestBody @Validated WhjkPersonDto whjkPersonDto) {
        personService.reported(whjkPersonDto);
        return Result.ok();
    }

    @PostMapping("/singleChangeSave")
    @ApiOperation("变更项目")
    public Result<T> singleChangeSave(@RequestBody @Validated ChangeItemSaveReqDto changeItemSaveReqDto) {
        personService.singleChangeSave(changeItemSaveReqDto);
        return Result.ok();
    }


    @PostMapping("/person/page")
    @ApiOperation("体检用户列表")
    public Result<Page<WhjkPersonDto>> personPage(@RequestBody PersonDto person) {
        return Result.ok(personService.personPage(person));
    }

    @PostMapping("/person/update")
    @ApiOperation("体检用户修改")
    public Result<T> personUpdate(@RequestBody WhjkPerson person) {
        personService.updateById(person);
        return Result.ok();
    }

    @PostMapping("/person/del")
    @ApiOperation("体检用户删除")
    public Result<T> personDel(@RequestBody List<String> ids) {
        Assert.isTrue(ids.size() > 0, "数组不能为空");
        personService.update(WhjkPerson.builder().delFlag("1").build(), new LambdaQueryWrapper<WhjkPerson>()
                .in(WhjkPerson::getId, ids));
        return Result.ok();
    }

    @GetMapping("/getByPersonId/{id}")
    @ApiOperation("体检用户详情")
    public Result<WhjkPersonDto> getByPersonId(@PathVariable("id") String id) {
        return Result.ok(personService.getByPersonId(id));
    }

    @PostMapping("/getTodayTjPage")
    @ApiOperation("今日待检")
    public Result<Page<WhjkPerson>> getTodayTjPage(@RequestBody WhjkTodayTjDto whjkTodayTjDto) {
        return Result.ok(personService.TodayTjPage(whjkTodayTjDto));
    }

    @PostMapping("/pageExport")
    @ApiOperation("导出列表信息")
    public void pageExport(HttpServletResponse response, @RequestBody PersonDto person) throws Exception {
        personService.personExcel(response,person);
    }
    @PostMapping("/review")
    @ApiOperation("复检")
    public Result<T> review(@RequestBody WhjkPersonDto whjkPersonDto) {
        personService.review(whjkPersonDto);
        return Result.ok();
    }
    @ApiOperation("保存总检")
    @PostMapping("/saveAdvice")
    public  Result<T> saveAdvice(@RequestBody ConclusionAdviceDto conclusionAdviceDto) {
        personService.saveAdvice(conclusionAdviceDto);
        return Result.ok();
    }
    @ApiOperation("撤回总检")
    @PostMapping("/backAdvice/{peId}")
    public  Result<T> backAdvice(@PathVariable("peId") String peId) {
        personService.backAdvice(peId);
        return Result.ok();
    }
    @PostMapping("/recycling/{peId}")
    @ApiOperation("回收指引单")
    public Result<T> recycling(@PathVariable("peId") String peId) {
        WhjkPerson person = personService.getById(peId);
        if(person.getProcess().equals("3")){
            personService.updateById(WhjkPerson.builder().id(peId).process("4").recyDoctor(SecurityUtils.getUser().getName()).recyTime(LocalDateTime.now()).build());
        }
        return Result.ok();
    }

    @PostMapping("/confirmPrint/{peId}")
    @ApiOperation("确认打印报告")
    public Result<T> confirmPrint(@PathVariable("peId") String peId) {
        WhjkPerson person = personService.getById(peId);
        if(person.getProcess().equals("5")){
            personService.updateById(WhjkPerson.builder().id(peId).process("6")
                    .printDoctor(SecurityUtils.getUser().getName()).printTime(LocalDateTime.now()).build());
        }
        return Result.ok();
    }
}

