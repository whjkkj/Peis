package com.whjk.system.biz.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.dto.PageDto;
import com.whjk.system.data.dto.WhjkTeamCheckInfoDto;
import com.whjk.system.data.entity.WhjkTeamCheckInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.vo.WHJKteamWaitDetailVo;

import java.util.List;

/**
 * <p>
 * 体检单位 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTeamCheckInfoService extends IService<WhjkTeamCheckInfo> {

    void addTeamCheckInfo(WhjkTeamCheckInfo whjkTeamCheckInfo);

    void updateTeamCheckInfo(WhjkTeamCheckInfo whjkTeamCheckInfo);

    Page<WhjkTeamCheckInfo> queryUnit(WhjkTeamCheckInfo TeamCheckInfo);

    List<WhjkTeamCheckInfoDto> queryTeamInfoList(PageDto pageDto);

    Page<WhjkTeamCheckInfo> waitInspectionPerson(WhjkTeamCheckInfo teamCheckInfo);

    WHJKteamWaitDetailVo teamWaitDetail(String groupId);


    WhjkTeamCheckInfoDto queryUnitGroup(String teamId);
}
