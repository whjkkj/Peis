package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysRole> selectUserRole(String userId);


    List<SysMenu> selectUserMenu(String userId);

    Page<SysUser> pageList(Page<Object> objectPage, @Param(Constants.WRAPPER) QueryWrapper<Object> eq);
}
