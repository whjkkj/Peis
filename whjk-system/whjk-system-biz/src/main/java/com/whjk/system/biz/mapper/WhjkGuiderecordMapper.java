package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkGuiderecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 指引单图片上传记录表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkGuiderecordMapper extends BaseMapper<WhjkGuiderecord> {

}
