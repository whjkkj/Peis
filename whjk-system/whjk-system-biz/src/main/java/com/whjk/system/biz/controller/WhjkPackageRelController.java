package com.whjk.system.biz.controller;


import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.core.security.uitls.SecurityUtils;
import com.whjk.system.biz.service.WhjkPackageRelService;
import com.whjk.system.biz.service.WhjkPersonService;
import com.whjk.system.data.dto.WhjkPackageRelDto;
import com.whjk.system.data.dto.WhjkPersonZhxmDto;
import com.whjk.system.data.dto.WhjkZdmxDto;
import com.whjk.system.data.entity.WhjkPackageRel;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.vo.ReviewVo;
import com.whjk.system.data.vo.ZdXjVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 体检登记项目明细关联表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-package-rel")
@Api(value = "体检用户项目详情", tags = {"体检用户项目详情"})
public class WhjkPackageRelController {
    @Autowired
    WhjkPackageRelService packageRelService;
    @Autowired
    WhjkPersonService personService;

    /**
     * @param relId
     * @param type  0恢复弃检 ,1弃检
     * @return
     */
    @PostMapping("/discard/{relId}/{type}")
    @ApiOperation("弃检/恢复弃检")
    public Result<T> recycling(@PathVariable("relId") String relId, @PathVariable("type") String type) {
        Assert.notNull(relId, "relId不能为空");
        Assert.notNull(type, "type0恢复弃检 ,1弃检不能为空");
        WhjkPackageRel packageRel = packageRelService.getById(relId);
        //检查状态（0-未检查，1-已检查,2-弃检,3-未保存
        if (type.equals("1")) {
            if (packageRel.getCheckStatus().equals("1")) {
                throw new RuntimeException("已检查的项目无法设置为弃检");
            } else {
                packageRel.setCheckStatus("2");
            }
        }
        if (type.equals("0")) {
            if (packageRel.getCheckStatus().equals("2")) {
                packageRel.setCheckStatus("0");
            } else {
                throw new RuntimeException("该项目没有弃检无需恢复");
            }
        }
        packageRelService.updateById(packageRel);
        return Result.ok();
    }

    @GetMapping("/getTree/{id}")
    @ApiOperation("医生站-根据用户id查询组合项目")
    public Result<WhjkPersonZhxmDto> getTree(@PathVariable("id") String id) {
        return Result.ok(personService.getTree(id));
    }

    @PostMapping("/generate/section")
    @ApiOperation("生成小节")
    public Result<ZdXjVo> generateSection(@RequestBody WhjkPackageRelDto packageRelDto) {
        return Result.ok(packageRelService.generateSection(packageRelDto));
    }

    @PostMapping("/save/results")
    @ApiOperation("保存体检项目及结果")
    public Result<String> saveResults(@RequestBody WhjkPackageRelDto packageRelDto) {
        packageRelService.saveResults(packageRelDto);
        return Result.ok();
    }


    @GetMapping("/reviewList/{peId}")
    @ApiOperation("初审总审终审诊断列表")
    public Result<List<ReviewVo>> getReviewList(@PathVariable("peId") String peId){
        return Result.ok(packageRelService.getReviewList(peId));
    }
}

