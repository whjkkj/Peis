package com.whjk.system.biz.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkTjbgzswhService;
import com.whjk.system.data.entity.WhjkTjbgzswh;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 体检报告展示维护(大文本信息维护) 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-tjbgzswh")
public class WhjkTjbgzswhController {

    @Autowired
    private WhjkTjbgzswhService tjbgzswhService;

    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkTjbgzswh>> page(@RequestBody WhjkTjbgzswh pageDto){
        return Result.ok(tjbgzswhService.pageList(pageDto));
    }

    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@RequestBody WhjkTjbgzswh whjkTjbgzswh){
        tjbgzswhService.save(whjkTjbgzswh);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result update(@RequestBody WhjkTjbgzswh whjkTjbgzswh){
        tjbgzswhService.updateById(whjkTjbgzswh);
        return Result.ok();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkTjbgzswh> getById(@PathVariable("id") String id){
        return Result.ok(tjbgzswhService.getById(id));
    }
}

