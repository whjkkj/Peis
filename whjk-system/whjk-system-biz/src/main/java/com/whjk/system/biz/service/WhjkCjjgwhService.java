package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkCjjgwhDto;
import com.whjk.system.data.entity.WhjkCjjgwh;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.WhjkZdjywh;

import java.util.List;

/**
 * <p>
 * 常见结果维护 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkCjjgwhService extends IService<WhjkCjjgwh> {

    List<WhjkZdjywh> getListNew(String tjksId);

    void saveCommon(WhjkCjjgwhDto commonResult);

    Page<WhjkCjjgwh> commonPage(WhjkCjjgwh whjkCjjgwh);
}
