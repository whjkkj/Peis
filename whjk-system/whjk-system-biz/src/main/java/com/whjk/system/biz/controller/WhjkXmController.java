package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkXmService;
import com.whjk.system.data.entity.WhjkXm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 体检项目表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-xm")
@Api(tags = "体检子项")
public class WhjkXmController {

    @Autowired
    private WhjkXmService whjkXmService;


    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkXm>> page(@RequestBody WhjkXm pageDto){
        return Result.ok(whjkXmService.pageList(pageDto));
    }

    @PostMapping("/list")
    @ApiOperation("列表")
    public Result<List<WhjkXm>> list(@RequestBody WhjkXm whjkXm){
        return Result.ok(whjkXmService.allList(whjkXm));
    }

    @PostMapping("/add")
    @ApiOperation("添加")
    public Result<T> add(@RequestBody WhjkXm whjkXm){
        whjkXmService.addXm(whjkXm);
        return Result.ok();
    }


    @PostMapping("/update")
    @ApiOperation("修改")
    public Result update(@RequestBody WhjkXm whjkXm){
        whjkXmService.updateById(whjkXm);
        return Result.ok();
    }


    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkXm> getById(@PathVariable("id") String id){
        return Result.ok(whjkXmService.getById(id));
    }
}

