package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkZhxmXmDto;
import com.whjk.system.data.entity.WhjkZhxm;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.po.TcZhxmPo;

import java.util.List;

/**
 * <p>
 * 组合项目主表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZhxmService extends IService<WhjkZhxm> {

    Page<WhjkZhxm> pageList(WhjkZhxm pageDto);

    Page<TcZhxmPo> poPageList(TcZhxmPo pageDto);

    List<TcZhxmPo> poList(TcZhxmPo pageDto);

    void addZhxm(WhjkZhxmXmDto whjkZhxmXmDto);

}
