package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkTemplate;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.vo.TemplateVo;

/**
 * <p>
 * 模板 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTemplateService extends IService<WhjkTemplate> {

    Page<TemplateVo> pageList(TemplateVo templateVo);

    String getTemplateText(String relId)throws Exception;
}
