package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.ArrangeDto;
import com.whjk.system.data.dto.PersonDto;
import com.whjk.system.data.dto.WhjkPersonDto;
import com.whjk.system.data.entity.WhjkArrange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 体检回访 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkArrangeService extends IService<WhjkArrange> {

    Page<WhjkPersonDto> arrPage(PersonDto person);

    void saveArrange(ArrangeDto arrangeDto);
}
