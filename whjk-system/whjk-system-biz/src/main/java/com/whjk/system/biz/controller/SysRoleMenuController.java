package com.whjk.system.biz.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.system.biz.service.SysRoleMenuService;
import com.whjk.system.data.dto.SysRoleMenuDto;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRoleMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "系统角色菜单", tags = {"系统角色菜单"})
@RestController
@RequestMapping(value = "roleMenu")
public class SysRoleMenuController {
    @Autowired
    private SysRoleMenuService sysRoleMenuService;


    @PostMapping("/add")
    @ApiOperation("新增角色菜单")
    public Result<SysRoleMenu> addRoleMenu(@RequestBody @Validated(BaseDto.list.class) SysRoleMenuDto sysRoleMenuDto) {
        sysRoleMenuService.addRoleMenu(sysRoleMenuDto);
        return Result.ok();
    }


    @PostMapping("delete")
    @ApiOperation("逻辑删除角色菜单")
    public Result<SysRoleMenu> updateRoleMenu(@RequestBody SysRoleMenu sysRoleMenu) {

        QueryWrapper<SysRoleMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("menu_id", sysRoleMenu.getMenuId());
        queryWrapper.eq("role_id", sysRoleMenu.getRoleId());
        sysRoleMenu.setDelFlag("1");
        sysRoleMenuService.update(queryWrapper);

        return Result.ok();
    }


    @PostMapping("/queryRoleMenu")
    @ApiOperation("查询角色菜单")
    public Result queryRoleMenu(@RequestBody SysRoleMenu sysRoleMenu) {
        List<SysMenu> sysMenus = sysRoleMenuService.queryRoleMenu(sysRoleMenu.getRoleId());
        return Result.ok(sysMenus);
    }

}
