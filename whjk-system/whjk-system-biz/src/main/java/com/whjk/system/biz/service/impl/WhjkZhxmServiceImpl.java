package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.core.common.utils.NoUtils;
import com.whjk.system.biz.mapper.WhjkTcInfoMapper;
import com.whjk.system.biz.mapper.WhjkZhxmMapper;
import com.whjk.system.biz.service.WhjkZhxmInfoService;
import com.whjk.system.biz.service.WhjkZhxmService;
import com.whjk.system.data.dto.WhjkZhxmXmDto;
import com.whjk.system.data.entity.WhjkTc;
import com.whjk.system.data.entity.WhjkZhxm;
import com.whjk.system.data.entity.WhjkZhxmInfo;
import com.whjk.system.data.po.TcZhxmPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 组合项目主表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkZhxmServiceImpl extends ServiceImpl<WhjkZhxmMapper, WhjkZhxm> implements WhjkZhxmService {

    @Autowired
    NoUtils noUtils;

    @Autowired
    private WhjkZhxmInfoService zhxmInfoService;

    @Override
    public Page<WhjkZhxm> pageList(WhjkZhxm pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkZhxm>().eq(WhjkZhxm::getDelFlag, 0)//按xssx和创建时间排序
                        .like(StringUtils.isNotEmpty(pageDto.getZhmc()), WhjkZhxm::getZhmc, pageDto.getZhmc())
                        .like(StringUtils.isNotEmpty(pageDto.getPyjm()), WhjkZhxm::getPyjm, pageDto.getPyjm())
                        .like(StringUtils.isNotEmpty(pageDto.getWbjm()), WhjkZhxm::getWbjm, pageDto.getWbjm())
                        .eq(StringUtils.isNotEmpty(pageDto.getSffk()), WhjkZhxm::getSffk, pageDto.getSffk())
                        .orderByAsc(WhjkZhxm::getXssx)
                        .orderByDesc(WhjkZhxm::getCreateTime)
        );
    }

    @Override
    public Page<TcZhxmPo> poPageList(TcZhxmPo pageDto) {
        //所有组合
        List<TcZhxmPo> zhxmList = this.baseMapper.allZhxmList(pageDto.getZhmc());
        //分页
        int fromIndex = (pageDto.getCurrent() - 1) * pageDto.getSize();
        int toIndex = Math.min(fromIndex + pageDto.getSize(), zhxmList.size());
        List<TcZhxmPo> pagedTcList = zhxmList.subList(fromIndex, toIndex);
        Page<TcZhxmPo> page = new Page<>(pageDto.getCurrent(), pageDto.getSize(), zhxmList.size());
        page.setRecords(pagedTcList);
        return page;
    }

    @Override
    public List<TcZhxmPo> poList(TcZhxmPo tcZhxmPo) {
        return this.baseMapper.allZhxmList(tcZhxmPo.getZhmc());
    }

    /**
     * 添加组合信息以及组合中的子项
     *
     * @param whjkZhxmXmDto
     * @return
     */
    @Override
    public void addZhxm(WhjkZhxmXmDto whjkZhxmXmDto) {
        //提前获取id和编号
        whjkZhxmXmDto.setId(Long.toString(IdWorker.getId(whjkZhxmXmDto)));
        whjkZhxmXmDto.setZhbh(noUtils.generateNumZhxm());
        this.save(whjkZhxmXmDto);
        //组合子项插入
        List<WhjkZhxmInfo> infoList = whjkZhxmXmDto.getXmIds().stream().map(i -> {
            WhjkZhxmInfo info = new WhjkZhxmInfo();
            info.setZhxmId(whjkZhxmXmDto.getId());
            info.setTjxmId(i);
            return info;
        }).collect(Collectors.toList());
        zhxmInfoService.saveBatch(infoList);
    }
}
