package com.whjk.system.biz.service;

import com.whjk.core.common.api.Result;
import com.whjk.system.data.dto.WhjkZhxmXmDto;
import com.whjk.system.data.entity.WhjkXm;
import com.whjk.system.data.entity.WhjkZhxm;
import com.whjk.system.data.entity.WhjkZhxmInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

/**
 * <p>
 * 组合项目关联表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZhxmInfoService extends IService<WhjkZhxmInfo> {

    List<WhjkXm> xmList(WhjkZhxm whjkZhxm);

    void xmAdd(WhjkZhxmXmDto whjkZhxmXmDto);
}
