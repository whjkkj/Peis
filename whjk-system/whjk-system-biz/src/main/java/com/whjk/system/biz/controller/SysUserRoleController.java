package com.whjk.system.biz.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.SysUserRoleService;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUserRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "系统角色权限", tags = {"系统角色权限"})
@RestController
@RequestMapping(value = "userRole")
public class SysUserRoleController {
    @Autowired
    private SysUserRoleService sysUserRoleService;

    @PostMapping("/add")
    @ApiOperation("新增用户角色")
    public Result<SysUserRole> addUserRole(@RequestBody SysUserRole sysUserRole) {
        /// 查询指定 userId 和 roleId 的记录数量
        LambdaQueryWrapper<SysUserRole> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(SysUserRole::getUserId, sysUserRole.getUserId())
                    .eq(SysUserRole::getRoleId, sysUserRole.getRoleId());
        if (sysUserRoleService.count(queryWrapper) == 0) {
            sysUserRoleService.save(sysUserRole);
        }
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("删除用户角色")
    public Result<SysUserRole> updateUserRole(@RequestBody SysUserRole sysUserRole) {
        sysUserRole.setDelFlag("1");
        sysUserRoleService.update(new LambdaQueryWrapper<SysUserRole>()
                                .eq(SysUserRole::getUserId, sysUserRole.getUserId())
                                .eq(SysUserRole::getRoleId, sysUserRole.getRoleId()));

        return Result.ok();
    }

    @PostMapping("/queryRole")
    @ApiOperation("查询角色权限")
    public Result<SysRole> queryUserRole(@RequestBody SysUserRole sysUserRole) {
        return Result.ok(sysUserRoleService.queryUserRole(sysUserRole.getUserId()));
    }

}
