package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 短信模板 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkMsgMapper extends BaseMapper<WhjkMsg> {

}
