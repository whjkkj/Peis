package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.whjk.system.biz.mapper.WhjkZdjywhRuleMapper;
import com.whjk.system.biz.service.WhjkZdjywhRuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.data.dto.WhjkZdjywhRuleDto;
import com.whjk.system.data.entity.WhjkZdjywhRule;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 规则 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-07-11
 */
@Service
public class WhjkZdjywhRuleServiceImpl extends ServiceImpl<WhjkZdjywhRuleMapper, WhjkZdjywhRule> implements WhjkZdjywhRuleService {
    @Override
    public List<WhjkZdjywhRuleDto> listByZhxm(QueryWrapper<Object> eq) {
        return this.baseMapper.listByZhxm(eq);
    }
}
