package com.whjk.system.biz.service.impl;

import com.whjk.system.biz.mapper.WhjkSwitchServiceMapper;
import com.whjk.system.biz.service.WhjkSwitchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.data.entity.WhjkSwitch;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 开关实现类
 * </p>
 *
 * @author cs
 * @since 2023-07-21
 */
@Service
public class WhjkSwitchServiceImpl extends ServiceImpl<WhjkSwitchServiceMapper, WhjkSwitch> implements WhjkSwitchService {


}
