package com.whjk.system.biz.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.whjk.system.biz.service.WhjkPersonService;
import com.whjk.system.biz.service.WhjkTeamCheckInfoService;
import com.whjk.system.biz.service.WhjkTeamGroupItemService;
import com.whjk.system.data.dto.WhjkTeamGroupDto;
import com.whjk.system.data.entity.*;
import com.whjk.system.biz.mapper.WhjkTeamGroupMapper;
import com.whjk.system.biz.service.WhjkTeamGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * <p>
 * 单位分组信息表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkTeamGroupServiceImpl extends ServiceImpl<WhjkTeamGroupMapper, WhjkTeamGroup> implements WhjkTeamGroupService {

    @Autowired
    private WhjkTeamGroupItemService teamGroupItemService;

    @Autowired
    private WhjkPersonService personService;

    @Override
    public Page<WhjkTeamGroup> queryTeamGroupListByPage(Page<T> Page, WhjkTeamGroup whjkTeamGroup) {
        QueryWrapper<WhjkTeamGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("del_flag", 0)
                .like(StringUtils.isNotBlank(whjkTeamGroup.getGroupName()), "group_name", whjkTeamGroup.getGroupName());
        return baseMapper.selectPage(new Page<>(whjkTeamGroup.getCurrent(), whjkTeamGroup.getSize()), queryWrapper);
    }


    @Override
    public WhjkTeamGroupDto queryTeamGroupList(String groupId) {
        WhjkTeamGroupDto whjkTeamGroup = this.baseMapper.selectTeamGroup(groupId);
        Assert.notNull(whjkTeamGroup, "分组不存在");
        whjkTeamGroup.setItemList(teamGroupItemService.list(new LambdaQueryWrapper<WhjkTeamGroupItem>()
                .eq(WhjkTeamGroupItem::getDelFlag, 0)
                .eq(WhjkTeamGroupItem::getGroupId, groupId)));
        return whjkTeamGroup;
    }


    /**
     * 分组新增
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addTeamGroup(WhjkTeamGroupDto teamGroupDto) {
        // 将新的项目添加到数据库
        this.baseMapper.insert(teamGroupDto);
        // 获取插入后的自增ID
        String groupId = teamGroupDto.getId();
        teamGroupDto.getItemList().forEach(item -> {
            item.setGroupId(groupId);
            item.setDelFlag("0");
        });
        teamGroupItemService.saveBatch(teamGroupDto.getItemList());
    }

    /**
     * 分组更新及分组项目
     */
    @Override
    public void updateGroupItem(WhjkTeamGroupDto teamGroupDto) {

        List<WhjkPerson> personList = personService.list(new LambdaQueryWrapper<WhjkPerson>()
                .eq(WhjkPerson::getDelFlag, 0)
                .eq(WhjkPerson::getTeamId, teamGroupDto.getTeamId())
                .ne(WhjkPerson::getPeStatus, "3"));
        Assert.isTrue(personList.isEmpty(), "已经总检的用户，不能变更项目");

        String groupId = teamGroupDto.getId();
        if (teamGroupDto.getDelFlag().equals("1")) {
            this.baseMapper.updateById(teamGroupDto);
            teamGroupItemService.remove(new LambdaQueryWrapper<WhjkTeamGroupItem>()
                    .eq(WhjkTeamGroupItem::getGroupId, groupId));
            return;
        }
        this.baseMapper.updateById(teamGroupDto);
        // 删除分组下的项目
        teamGroupItemService.remove(new LambdaQueryWrapper<WhjkTeamGroupItem>()
                .eq(WhjkTeamGroupItem::getGroupId, groupId));
        // 添加更新后的项目
        teamGroupDto.getItemList().forEach(item -> item.setGroupId(groupId));
        teamGroupItemService.saveBatch(teamGroupDto.getItemList());
    }


}


