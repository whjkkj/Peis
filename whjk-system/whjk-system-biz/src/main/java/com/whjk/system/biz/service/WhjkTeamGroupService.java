package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkTeamGroupDto;
import com.whjk.system.data.entity.WhjkTeamGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

/**
 * <p>
 * 单位分组信息表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTeamGroupService extends IService<WhjkTeamGroup> {

    Page<WhjkTeamGroup> queryTeamGroupListByPage(Page<T> tPage,WhjkTeamGroup whjkTeamGroup);

    WhjkTeamGroupDto queryTeamGroupList(String groupId);

    void addTeamGroup(WhjkTeamGroupDto teamGroupDto);

    void updateGroupItem(WhjkTeamGroupDto teamGroupDto);
}
