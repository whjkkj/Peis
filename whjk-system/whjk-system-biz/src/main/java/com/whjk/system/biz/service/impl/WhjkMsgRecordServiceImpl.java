package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkMsgRecord;
import com.whjk.system.biz.mapper.WhjkMsgRecordMapper;
import com.whjk.system.biz.service.WhjkMsgRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发送短信记录表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkMsgRecordServiceImpl extends ServiceImpl<WhjkMsgRecordMapper, WhjkMsgRecord> implements WhjkMsgRecordService {

}
