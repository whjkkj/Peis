package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkJcxmsqd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 检查项目申请单 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkJcxmsqdService extends IService<WhjkJcxmsqd> {

}
