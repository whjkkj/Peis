package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkSwitchService;
import com.whjk.system.data.entity.WhjkSwitch;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 开关 前端控制器
 * </p>
 *
 * @author Cs
 * @since 2023-07-21
 */
@RestController
@RequestMapping("/whjk-arrange")
public class WhjkSwitchController {
    @Autowired
    private WhjkSwitchService switchService;

    @PostMapping("/switch/page")
    @ApiOperation("开关列表")
    public Result<List<WhjkSwitch>> switchPage(@RequestBody WhjkSwitch whjkSwitch) {
        return Result.ok(switchService.list(new LambdaQueryWrapper<WhjkSwitch>()
                .eq(StringUtils.isNotBlank(whjkSwitch.getType()), WhjkSwitch::getType, whjkSwitch.getType())
                .eq(StringUtils.isNotBlank(whjkSwitch.getOpen()), WhjkSwitch::getOpen, whjkSwitch.getOpen())
        ));
    }

    /**
     * 功能描述：更改是否启用
     */
    @PostMapping("/switch/isEnable")
    @ApiOperation("更新启用状态")
    public void isEnable(@RequestBody WhjkSwitch whjkSwitch) {
        switchService.updateById(whjkSwitch);
    }


}