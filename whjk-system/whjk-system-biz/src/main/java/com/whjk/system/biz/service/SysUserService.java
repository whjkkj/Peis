package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whjk.system.data.entity.SysMenu;
import com.whjk.system.data.entity.SysRole;
import com.whjk.system.data.entity.SysUser;

import java.util.List;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author hl
 * @since 2021-07-18
 */
public interface SysUserService extends IService<SysUser> {

    List<SysRole> queryUserRole(SysUser sysUser);

    List<SysMenu> queryUserMenu(SysUser Roles);

    Page<SysUser> pageList(Page<Object> objectPage, QueryWrapper<Object> eq);
}
