package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkMsgRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 发送短信记录表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkMsgRecordMapper extends BaseMapper<WhjkMsgRecord> {

}
