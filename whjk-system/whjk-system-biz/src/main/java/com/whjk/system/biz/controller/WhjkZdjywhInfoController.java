package com.whjk.system.biz.controller;



import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkZdjywhInfoService;
import com.whjk.system.data.entity.WhjkZdjywhInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 诊断建议从表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-zdjywh-info")
@Api(tags = "诊断建议从表")
public class WhjkZdjywhInfoController {
    @Autowired
    private WhjkZdjywhInfoService whjkZdjywhInfoService;

    /**
     * 功能描述：新增诊断主表的从表
     *
     * @param  zdjywhInfo
     * @return  返回返回SlaveTable
     */
    @ApiOperation("新增诊断主表的从表")
    @PostMapping("add")
    public Result<WhjkZdjywhInfo> addZdjywh(@RequestBody WhjkZdjywhInfo zdjywhInfo) {
        whjkZdjywhInfoService.save(zdjywhInfo);
        return Result.ok();
    }

    /**
     * 功能描述：更新诊断主表的从表
     *
     * @param  zdjywhInfo
     * @return  返回
     */
    @ApiOperation("更新诊断主表的从表")
    @PostMapping("update")
    public Result<WhjkZdjywhInfo> updateZdjywhInfo(@RequestBody List<WhjkZdjywhInfo> zdjywhInfo) {
        whjkZdjywhInfoService.updateBatchById(zdjywhInfo);
        return Result.ok();
    }

    /**
     * 功能描述：查询主表下的从表内容
     *
     * @param  zdjyId
     * @return  返回该主表Id内容
     */
    @ApiOperation("查询主表下的从表")
    @GetMapping("/querySlaveTable/{id}")
    public Result<WhjkZdjywhInfo> querySlaveTable(@PathVariable("id") String zdjyId) {
        return Result.ok(whjkZdjywhInfoService.querySlaveTable(zdjyId));
    }



}

