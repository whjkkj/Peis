package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkGuiderecord;
import com.whjk.system.biz.mapper.WhjkGuiderecordMapper;
import com.whjk.system.biz.service.WhjkGuiderecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 指引单图片上传记录表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkGuiderecordServiceImpl extends ServiceImpl<WhjkGuiderecordMapper, WhjkGuiderecord> implements WhjkGuiderecordService {

}
