package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkMsgRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 发送短信记录表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkMsgRecordService extends IService<WhjkMsgRecord> {

}
