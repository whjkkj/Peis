package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.whjk.system.biz.service.ApiService;
import com.whjk.system.biz.service.WhjkPackageRelService;
import com.whjk.system.biz.service.WhjkPersonService;
import com.whjk.system.data.dto.ApiDto;
import com.whjk.system.data.entity.WhjkPackageRel;
import com.whjk.system.data.entity.WhjkPerson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/5/005 9:53
 */
@Slf4j
@Service
public class ApiServiceImpl implements ApiService {
    @Autowired
    WhjkPersonService personService;
    @Autowired
    WhjkPackageRelService packageRelService;

    @Override
    public void payCallback(ApiDto apiDto) {
        log.info("HIS回调体检系统报文:" + apiDto);
        //根据登记流水号查询登记信息
      /*  WhjkPerson person = personService.getOne(new QueryWrapper<WhjkPerson>().lambda()
                .eq(WhjkPerson::getRegSerialNo, apiDto.getRegSerialNo()));*/
        WhjkPerson person = personService.getById(apiDto.getPeId());
        Assert.isTrue(person.getChargeStatus().equals("0"),"已经缴费过了，需要退费后才能再次缴费!!!");
        Assert.notNull(person, "通过用户id未查到对应数据");
        //收费标志(0未收，1已收, 2部分已收)
        String payStatus = "1";
        //流程（0-未开始1-已登记,2-已缴费,3-全科结果录入4-已回收指引单,5-已总检 6-报告打印 7-用户接收报告）
        String process = "2";
        //体检状态(0:预约,1:登记,2:检查中,3:已终检)
        String peStatus = "2";

        packageRelService.update(WhjkPackageRel.builder()
                .payStatus(payStatus)
                .otherStatus(payStatus).build(), new QueryWrapper<WhjkPackageRel>().lambda()
                .eq(WhjkPackageRel::getPeId, person.getId()));
        personService.updateById(WhjkPerson.builder()
                .id(person.getId())
                .chargeStatus(payStatus)
                .chargeTime(LocalDateTime.now())
                .peStatus(peStatus)
                .process(process).build());
    }

    @Override
    public void refund(ApiDto apiDto) {
        WhjkPerson person = personService.getById(apiDto.getPeId());
        Assert.notNull(person, "通过用户id未查到对应数据");
        Assert.notNull(person, "通过用户id未查到对应数据");
        Assert.isTrue(person.getChargeStatus().equals("1"),"还没缴费,无法退费!!!");
        //收费标志(0未收，1已收, 2部分已收)
        String payStatus = "0";
        //流程（0-未开始1-已登记,2-已缴费,3-全科结果录入4-已回收指引单,5-已总检 6-报告打印 7-用户接收报告）
        String process = "1";
        //体检状态(0:预约,1:登记,2:检查中,3:已终检)
        String peStatus = "1";

        packageRelService.update(WhjkPackageRel.builder()
                .payStatus(payStatus)
                .otherStatus(payStatus).build(), new QueryWrapper<WhjkPackageRel>().lambda()
                .eq(WhjkPackageRel::getPeId, person.getId()));
        personService.updateById(WhjkPerson.builder()
                .id(person.getId())
                .chargeStatus(payStatus)
                .chargeTime(null)
                .peStatus(peStatus)
                .process(process).build());
    }
}
