package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.data.dto.WhjkTeamCheckInfoDto;
import com.whjk.system.data.entity.WhjkTeamCheckInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 体检单位 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTeamCheckInfoMapper extends BaseMapper<WhjkTeamCheckInfo> {

    List<WhjkTeamCheckInfoDto> selectTeamInfoList();

    Page<WhjkTeamCheckInfo> waitInspectionPersonPage(Page<Object> objectPage, @Param(Constants.WRAPPER)QueryWrapper<Object> objectQueryWrapper);
}
