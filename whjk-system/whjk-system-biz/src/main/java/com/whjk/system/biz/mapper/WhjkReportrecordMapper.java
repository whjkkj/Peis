package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkReportrecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 体检报告领取记录表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkReportrecordMapper extends BaseMapper<WhjkReportrecord> {

}
