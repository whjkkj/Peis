package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkLxService;
import com.whjk.system.data.entity.WhjkLx;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 体检类型设置 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-lx")
@Api(tags = "体检类型")
public class WhjkLxController {

    @Autowired
    private WhjkLxService lxService;

    @PostMapping("/page")
    @ApiOperation("分页")
    public Result<Page<WhjkLx>> page(@RequestBody WhjkLx pageDto){
        return Result.ok(lxService.pageList(pageDto));
    }

    @PostMapping("/list")
    @ApiOperation("列表")
    public Result<List<WhjkLx>> list(){
        return Result.ok(lxService.list(new LambdaQueryWrapper<WhjkLx>().eq(WhjkLx::getDelFlag,0)));
    }

    @PostMapping("/add")
    @ApiOperation("新增")
    public Result<T> add(@RequestBody WhjkLx whjkLx){
        lxService.save(whjkLx);
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result<T> update(@RequestBody WhjkLx whjkLx){
        lxService.updateById(whjkLx);
        return Result.ok();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询")
    public Result<WhjkLx> getById(@PathVariable("id") String id){
        return Result.ok(lxService.getById(id));
    }
}

