package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkLx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 体检类型设置 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkLxService extends IService<WhjkLx> {

    Page<WhjkLx> pageList(WhjkLx pageDto);
}
