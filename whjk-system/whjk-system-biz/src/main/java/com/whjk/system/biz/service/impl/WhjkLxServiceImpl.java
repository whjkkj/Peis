package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkLx;
import com.whjk.system.biz.mapper.WhjkLxMapper;
import com.whjk.system.biz.service.WhjkLxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体检类型设置 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkLxServiceImpl extends ServiceImpl<WhjkLxMapper, WhjkLx> implements WhjkLxService {

    @Override
    public Page<WhjkLx> pageList(WhjkLx pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkLx>().eq(WhjkLx::getDelFlag,0)
                        .like(StringUtils.isNotEmpty(pageDto.getMc()),WhjkLx::getMc,pageDto.getMc())
                        .eq(StringUtils.isNotEmpty(pageDto.getSfqy()),WhjkLx::getSfqy,pageDto.getSfqy())
                        .orderByDesc(WhjkLx::getCreateTime)
        );
    }
}
