package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkTcZhxmDto;
import com.whjk.system.data.entity.WhjkTc;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 体检套餐主表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTcService extends IService<WhjkTc> {

    Page<WhjkTc> pageList(WhjkTc pageDto);

    List<WhjkTc> getList(WhjkTc pageDto);

    void addTc(WhjkTcZhxmDto whjkTcZhxmDto);

}
