package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkPackageRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.po.RegXmListPo;
import com.whjk.system.data.po.WhjkPackageRelInfoPo;
import com.whjk.system.data.po.WhjkPackageRelPo;
import com.whjk.system.data.vo.PackageRelCountVo;
import com.whjk.system.data.vo.ReviewVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * <p>
 * 体检登记项目明细关联表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Mapper
public interface WhjkPackageRelMapper extends BaseMapper<WhjkPackageRel> {

    List<WhjkPackageRelPo> selectByCheckStatus(@Param(Constants.WRAPPER)QueryWrapper<Object> eq);

    List<RegXmListPo> getRelXmList(String id);

    List<WhjkPackageRelInfoPo> selecttjxm(@Param(Constants.WRAPPER)QueryWrapper<Object> eq);

    Page<PackageRelCountVo> selectCountByDoctors(Page<Object> Page, @Param(Constants.WRAPPER)QueryWrapper<Object> groupBy);

    List<WhjkPerson> SelectPersonDetails(@Param(Constants.WRAPPER)QueryWrapper<WhjkPerson> eq);

    List<String> getConclusion(String id);

    List<ReviewVo> getReviewList(String peId);

}
