package com.whjk.system.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkXm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 体检项目表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkXmService extends IService<WhjkXm> {

    Page<WhjkXm> pageList(WhjkXm pageDto);

    List<WhjkXm> allList(WhjkXm whjkXm);

    List<WhjkXm> allListByZhxmId(String zhxmId);

    void addXm(WhjkXm whjkXm);
}
