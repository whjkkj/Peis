package com.whjk.system.biz.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 提醒诊断设置 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-txzd")
public class WhjkTxzdController {

}

