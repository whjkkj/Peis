package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkHisinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * His缴费记录表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkHisinfoService extends IService<WhjkHisinfo> {

}
