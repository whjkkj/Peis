package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkYjsbwh;
import com.whjk.system.biz.mapper.WhjkYjsbwhMapper;
import com.whjk.system.biz.service.WhjkYjsbwhService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医技设备维护 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkYjsbwhServiceImpl extends ServiceImpl<WhjkYjsbwhMapper, WhjkYjsbwh> implements WhjkYjsbwhService {

}
