package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkTeamGroupItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 单位分组对应所选项目表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTeamGroupItemMapper extends BaseMapper<WhjkTeamGroupItem> {

}
