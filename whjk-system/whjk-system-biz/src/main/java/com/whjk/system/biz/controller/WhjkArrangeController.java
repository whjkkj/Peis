package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkArrangeService;
import com.whjk.system.data.dto.ArrangeDto;
import com.whjk.system.data.dto.PersonDto;
import com.whjk.system.data.dto.WhjkPersonDto;
import com.whjk.system.data.entity.WhjkArrange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 体检回访 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-arrange")
@Api(tags = "随访管理")
public class WhjkArrangeController {

    @Autowired
    private WhjkArrangeService arrangeService;

    @PostMapping("/page")
    @ApiOperation("随访人员")
    public Result<Page<WhjkPersonDto>> arrPage(@RequestBody PersonDto person) {
        return Result.ok(arrangeService.arrPage(person));
    }

    @GetMapping("/list/{id}")
    @ApiOperation("随访详情")
    public Result<List<WhjkArrange>> list(@PathVariable("id") String id){
        return Result.ok(arrangeService.list(new LambdaQueryWrapper<WhjkArrange>()
                .eq(WhjkArrange::getPeId,id)
                .eq(WhjkArrange::getDelFlag,0)
                .orderByDesc(WhjkArrange::getArrTime)));
    }

    @PostMapping("/add")
    @ApiOperation("新增随访")
    public Result<T> add(@RequestBody ArrangeDto arrangeDto){
        arrangeService.saveArrange(arrangeDto);
        return Result.ok();
    }

}

