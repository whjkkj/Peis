package com.whjk.system.biz.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.core.common.dto.BaseDto;
import com.whjk.system.biz.service.WhjkZdjywhService;
import com.whjk.system.data.dto.WhjkPackageRelDto;
import com.whjk.system.data.dto.WhjkPackageRelInfoDto;
import com.whjk.system.data.dto.WhjkZdjywhDto;
import com.whjk.system.data.dto.WhjkZdjywhRuleDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import com.whjk.system.data.entity.WhjkTeamGroupItem;
import com.whjk.system.data.entity.WhjkZdjywh;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 诊断建议主表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-zdjywh")
@Api(tags = "诊断建议主表")
public class WhjkZdjywhController {
    @Autowired
    private WhjkZdjywhService whjkZdjywhService;

    /**
     * 功能描述：新增诊断建议主表
     *
     * @param zdjywhDto
     * @return 返回
     */
    @ApiOperation("新增诊断建议主表")
    @PostMapping("add")
    public Result<WhjkTeamGroupItem> addZdjywh(@RequestBody @Validated(BaseDto.add.class) WhjkZdjywhDto zdjywhDto) {
        whjkZdjywhService.addSuggestionTable(zdjywhDto);
        return Result.ok();
    }

    /**
     * 功能描述：更改诊断建议主表
     *
     * @param zdjywhDto
     * @return 返回
     */
    @ApiOperation("更改诊断建议主表")
    @PostMapping("update")
    public Result<WhjkTeamGroupItem> updateZdjywh(@RequestBody @Validated(BaseDto.update.class) WhjkZdjywhDto zdjywhDto) {
        whjkZdjywhService.updateZdjywh(zdjywhDto);
        return Result.ok();
    }
    @ApiOperation("删除诊断建议主表")
    @PostMapping("delete")
    public Result<WhjkTeamGroupItem> deleteZdjywh(@RequestBody WhjkZdjywhDto zdjywhDto) {
        whjkZdjywhService.deleteZdjywh(zdjywhDto);
        return Result.ok();
    }
    /**
     * 功能描述：查询诊断建议
     *
     * @param proposalForm
     * @return 返回分页数据
     */
    @ApiOperation("查询诊断建议主表全部列表")
    @PostMapping("queryProposalForm")
    public Result<Page<WhjkZdjywhDto>> queryProposalForm(@RequestBody WhjkZdjywhDto proposalForm) {
        return Result.ok(whjkZdjywhService.queryProposalForm(proposalForm));
    }

    /**
     * 功能描述：查询诊断建议
     *
     * @param whjkZdjywhRuleDto
     * @return 返回分页数据
     */
    @ApiOperation("根据项目id查询诊断建议")
    @PostMapping("getByzhxmId")
    public Result<Page<WhjkZdjywh>> getByzhxmId(@RequestBody WhjkZdjywhRuleDto whjkZdjywhRuleDto) {
        return Result.ok(whjkZdjywhService.getByzhxmId(whjkZdjywhRuleDto));
    }

    /**
     * 功能描述：查询诊断建议主表及从表
     *
     * @param id 诊断建议主id
     * @return 返回分页数据
     */
    @ApiOperation("查询诊断建议主表及从表")
    @GetMapping("getById/{id}")
    public Result<WhjkZdjywhDto> queryMasterSlaveTable(@PathVariable("id") String id) {
        return Result.ok(whjkZdjywhService.queryMasterSlaveTable(id));
    }

    /**
     * 根据体检结果查询诊断结果
     * @param packageRelDto
     * @return
     */
    @PostMapping("/getZdResult")
    @ApiOperation("根据体检结果查询诊断结果")
    public Result<List<WhjkZdjywhDto>> getZdResult(@RequestBody WhjkPackageRelDto packageRelDto){
        Assert.notNull(packageRelDto,"不能为空！");
        return Result.ok(whjkZdjywhService.getZdResult(packageRelDto));
    }


}

