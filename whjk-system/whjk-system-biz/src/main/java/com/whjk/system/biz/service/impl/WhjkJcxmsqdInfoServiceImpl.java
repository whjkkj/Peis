package com.whjk.system.biz.service.impl;

import com.whjk.system.data.entity.WhjkJcxmsqdInfo;
import com.whjk.system.biz.mapper.WhjkJcxmsqdInfoMapper;
import com.whjk.system.biz.service.WhjkJcxmsqdInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检查项目申请单关联表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkJcxmsqdInfoServiceImpl extends ServiceImpl<WhjkJcxmsqdInfoMapper, WhjkJcxmsqdInfo> implements WhjkJcxmsqdInfoService {

}
