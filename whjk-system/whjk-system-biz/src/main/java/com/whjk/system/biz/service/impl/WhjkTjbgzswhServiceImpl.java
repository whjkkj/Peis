package com.whjk.system.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkTjbgzswh;
import com.whjk.system.biz.mapper.WhjkTjbgzswhMapper;
import com.whjk.system.biz.service.WhjkTjbgzswhService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whjk.system.data.entity.WhjkZhxm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体检报告展示维护(大文本信息维护) 服务实现类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Service
public class WhjkTjbgzswhServiceImpl extends ServiceImpl<WhjkTjbgzswhMapper, WhjkTjbgzswh> implements WhjkTjbgzswhService {

    @Override
    public Page<WhjkTjbgzswh> pageList(WhjkTjbgzswh pageDto) {
        return page(new Page<>(pageDto.getCurrent(), pageDto.getSize()),
                new LambdaQueryWrapper<WhjkTjbgzswh>()
                .like(StringUtils.isNotEmpty(pageDto.getName()),WhjkTjbgzswh::getName,pageDto.getName())
                .orderByAsc(WhjkTjbgzswh::getXssx)
                .orderByDesc(WhjkTjbgzswh::getCreateTime)
        );
    }
}
