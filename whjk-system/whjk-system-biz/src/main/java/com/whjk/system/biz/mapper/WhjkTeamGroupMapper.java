package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkTeamGroupDto;
import com.whjk.system.data.entity.WhjkTeamGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.WhjkTeamGroupItem;
import org.apache.poi.ss.formula.functions.T;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 单位分组信息表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkTeamGroupMapper extends BaseMapper<WhjkTeamGroup> {

    List<WhjkTeamGroup> getSubGroups(String groupId);

    BigDecimal sumStandardPrice(String groupId);

    WhjkTeamGroupDto selectTeamGroup(String groupId);
}
