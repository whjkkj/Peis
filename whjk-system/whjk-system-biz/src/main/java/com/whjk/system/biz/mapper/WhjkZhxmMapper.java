package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkZhxm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.po.TcZhxmPo;

import java.util.List;

/**
 * <p>
 * 组合项目主表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZhxmMapper extends BaseMapper<WhjkZhxm> {

    List<TcZhxmPo> allZhxmList(String zhmc);

}
