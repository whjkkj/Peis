package com.whjk.system.biz.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkPackageRelService;
import com.whjk.system.data.dto.StatisticsDto;
import com.whjk.system.data.entity.WhjkPerson;
import com.whjk.system.data.vo.PackageRelCountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/24/024 9:38
 */
@RestController
@RequestMapping("statistics")
@Api(tags = "统计")
public class StatisticsController {

    @Autowired
    private WhjkPackageRelService whjkPackageRelService;


    //条件查询统计人数
    //whjk_package_rel表，按照check_doctor医生姓名,xm_id项目分组，统计项目人数,且check_status状态为1，可以更具时间筛选，对应数据库check_time
    @PostMapping("/pageList")
    @ApiOperation("统计医生已检患者")
    public Result<Page<PackageRelCountVo>> StatisticsChecked(@RequestBody StatisticsDto statisticsDto){
        return Result.ok(whjkPackageRelService.countDoctors(new Page<>(statisticsDto.getCurrent(), statisticsDto.getSize()),statisticsDto));
    }


    @PostMapping("/personDetails")
    @ApiOperation("医生项目已检查人详情")
    public Result<List<WhjkPerson>> ProjectPersonDetails (@RequestBody StatisticsDto statisticsDto){
        return Result.ok(whjkPackageRelService.ProjectPersonDetails(statisticsDto));
    }

}

