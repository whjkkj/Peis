package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.dto.WhjkZdjywhDto;
import com.whjk.system.data.entity.WhjkPackageRelInfo;
import com.whjk.system.data.entity.WhjkZdjywh;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.WhjkZdjywhRule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 诊断建议主表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@Mapper
public interface WhjkZdjywhMapper extends BaseMapper<WhjkZdjywh> {

    List<WhjkZdjywhDto> selectMasterSlaveBatch(List<String> ids);

    Page<WhjkZdjywhDto> queryProposalForm(Page<Object> objectPage,@Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);

    Page<WhjkZdjywh> getByzhxmId(Page<Object> objectPage,@Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);

    List<WhjkZdjywh> getByzhxmId(@Param(Constants.WRAPPER) QueryWrapper<Object> objectQueryWrapper);

    WhjkZdjywh getZdjywh(String id);
}
