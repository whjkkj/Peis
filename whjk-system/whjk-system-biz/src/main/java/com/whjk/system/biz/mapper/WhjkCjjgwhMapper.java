package com.whjk.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whjk.system.data.entity.WhjkCjjgwh;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whjk.system.data.entity.WhjkPerson;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 常见结果维护 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkCjjgwhMapper extends BaseMapper<WhjkCjjgwh> {

    Page<WhjkCjjgwh> commonPage(Page<Object> objectPage, @Param(Constants.WRAPPER)QueryWrapper<Object> objectQueryWrapper);
}
