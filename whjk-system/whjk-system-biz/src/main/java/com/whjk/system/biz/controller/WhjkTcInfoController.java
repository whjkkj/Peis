package com.whjk.system.biz.controller;


import com.whjk.core.common.annotation.Inner;
import com.whjk.core.common.api.Result;
import com.whjk.system.biz.service.WhjkTcInfoService;
import com.whjk.system.data.dto.WhjkTcZhxmDto;
import com.whjk.system.data.entity.WhjkTc;
import com.whjk.system.data.entity.WhjkTcInfo;
import com.whjk.system.data.entity.WhjkZhxm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 套餐关联表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/whjk-tc-info")
@Api(tags = "体检套餐组合关联")
public class WhjkTcInfoController {

    @Autowired
    private WhjkTcInfoService whjkTcInfoService;

    @PostMapping("/list")
    @ApiOperation("组合列表")
    public Result<List<WhjkZhxm>> list(@RequestBody WhjkTc whjkTc){
        return Result.ok(whjkTcInfoService.zhxmList(whjkTc));
    }



    @PostMapping("/delete")
    @ApiOperation("删除组合")
    public Result delete(@RequestBody WhjkTcInfo whjkTcInfo){
        whjkTcInfoService.updateById(whjkTcInfo);
        return Result.ok();
    }
}

