package com.whjk.system.biz.service;

import com.whjk.system.data.entity.WhjkZdjywhInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 诊断建议从表 服务类
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkZdjywhInfoService extends IService<WhjkZdjywhInfo> {

    WhjkZdjywhInfo querySlaveTable(String zdjyId);
}
