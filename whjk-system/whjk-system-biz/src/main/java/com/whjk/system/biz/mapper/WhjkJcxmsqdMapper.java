package com.whjk.system.biz.mapper;

import com.whjk.system.data.entity.WhjkJcxmsqd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 检查项目申请单 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2023-06-28
 */
public interface WhjkJcxmsqdMapper extends BaseMapper<WhjkJcxmsqd> {

}
