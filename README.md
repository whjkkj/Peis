
# Peis体检系统

#### 介绍
开源体检系统，健康体检系统，公众号预约体检，无纸化体检系统

####  系统登录
![输入图片说明](generator/image.png)
####  团检登记
![输入图片说明](generator/tuanjiandengji.png)
####  体检登记
![输入图片说明](generator/dengji.png)
####  套餐选择
![输入图片说明](generator/taocanxuanze.png)
####  价格修改
![输入图片说明](generator/jiagexiugai.png)
####  体检进度
![输入图片说明](generator/tijianjindu.png)
####  体检医生站
![输入图片说明](generator/tijianyishengzhan.png)
####  总审
![输入图片说明](generator/zongsheng.png)
####  体检报告
![输入图片说明](generator/tijianbaogao.png)
####  导检管理
![输入图片说明](generator/daojianguanli.png)

# 微信端
####  微信首页
![输入图片说明](generator/%E9%A6%96%E9%A1%B5.png)
####  套餐详情
![输入图片说明](generator/%E5%A5%97%E9%A4%90%E8%AF%A6%E6%83%85.png)
####  体检预约
![输入图片说明](generator/%E4%BD%93%E6%A3%80%E9%A2%84%E7%BA%A6.png)
####  体检报告
![输入图片说明](generator/%E4%BD%93%E6%A3%80%E6%8A%A5%E5%91%8A-%E5%BC%82%E5%B8%B8.png)

# 体验地址
体检地址请扫描微信二维码获取
![输入图片说明](generator/ccdd59589590588a1f44c47e1e75919.jpg)