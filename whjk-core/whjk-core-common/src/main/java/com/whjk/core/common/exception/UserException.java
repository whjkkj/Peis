package com.whjk.core.common.exception;

/**
 * 自定义用户异常
 * Created by hl on 2021/7/13.
 */
public class UserException extends RuntimeException {

    public UserException() {
        this("操作失败，请稍后重试");
    }

    public UserException(String message) {
        super(message);
    }

    public UserException(Throwable cause) {
        super(cause);
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }

    public static UserException create(String message) {
        return new UserException(message);
    }
}
