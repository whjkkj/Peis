package com.whjk.core.common.utils;


import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * &#064;Author：hl
 * &#064;DateTime：2022/6/10  13:39
 */
public class BigDecimalUtil {
    
    // 两个数字相除，转百分比并保留两位小数(四舍五入)
    public static BigDecimal divide(Object obj, Object obj2) {
        return new BigDecimal(obj.toString()).divide(new BigDecimal(obj2.toString()), 2, RoundingMode.HALF_UP);
    }

    //计算打折  HALF_DOWN
    public static BigDecimal divideDiscount(Object obj, Object obj2) {
        return new BigDecimal(obj.toString()).multiply(new BigDecimal(obj2.toString())).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
    }
}
