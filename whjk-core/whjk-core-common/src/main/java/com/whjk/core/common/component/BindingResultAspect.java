package com.whjk.core.common.component;


import com.whjk.core.common.api.Result;
import com.whjk.core.common.api.ResultCode;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * HibernateValidator错误结果处理切面
 *
 * @author hl
 * @date 2021-07-14 00:19:58
 */
@Aspect
@Component
@Order(2)
public class BindingResultAspect {

    @Pointcut("execution(public * com.whjk.*.*.controller.*.*(..))")
    public void bindingResult() {
        //
    }

    @Around("bindingResult()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            if (arg instanceof BindingResult) {
                BindingResult result = (BindingResult) arg;
                if (result.hasErrors()) {
                    FieldError fieldError = result.getFieldError();
                    if (fieldError != null) {
                        //return CommonResult.validateFailed(fieldError.getDefaultMessage());
                        return Result.er(ResultCode.VALIDATE_FAILED.getCode(), fieldError.getDefaultMessage());
                    } else {
                        //return CommonResult.validateFailed();
                        return Result.er(ResultCode.VALIDATE_FAILED.getCode());
                    }
                }
            }
        }
        return joinPoint.proceed();
    }
}
