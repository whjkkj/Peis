package com.whjk.core.common.api;

/**
 * 封装API的错误码
 * Created by hl on 2021/7/13.
 */
public interface IErrorCode {
    int getCode();

    String getMessage();
}
