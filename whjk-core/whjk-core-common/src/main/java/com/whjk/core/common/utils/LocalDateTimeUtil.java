package com.whjk.core.common.utils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author hl
 * @creatDate 2023/7/3
 */
public class LocalDateTimeUtil {

    public final static String ALL_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public final static String ALLFORMAT = "yyyyMMddHHmmss";
    public final static String FORMAT = "yyyy-MM-dd";
    public static void main(String[] args) {
        String time = "2022-08-05 14:12:11";
        String time2 = "2022-08-05 14:12:12";
        String timenull = null;

        LocalDateTime now = getCurrentTime();
        LocalDateTime nownull = null;

        LocalDateTime timeA = strToLocalDateTime(time, ALL_FORMAT);
        LocalDateTime timeB = strToLocalDateTime(time2, ALL_FORMAT);

        System.out.println(gt(timeA, timeB));
        System.out.println(localDateTimeToStr(LocalDateTime.now(),ALL_FORMAT));
    }

    /**
     * 字符串转时间
     * @param text
     * @param format
     * @return
     */
    public static LocalDateTime strToLocalDateTime(String text, String format) {
        try {
            return LocalDateTime.parse(text, DateTimeFormatter.ofPattern(format)); // text和format格式须一致不然报错
        } catch (Exception e) {
            throw new RuntimeException("时间转换异常");
        }
    }

    public static String localDateTimeToStr(LocalDateTime time, String format) {
        try {
            return time.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            throw new RuntimeException("时间转换异常");
        }
    }


    /**
     * LocalDate转Date
     * @param localDate
     * @return
     */
    public static Date localDateToDate(LocalDate localDate) {
        if (null == localDate) {
            return null;
        }
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * Date转LocalDate
     * @param date
     */
    public static LocalDate dateToLocalDate(Date date) {
        if(null == date) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * 相差的天数(忽略时分秒)
     *
     * 类似的还有相差时分秒年月日等
     * 时间小的在前，时间大的在后 返回正数，反之负数
     * @param beg
     * @param end
     * @return
     */
    public static long betweenDay(LocalDateTime beg, LocalDateTime end) {
        /*Duration duration = Duration.between(beg,end);
        return duration.toDays();*/
        return beg.until(end, ChronoUnit.DAYS);
    }

    /**
     * 日期加N天 类似的还有时分秒年月日星期等
     * @param time
     * @param day
     * @return
     */
    public static LocalDateTime plusDays(LocalDateTime time, int day) {
        return time.plusDays(day);
    }

    /**
     * 日期减N天 类似的还有时分秒年月日星期等
     * @param time
     * @param day
     * @return
     */
    public static LocalDateTime minusDays(LocalDateTime time, int day) {
        return time.minusDays(day);
    }

    /**
     * 两个日期是否相同
     * @param paramA
     * @param paramB
     * @return
     */
    public static boolean isEqual(LocalDateTime paramA, LocalDateTime paramB) {
        if (paramA == null || paramB == null) {
            return false;
        }
        return paramA.isEqual(paramB);
    }

    /**
     * 前一个日期是否在后一个日期 之前
     * @param paramA
     * @param paramB
     * @return
     */
    public static boolean isBefore(LocalDateTime paramA, LocalDateTime paramB) {
        return paramA.isBefore(paramB);
    }

    /**
     * 前一个日期是否在后一个日期 之后
     * @param paramA
     * @param paramB
     * @return
     */
    public static boolean isAfter(LocalDateTime paramA, LocalDateTime paramB) {
        return paramA.isAfter(paramB);
    }

    /**
     * 前一个日期是否 大于等于 后一个日期
     * @param paramA
     * @param paramB
     * @return
     */
    public static boolean gt(LocalDateTime paramA, LocalDateTime paramB) {
        if (paramA == null || paramB == null) {
            return false;
        }
        long a = Instant.from(paramA.atZone(ZoneId.systemDefault())).toEpochMilli();
        long b = Instant.from(paramB.atZone(ZoneId.systemDefault())).toEpochMilli();
        return a >= b;
    }

    /**
     * 前一个日期是否 小于等于 后一个日期
     * @param paramA
     * @param paramB
     * @return
     */
    public static boolean lt(LocalDateTime paramA, LocalDateTime paramB) {
        if (paramA == null || paramB == null) {
            return false;
        }
        long a = Instant.from(paramA.atZone(ZoneId.systemDefault())).toEpochMilli();
        long b = Instant.from(paramB.atZone(ZoneId.systemDefault())).toEpochMilli();
        return a <= b;
    }

    /**
     * 获取年月日时分秒星期等
     * @param time
     * @return
     */
    public static int getYear(LocalDateTime time) {
        return time.getYear();
    }

    /**
     * 自定义时间
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @param second
     * @return
     */
    public static LocalDateTime getCustom(int year, int month, int day, int hour, int minute, int second) {
        return LocalDateTime.of(year, month, day, hour, minute, second);
    }

    /**
     * 获取当前时间
     * @return
     */
    public static LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    /**
     * 小结
     * 获取当前时间：LocalDateTime.now()
     * 自定义时间：LocalDateTime.of(参数列表)
     * 获取LocalDateTime对象的时间数据：get方法
     * 修改LocalDateTime对象的时间数据：with方法
     * 时间的相加：plus方法
     * 时间的相减：minus方法
     * 判断时间相等：equals方法
     * 判断当前时间是否在某个时间之前：isBefor(LocalDateTime other)
     * 判断当前时间是否在某个时间之后：isAfter(LocalDateTime other)
     * 时间转换成字符串：toString()
     * 字符串转换成时间：parse()
     */
}