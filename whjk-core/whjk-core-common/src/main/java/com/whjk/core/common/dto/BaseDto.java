package com.whjk.core.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    public @interface update {
    }

    public @interface add {
    }

    public @interface list {
    }

    public @interface recordList {
    }

    public @interface uploadList {
    }

    public @interface review {
    }
}