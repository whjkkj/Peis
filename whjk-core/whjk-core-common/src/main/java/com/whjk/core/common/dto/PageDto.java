package com.whjk.core.common.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PageDto extends BaseDto implements Serializable {

    private static final long serialVersionUID = 3011470564363878428L;
    /**
     * 当前第几页，起始页为1
     */
    @ApiModelProperty(value = "当前第几页，起始页为1")
    @TableField(exist = false)
    private Integer current = 1;
    /**
     * 每页条数不传默认为10
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "每页条数不传默认为10")
    private Integer size = 10;

    @ApiModelProperty(value = "模糊查询字段")
    @TableField(exist = false)
    private String keyword;  // 模糊查询关键字（搜索框的值）

}