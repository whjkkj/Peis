package com.whjk.core.common.exception;

/**
 * codeAPI异常
 * Created by hl on 2021/7/13.
 */
public class LoginException extends RuntimeException {

    private String message;
    private String code;

    public LoginException(String message) {
        this.code = null;
        this.message = message;
    }

    public LoginException(String message,String code ) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
