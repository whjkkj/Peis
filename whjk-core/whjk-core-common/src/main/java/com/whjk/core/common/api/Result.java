package com.whjk.core.common.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@ApiModel("全局统一返回结果")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> implements Serializable {
    private static final long serialVersionUID = -27436821367995230L;
    @ApiModelProperty("状态码")
    private Integer code;
    @ApiModelProperty("返回消息")
    private String desc;
    @ApiModelProperty("返回数据")
    private T data;

    public static <T> Result<T> ok(T data) {
        Result<T> result = new Result<>();
        result.setData(data);
        result.setDesc(ResultCode.SUCCESS.getMessage());
        result.setCode(ResultCode.SUCCESS.getCode());
        return result;
    }

    public static <T> Result<T> ok() {
        return ok(null);
    }

    public static <T> Result<T> er(int code) {
        Result<T> result = new Result<>();
        result.setCode(code);
        return result;
    }

    public static <T> Result<T> er(String desc) {
        Result<T> result = new Result<>();
        result.setDesc(desc);
        result.setCode(ResultCode.FAILED.getCode());
        return result;
    }

    public static <T> Result<T> er(int code, String desc) {
        return new Result<>(code, desc, null);
    }
}
