package com.whjk.core.common.exception;


import com.whjk.core.common.api.IErrorCode;

/**
 * 自定义API异常
 * Created by hl on 2021/7/13.
 */
public class WeApiException extends RuntimeException {
    private IErrorCode errorCode;

    public WeApiException(IErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public WeApiException(String message) {
        super(message);
    }

    public WeApiException(Throwable cause) {
        super(cause);
    }

    public WeApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public WeApiException() {
        this("参数错误");
    }

    public IErrorCode getErrorCode() {
        return errorCode;
    }
}
