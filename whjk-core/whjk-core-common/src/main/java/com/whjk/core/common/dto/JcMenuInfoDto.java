package com.whjk.core.common.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class JcMenuInfoDto{

        // JcMenu的属性
        private Long id;
        private String title;
        private String icon;
        private String url;
        private LocalDateTime createTime;
        private String createUser;
        private LocalDateTime updateTime;
        private String updateUser;
        private String delFlag;
        private String describe;
        private String enable;
        // JcMenuInfo的属性
        private Long jcMenuId;
        private String type;
        private String imgUrl;
        private LocalDateTime infoCreateTime;
        private String infoCreateUser;
        private LocalDateTime infoUpdateTime;
        private String infoUpdateUser;
        private String infoDelFlag;
        // 分页相关属性
        private Integer current;
        private Integer size;
        private Integer totalCount;

}
