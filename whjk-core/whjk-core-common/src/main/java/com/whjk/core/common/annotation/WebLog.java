package com.whjk.core.common.annotation;

import java.lang.annotation.*;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/11/011 15:40
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebLog {
}
