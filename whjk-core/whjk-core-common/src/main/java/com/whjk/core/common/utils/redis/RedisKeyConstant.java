package com.whjk.core.common.utils.redis;

public class RedisKeyConstant {
    public static final Integer REDIS_EXPIRE_5_S = 5;//5秒
    public static final Integer REDIS_EXPIRE_10_S = 10;//10秒
    public static final Integer REDIS_EXPIRE_1_MIN = 60;//60秒
    public static final Integer REDIS_EXPIRE_1_HOUNR = 60 * 60;//1小时
    public static final Integer REDIS_EXPIRE_1_DAY = 60 * 60 * 24;//1天
    public static final Integer REDIS_EXPIRE_1_WEEK = 60 * 60 * 24 * 7;//一个星期
    public static final Integer REDIS_EXPIRE_1_MONTH = 60 * 60 * 24 * 30;//一个月


    public static final String PHONE_CHECKCODE_KEY = "phone:checkCode:%s";
    public static final String PHONE_MAIL_KEY = "phone:mail:%s";
    public static final String v_validate_times="validate:times:%s";

}
