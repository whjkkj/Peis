package com.whjk.core.common.exception;


import com.whjk.core.common.api.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 * Created by hl on 2021/7/13.
 */

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final String GLOBAL_ERR_MSG = "数据请求异常，请稍后重试!";

    @ExceptionHandler(value = WeApiException.class)
    public Result<String> handle(WeApiException e) {
        String message = e.getMessage();
        log.debug(message);
        return Result.er(message);
    }


    @ExceptionHandler(value = UserException.class)
    public Result<String> handle(UserException e) {
        String message = e.getMessage();
        log.debug(message);
        return Result.er(message);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<String> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        // 从异常对象中拿到ObjectError对象
        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        String message = objectError.getDefaultMessage();
        log.debug(message);
        // 然后提取错误提示信息进行返回
        return Result.er(message);
    }

    /**
     * 断言
     *
     * @param e
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public Result<String> IllegalArgumentException(IllegalArgumentException e) {
        String message = e.getMessage();
        log.error("报错原因是:" + getTraceText(e));
       // log.debug(message);
        return Result.er(message);
    }


    @ExceptionHandler(value = LoginException.class)
    public Result<String> handle(LoginException e) {
        log.error(e.getMessage());
        return Result.er(e.getMessage());
    }

    /**
     * 处理AccessDeineHandler无权限异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = AccessDeniedException.class)
    public Result<String> exceptionHandler(AccessDeniedException e) {
        log.error("不允许访问！原因是:", e.getMessage());
        return Result.er("权限不足");
    }

    @ExceptionHandler(value = RuntimeException.class)
    public Result<String> RuntimeException(RuntimeException e) {
        log.error("报错原因是:" + getTraceText(e));
        return Result.er(GLOBAL_ERR_MSG + ":" + e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public Result<String> handle(Exception e) {
        e.printStackTrace();
        log.error("报错原因是:" + getTraceText(e));
        return Result.er(GLOBAL_ERR_MSG);
    }

    private String getTraceText(Exception exception) {
        if (exception == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        builder.append(exception.getClass().getName()).append(" : ").append(exception.getMessage()).append("\n");
        for (StackTraceElement element : exception.getStackTrace()) {
            builder.append(element.toString()).append("\n");
        }
        return builder.toString();
    }

}
