package com.whjk.core.common.utils;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.text.*;
import java.time.LocalDateTime;
import java.util.Calendar;

/**
 * @Author: create by hl
 * @Version: v1.0
 * @Description: 序号生成唯一id
 * @Date:2023/7/3
 */
@Slf4j
public class SequenceNoUtils {

    /** .log */
    /**
     * The FieldPosition.
     */
    private static final FieldPosition HELPER_POSITION = new FieldPosition(0);
    /**
     * This Format for format the data to special format.
     */
//    private final static Format dateFormat = new SimpleDateFormat("MMddHHmmssS");
    private final static Format dateFormat = new SimpleDateFormat("yyyyMMdd");
    private final static Format dateFormat2 = new SimpleDateFormat("yyMMdd");
    /**
     * This Format for format the number to special format.
     */
    private final static NumberFormat numberFormat = new DecimalFormat("0000");
    /**
     * 起始值
     */
    private static int seq = 1;
    private static final int MAX = 9999;
    /**
     * 起始日期
     */
    private static Calendar today = Calendar.getInstance();

    /**
     * 时间格式生成序列
     *
     * @return String
     */
    public static synchronized String generateSequenceNo() {
        Calendar rightNow = Calendar.getInstance();
        StringBuffer sb1 = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        StringBuffer format1 = dateFormat.format(today.getTime(), sb1, HELPER_POSITION);
        StringBuffer format2 = dateFormat.format(rightNow.getTime(), sb2, HELPER_POSITION);
        //判断不是当天序号回归1
        if (Integer.parseInt(format1.toString()) != Integer.parseInt(format2.toString())) {
            today = rightNow;
            seq = 1;
        }
        if (seq == MAX) {
            seq = 1;
        } else {
            seq++;
        }
        numberFormat.format(seq, sb2, HELPER_POSITION);
        //log.info("THE SQUENCE IS :" + sb.toString());
        return sb2.toString();
    }

    /**
     * 生成6位的体检编号
     * 100000
     */
    public static String generateTjNum(String sequenceNo) {
        Assert.notNull(sequenceNo, "入参序列号不能为空");
        int defaultLimit = 6;
        int length = sequenceNo.length();

        if (length > defaultLimit) {
            defaultLimit = length;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < defaultLimit - length; i++) {
            sb.append("0");
        }
        sb.append(sequenceNo);
        return sb.toString();
    }

    /**
     * 生成自定义的位数
     *
     * @param sequenceNo
     * @param defaultLimit 自定义位数
     * @return
     */
    public static String generateTjNum(String sequenceNo, int defaultLimit) {
        Assert.notNull(sequenceNo, "入参序列号不能为空");
        int length = sequenceNo.length();

        if (length > defaultLimit) {
            defaultLimit = length;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < defaultLimit - length; i++) {
            sb.append("0");
        }
        sb.append(sequenceNo);
        return sb.toString();
    }

    /**
     * 生成登记流水号
     * yyMMdd000001
     */
    public static String generateRegNum(String seqNo) {
        if (StringUtils.isEmpty(seqNo)) {
            return generateSequenceNo();
        }
        return dateFormat2.format(DateUtil.date()) + seqNo.substring(1);
    }

    public static String generateNum(String sequenceNo, String bhName) {
        Assert.notNull(sequenceNo, "入参序列号不能为空");
        int defaultLimit = 9;
        String s = LocalDateTimeUtil.localDateTimeToStr(LocalDateTime.now(), "yyyyMMdd");
        defaultLimit = defaultLimit - s.length();
        int length = sequenceNo.length();
        if (length > defaultLimit) {
            defaultLimit = length;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        for (int i = 0; i < defaultLimit - length; i++) {
            sb.append("0");
        }
        sb.append(sequenceNo);
        return "jk_" + bhName + "_" + sb;
    }
}

