package com.whjk.core.common.utils;

import com.whjk.core.common.utils.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author hl
 * @since 2023/7/6/006 17:40
 */
@Component
public class NoUtils {
    @Autowired
    RedisUtil redisUtil;

    /**
     * 生成体检号
     * @return
     */
    public String generateNumTj() {
        String boName = "tj";
        return  SequenceNoUtils.generateRegNum(SequenceNoUtils.generateTjNum(nextTjNumValue(boName)));
    }
    /**
     * 生成 4_4_9   体检基础项目编号，例如 jk_jcxm_000000002
     * 100000
     */
    public String generateNumJcxm() {
        String boName = "jcxm";
        return SequenceNoUtils.generateNum(SequenceNoUtils.generateTjNum(nextTjNumValue(boName), 3), boName);
    }
    /**
     * 生成 4_4_9   体检组合项目编号，例如 jk_zhxm_000000002
     * 100000
     */
    public String generateNumZhxm() {
        String boName = "zhxm";
        return SequenceNoUtils.generateNum(SequenceNoUtils.generateTjNum(nextTjNumValue(boName), 3), boName);
    }

    private String nextTjNumValue(String boName) {
        Long increment = redisUtil.increment(LocalDateTimeUtil.localDateTimeToStr(LocalDateTime.now(), "yyyy-MM-dd") + "_" + boName, 60L * 60 * 24 * 7);
        return increment.toString();
    }
}
