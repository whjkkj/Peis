package com.whjk.core.security.uitls;


import com.whjk.core.security.component.UserAuthentication;
import com.whjk.core.security.entity.SecurityUserEntity;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

/**
 * 安全工具类
 *
 * @author hl
 */
@UtilityClass
public class SecurityUtils {
    /**
     * 获取Authentication
     */
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 获取用户
     *
     * @param authentication
     * @return User
     * <p>
     */
    public UserAuthentication getUser(Authentication authentication) {
        return authentication.getPrincipal() instanceof UserAuthentication ? (UserAuthentication) authentication.getPrincipal() : null;
    }

    /**
     * 获取用户
     */
    public SecurityUserEntity getUser() {
        UserAuthentication user = getUser(getAuthentication());
        return user != null ? user.getExtendUserBean() : null;
    }

    /**
     * 获取用户角色信息
     *
     * @return 角色集合
     */
    public List<Integer> getRoles() {

        return null;
    }
}