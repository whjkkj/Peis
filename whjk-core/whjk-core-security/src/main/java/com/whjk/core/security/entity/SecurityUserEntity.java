package com.whjk.core.security.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecurityUserEntity implements Serializable {
    /**
     * 用户ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private String id;
    /**
     * 用户昵称
     */
    private String name;
    /**
     * 用户账号
     */
    private String account;

    private String password;

    private String roleId;
}
