package com.whjk.core.security.component;

import com.whjk.core.security.entity.SecurityUserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * @author hl
 * @date 2021/3/11 11:35
 * @description MyUser
 */
public class UserAuthentication extends User {
    private static final long serialVersionUID = -5302718296398589381L;
    private SecurityUserEntity extendUserBean;
    private String roleCode;
    private Object menu;

    public UserAuthentication(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public SecurityUserEntity getExtendUserBean() {
        return extendUserBean;
    }

    public void setExtendUserBean(SecurityUserEntity extendUserBean) {
        this.extendUserBean = extendUserBean;

    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
    public Object getMenu() {
        return menu;
    }

    public void setMenu(Object menu) {
        this.menu = menu;
    }
}