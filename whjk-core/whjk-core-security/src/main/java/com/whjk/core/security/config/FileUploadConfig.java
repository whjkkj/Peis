package com.whjk.core.security.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "files.upload")
public class FileUploadConfig {

    private String url;
    private String imgUrl;
    private String templeUrl;
}
