package com.whjk.core.security.component;

import com.whjk.core.security.entity.SecurityUserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 描述
 * @create: 2019-07-22 11:26
 */
@Component
public class JwtUtil {


    /**
     * token 过期时间, 单位: 秒. 这个值表示 30 天
     */
    private static final long TOKEN_EXPIRED_TIME = 30 * 24 * 60 * 60;

    /**
     * jwt 加密解密密钥, 这里请自行赋值，本人暂且使用随机数16位
     */
    private static String JWT_SECRET;

    public static final String jwtId = "tokenId";

    @Value("${jwt.secret}")
    private void jwt(String jwt_secret) {
        JWT_SECRET = jwt_secret;
    }

    /**
     * 创建JWT
     *
     * @param claims 这个是payLoad的部分内容, jwt格式：jwtHeader.jwtPayLoad.Signature
     */
    public static String createJWT(Map<String, Object> claims, Long time) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256; //指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
        Date now = new Date(System.currentTimeMillis());

        SecretKey secretKey = generalKey();
        long nowMillis = System.currentTimeMillis();//生成JWT的时间
        //下面就是在为payload添加各种标准声明和私有声明了
        JwtBuilder builder = Jwts.builder().setClaims(claims) //这里其实就是new一个JwtBuilder，设置jwt的body
                .setClaims(claims)          //如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的
                .setHeaderParam("alg", "HS256")  //设置header
                .signWith(signatureAlgorithm, secretKey);//设置签名使用的签名算法和签名使用的秘钥
        if (time >= 0) {
            long expMillis = nowMillis + time * 1000;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);     //设置过期时间
        }
        return builder.compact();
    }

    /**
     * 验证jwt
     */
    public static Claims verifyJwt(String token) {
        //签名秘钥，和生成的签名的秘钥一模一样
        SecretKey key = generalKey();
        Claims claims;
        try {
            claims = Jwts.parser()  //得到DefaultJwtParser
                    .setSigningKey(key)         //设置签名的秘钥
                    .parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
        }//设置需要解析的jwt
        return claims;
    }


    /**
     * 由字符串生成加密key
     *
     * @return
     */
    public static SecretKey generalKey() {
        String stringKey = JWT_SECRET;
        byte[] encodedKey = Base64.decodeBase64(stringKey);
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "HS256");
        return key;
    }

    /**
     * 根据userId和openid生成token
     */
    public static String generateToken(String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("role", "a");
        return createJWT(map, TOKEN_EXPIRED_TIME);
    }

    public static String GetgenerateToken(String token) {
        return verifyJwt(token).get("id").toString();
    }

    public static String generateTokens(String id, String phone) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("phone", phone);
        return createJWT(map, TOKEN_EXPIRED_TIME);
    }

    public static void main(String[] args) {
        System.out.println(generateToken("1"));
    }


    public static String generateToken(SecurityUserEntity extendUserBean, String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", extendUserBean.getId());
        map.put("user", extendUserBean);
        map.put("name", name);
        return createJWT(map, TOKEN_EXPIRED_TIME);
    }

}
