

package com.whjk.core.security.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;
import java.io.File;

@Slf4j
@Configuration
public class MultipartConfig {

    @Value("${files.upload.url}")
    private String tempDir;

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        String property = System.getProperty("user.dir");
        String substring = property.substring(0, property.indexOf(":") + 1);
        String s = substring + tempDir;
        File tmpDirFile = new File(s);
        // 判断文件夹是否存在
        if (!tmpDirFile.exists()) {
            log.info("创建文件夹: " + s);
            //创建文件夹
            tmpDirFile.mkdirs();
        }
        factory.setLocation(s);
        return factory.createMultipartConfig();
    }
}

